import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ArticuloProveedor } from '../../../Modelos/ArticuloProveedor';
import { UtilService } from '../../../util.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ArticuloInter } from 'src/app/Interfaces/Articulo';
import { ProveedorInter } from 'src/app/Interfaces/Proveedor';
import { CatalagoService } from '../../../Servicios/catalago.service';

@Component({
  selector: 'app-articulos',
  templateUrl: './articulos.component.html',
  styleUrls: ['./articulos.component.scss']
})

export class ArticulosComponent implements OnInit {
  idRol;
  articulos = new ArticuloProveedor(0,0,0,0,0,0,0,'',0,'','','');
  Columns = ['acciones', 'modArticulo','nomArticulo', 'nomProveedor', 'exisArticulo', 'precArticulo', 'dctoArticulo', 'cRepArticulo', 'gtosArticulo', 'cBaseArticulo', 'ginsArticulo', 'cveMoneda', 'tEntArticulo'];
  dataSource: any;
  title = 'Asignar Proveedor a Articulo';  
  usuario; articulo; proveedor; moneda;
  @ViewChild(MatPaginator, {static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static:true}) sort:MatSort;
  
  constructor(public http:CatalagoService, public util:UtilService, public dialog:MatDialog) { }

  ngOnInit() {
    this.RenderTable();
    this.usuario = this.util.showSession();
    this.articulos.idUsuario = localStorage.Usuario;
    if(localStorage.Rol == 1 || localStorage.Rol == 2 ||localStorage.Rol == 5){
      this.idRol = 1;
    } else {
      this.idRol = 0;
    }    
  }

  RenderTable(){
    this.http.ArticuloProveedores().subscribe(result=>{
      this.dataSource = new MatTableDataSource;
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{ console.log(<any>error); });
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
      this.dataSource.paginator.firstPage();
  }

  Cargar(row){
    this.articulos.idArticulo = row.idArticulo;
    this.articulos.idProveedor = row.idProveedor;
    this.articulos.idMoneda = row.idMoneda;
    this.articulos.precArticulo = row.precArticulo;
    this.articulos.dctoArticulo = row.dctoArticulo;
    this.articulos.gtosArticulo = row.gtosArticulo;
    this.articulos.ginsArticulo = row.ginsArticulo;
    this.articulos.tEntArticulo = row.tEntArticulo;
    this.articulos.nomArticulo = row.nomArticulo;
    this.articulos.nomProveedor = row.nomProveedor;
    this.openDialog(2);
  }

  openDialog(param):void{
    this.articulos.title = param;
    const dialogRef = this.dialog.open(ModalArticulos,{
      width: '750px',
      height: '410px',
      data:this.articulos
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    });
  }

  Reset(){
    this.articulos = { idArticulo:0, idProveedor:0, idMoneda:0, precArticulo:0, dctoArticulo:0, gtosArticulo:0, ginsArticulo:0, tEntArticulo:'', idUsuario:this.articulos.idUsuario, title:'', nomArticulo:'', nomProveedor:''}; 
  }

  Eliminar(row){
    this.articulos.idArticulo = row.idArticulo;
    this.articulos.idProveedor = row.idProveedor;
    this.articulos.idMoneda = row.idMoneda;
    this.articulos.precArticulo = row.precArticulo;
    this.articulos.dctoArticulo = row.dctoArticulo;
    this.articulos.gtosArticulo = row.gtosArticulo;    
    this.articulos.ginsArticulo = row.ginsArticulo;
    this.articulos.tEntArticulo = row.tEntArticulo;
    this.http.ArticuloProveedorEliminar(this.articulos).subscribe(result=>{
      this.ngOnInit();
    },
    error=>{ console.log(<any>error); });
  }

}

@Component({
  selector:'articulosModelo',
  templateUrl:'./articulosModelo.html',
})

export class ModalArticulos implements OnInit{
  title; moneda; nomArt; nomProv;
  stateCtrlArt = new FormControl();
  FilteredArt: Observable<ArticuloInter[]>;
  ArticuloI: ArticuloInter[]=[];
  stateCtrlProv = new FormControl();
  FilteredProv: Observable<ProveedorInter[]>;
  ProveedorI: ProveedorInter[]=[];

  constructor(public Modal:MatDialogRef<ModalArticulos>,public http:CatalagoService
    ,@Inject(MAT_DIALOG_DATA) public articulos:ArticuloProveedor){}
  
  ngOnInit(){
    this.Moneda();
    this.ValidadorEvento();
  }

  filterArt(value:string):ArticuloInter[]{
    const filterValue = value.toLowerCase();
    return this.ArticuloI.filter(articulos => articulos.modArticulo.toLowerCase().indexOf(filterValue)===0);
  }

  filterProv(value:string):ProveedorInter[]{
    const filterValue = value.toLowerCase();
    return this.ProveedorI.filter(proveedores => proveedores.nomProveedor.toLowerCase().indexOf(filterValue)===0);
  }

  Articulo(){
    this.http.Catalagos().subscribe(result=>{
      this.ArticuloI = Object.values(result);      
      this.FilteredArt = this.stateCtrlArt.valueChanges.pipe(startWith(''), map(articulos => articulos ? this.filterArt(articulos):this.ArticuloI.slice()));
    },
    error=>{ console.log(<any>error); });
  }

  Proveedor(){
    this.http.Proveedores().subscribe(result=>{
      this.ProveedorI = Object.values(result);      
      this.FilteredProv = this.stateCtrlProv.valueChanges.pipe(startWith(''), map(proveedores => proveedores ? this.filterProv(proveedores):this.ProveedorI.slice()));    
    },
    error=>{ console.log(<any>error); });
  }

  Moneda(){
    this.http.Monedas().subscribe(result=>{
      this.moneda = result;
    },
    error=>{ console.log(<any>error); });
  }
  
  Editar(){
    this.http.ArticuloProveedorEditar(this.articulos).subscribe(result=>{
      this.Modal.close();
    },
    error=>{ console.log(<any>error); });
  }

  Nuevo(){
    this.http.ArticuloProveedorCrear(this.articulos).subscribe(result=>{
      this.Modal.close();
    },
    error=>{ console.log(<any>error); });
  }
 
  onNoClick():void{
    this.Modal.close();
  }

  ValidadorEvento(){
    if(this.articulos.title=="1"){ 
      this.Articulo();
      this.Proveedor();
      this.title = 'Asignar Proveedor a Articulo';
      document.getElementById('Nuevo').style.display = 'inline';
      document.getElementById('Actualizar').style.display = 'none';
      this.stateCtrlProv.enable();      
      this.stateCtrlArt.enable();
    } else {
      this.title = 'Editar Asignacion';
      document.getElementById('Nuevo').style.display='none';
      document.getElementById('Actualizar').style.display='inline';
      this.setArticulo(this.articulos.idArticulo);
      this.stateCtrlArt.setValue(this.articulos.nomArticulo);
      this.stateCtrlArt.disable();
      this.setProveedor(this.articulos.idProveedor);
      this.stateCtrlProv.setValue(this.articulos.nomProveedor);
      this.stateCtrlProv.disable();
    }      
  }

  setArticulo(idArt){
    this.articulos.idArticulo = idArt;
  }

  setProveedor(idProv){
    this.articulos.idProveedor = idProv;
  }
}
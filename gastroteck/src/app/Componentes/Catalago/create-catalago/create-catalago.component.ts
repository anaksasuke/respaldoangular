import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Catalago } from '../../../Modelos/CatalogoPru';
import { CatalagoService } from '../../../Servicios/catalago.service';
import { UtilService } from '../../../util.service';
import { Marcas } from '../../../Interfaces/Marcas';
import { MatAccordion } from '@angular/material';

@Component({
  selector: 'app-create-catalago',
  templateUrl: './create-catalago.component.html',
  styleUrls: ['./create-catalago.component.scss']
})

export class CreateCatalagoComponent implements OnInit {
  catalagos = new Catalago(0,0,'',0,'','',' - ',' - ',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
  checked = false; checkedplo = false;
  constructor(public http:CatalagoService,public util:UtilService) { }
  Clasificacion; plomerias;  gaces;  energias; ; departamento; grupo;  linea;  unidad; tipos;
  TipoPlomeria;
  @ViewChild('accordion',{static:true}) Accordion:MatAccordion;

  stateMarca = new FormControl();
  FilterMarca:Observable<Marcas[]>;
  MarcasAuto:Marcas[]=[];
  ngOnInit() {
    this.catalagos.idUser = localStorage.Usuario;
    //this.OpendAccordion();
    this.Clasificaciones();
    this.Marcas();
    this.UnidadMedida();
    this.Departamento();
    this.Energia();
    this.Calor();
  }

 /* OpendAccordion(){
    this.Accordion.multi = true;
    this.Accordion.openAll();
  }*/

  Clasificaciones(){
    this.http.Clasificaciones().subscribe(result=>{
      this.Clasificacion = Object.values(result);
      this.Clasificacion.splice(2,1);
    },error=>{console.log(<any>error);});
  }

  Categorias(idClasificacion){
    this.http.Categoria(idClasificacion).subscribe(result=>{
      this.tipos = result;
    },error=>{console.log(<any>error);});
  }
  
  Marcas(){
    this.http.Marcas().subscribe(result=>{
      this.MarcasAuto = Object.values(result);
      this.FilterMarca = this.stateMarca.valueChanges.pipe(startWith(''),map(mar => mar ? this.filterMar(mar):this.MarcasAuto.slice()));
    },error=>{console.log(<any>error);});
  }

  filterMar(value:string):Marcas[]{
    const filterValue = value.toLowerCase();
    return this.MarcasAuto.filter(mar => mar.marcArticulo.toLowerCase().indexOf(filterValue)===0);
  }

  ObtMarca(id){
    this.catalagos.idMarca = id;
  }

  UnidadMedida(){
    this.http.Unidades().subscribe(result=>{
      this.unidad = result;
    },error=>{console.log(<any>error);});
  }

  Departamento(){
    this.http.Departamentos().subscribe(result=>{
      this.departamento = result;
    },error=>{console.log(<any>error);});
  }

  Grupos(idDepartamento){
    this.http.Grupos(idDepartamento).subscribe(result=>{
      this.grupo = result;
    },error=>{console.log(<any>error);});
  }

  Lineas(idGrupo){
    this.http.Lineas(this.catalagos.idDep,idGrupo).subscribe(result=>{
      this.linea = result;
    },error=>{console.log(<any>error);});
  }

  Plomerias(){
    this.http.Plomerias().subscribe(result=>{
      this.plomerias = Object.values(result);
      this.plomerias.splice(0,1);
    },error=>{console.log(<any>error);});
  }

  Calor(){
    this.http.EnergiaCalor().subscribe(result=>{
      this.gaces = result;
    },error=>{console.log(<any>error);});
  }

  Energia(){
    this.http.EnergiaElectrica().subscribe(result=>{
      this.energias = result;
    },error=>{console.log(<any>error);});
  }

  Dimenciones(){
    if(this.checked == false){
      this.catalagos.Frente = 0;
      this.catalagos.Altura = 0;
      this.catalagos.Profundidad = 0;
    }
  }

  TiposPlomeria(){
    if(this.checkedplo == true){
      this.Plomerias();
    }else {
      this.TipoPlomeria = undefined;
    }
  }

  ValidarMultiSelect(){
    if(this.TipoPlomeria == undefined){
      this.catalagos.idPlomeria = 1;
    }else {
      if(this.TipoPlomeria.length != 0){
        this.TipoPlomeria.forEach(element=>{
          if(parseInt(element) == 1){
            this.catalagos.idPlomeriaC = 1;
          }else if(parseInt(element) == 2){
            this.catalagos.idPlomeriaD = 3;
          }else {
            this.catalagos.idPlomeriaF = 2;
          }
        });
      }
    }
    this.Guardar();
  }

  Guardar(){
    this.http.Guardar(this.catalagos).subscribe(result=>{
      this.util.NavegacionDinamica(result,"catalago");
      console.log(result);
    },error=>{console.log(<any>error);});
  }

}

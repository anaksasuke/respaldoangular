import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CatalagoEstatus } from '../../../Modelos/CatalagoEstatus';
import { CatalagoService } from '../../../Servicios/catalago.service';
import { UtilService } from '../../../util.service';

@Component({
  selector: 'app-desactivados',
  templateUrl: './desactivados.component.html',
  styleUrls: ['./desactivados.component.scss']
})
export class DesactivadosComponent implements OnInit {

  constructor(public http:CatalagoService,public util:UtilService) { }
  Columns =['acciones','modArticulo','codSatArticulo','clasArticulo','nomArticulo','dimArticulo','marcArticulo','depArticulo','linArticulo','gruArticulo','catArticulo'];
  dataSource:any;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  catalagoEstatus = new CatalagoEstatus(0,0);
  ngOnInit() {
    this.catalagoEstatus.idUsuario =  localStorage.Usuario;
    this.RenderTable();
  }

  RenderTable(){
    this.http.ArticulosDesctivados().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },error=>{console.log(<any>error);});
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  ActivarArticulo(row){
    this.catalagoEstatus = {idArticulo:row.idArticulo,idUsuario:this.catalagoEstatus.idUsuario};
    this.http.Activar(this.catalagoEstatus).subscribe(result=>{
      this.util.CambioEstatus(result);
      this.RenderTable();
    },error=>{console.log(<any>error);});
  }


}

import { Component, OnInit, ViewChild } from '@angular/core';
import { UtilService } from '../../../util.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Catalago } from '../../../Modelos/CatalogoPru';
import { CatalagoEstatus } from '../../../Modelos/CatalagoEstatus';
import { CatalagoService } from '../../../Servicios/catalago.service';

@Component({
  selector: 'app-show-catalago',
  templateUrl: './show-catalago.component.html',
  styleUrls: ['./show-catalago.component.scss']
})
export class ShowCatalagoComponent implements OnInit {

  constructor(public http:CatalagoService,public util:UtilService) { }
  Columns =['acciones','modArticulo','codSatArticulo','clasArticulo','nomArticulo','dimArticulo','marcArticulo','depArticulo','linArticulo','gruArticulo','catArticulo', 'fAltArticulo', 'fModArticulo','usuario'];
  dataSource:any;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  catalagos = new Catalago(0,0,'',0,'','',' - ',' - ',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
  catalagoEstatus = new CatalagoEstatus(0,0);
  ngOnInit() {
    this.RenderTable();
    this.catalagos.idUser = localStorage.Usuario;
  }

  RenderTable(){
    this.http.Catalagos().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },error=>{console.log(<any>error);});
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  ObtenerDatos(row){
    this.http.ObtenerCatalago(row.idArticulo).subscribe(result=>{
      this.util.getDatos(result);
      this.util.navegacion('catalago/editar');
    },error=>{console.log(<any>error);});
  }

  Eliminar(row){
    this.catalagos.idCatalago = row.idArticulo;
    this.http.CatalogoEliminar(this.catalagos).subscribe(result=>{
      this.RenderTable();
      console.log(result);
    },error=>{console.log(<any>error);});
  }

  DesactivarArticulo(row){
    this.catalagoEstatus = {idArticulo:row.idArticulo,idUsuario:this.catalagos.idUser};
    this.http.Desactivar(this.catalagoEstatus).subscribe(result=>{
      this.util.CambioEstatus(result);
      this.RenderTable();
    },error=>{console.log(<any>error);});
  }

}

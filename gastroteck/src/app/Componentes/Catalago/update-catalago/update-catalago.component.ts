import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Catalago } from '../../../Modelos/CatalogoPru';
import { UtilService } from '../../../util.service';
import { CatalagoService } from '../../../Servicios/catalago.service';
import { Marcas } from '../../../Interfaces/Marcas';
import { MatAccordion } from '@angular/material';
@Component({
  selector: 'app-update-catalago',
  templateUrl: './update-catalago.component.html',
  styleUrls: ['./update-catalago.component.scss']
})
export class UpdateCatalagoComponent implements OnInit {

  constructor(public http:CatalagoService,public util:UtilService) { }
  catalagos = new Catalago(0,0,'',0,'','','','',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
  ArrayString; optMul=[]; optMulNuev; checkedplo=false; dimension=false;
  Clasificacion; plomerias; gaces; energias; marcas; departamento; DepTem; linea; grupo; unidad; tipos;
  stateMarca = new FormControl();
  FilterMarca:Observable<Marcas[]>;
  MarcasAuto:Marcas[]=[];
  @ViewChild('accordion',{static:true}) Accordion:MatAccordion;

  ngOnInit(){
    this.catalagos.idUser = localStorage.Usuario;
    this.Clasificaciones();
    this.Marcas();
    this.UnidadMedida();
    this.Departamento();
    this.Calor();
    this.Energia();
    this.Plomerias();
    this.CargarDatos();
    //this.OpendAccordion();
  }

  /*OpendAccordion(){
    this.Accordion.multi = true;
    this.Accordion.openAll();
  }*/

  CargarDatos(){
    var actualizar = this.util.showDatos();
    actualizar.forEach(element=>{
      this.catalagos ={
        idCatalago:parseInt(element.idArticulo),
        idClas:element.idClasArticulo,
        Model:element.modArticulo,
        SAT:element.codSatArticulo,
        Nom:element.nomArticulo,
        Des:element.descArticulo,
        ficha:element.ficArticulo,
        img:element.imaArticulo,
        Frente:element.frenteArticulo,
        Profundidad:element.profArticulo,
        Altura:element.altArticulo,
        idPlomeria:0,
        idPlomeriaC:0,
        idPlomeriaF:0,
        idPlomeriaD:0,
        idElectrico:element.idEneElectrica,
        idGas:element.idEneCalorifica,
        idMarca:element.idMarcArticulo,
        idDep:element.idDepArticulo,
        idGru:element.idGruArticulo,
        idLine:element.idLinArticulo,
        idUMed:element.idMedArticulo,
        idTipo:element.idCatarticulo,
        idUser:this.catalagos.idUser
      }
      this.ArrayString = element.tubArticulo
      this.stateMarca.setValue(element.marcArticulo);
    });
    this.Categorias(this.catalagos.idClas);
    this.Grupos(this.catalagos.idDep);
    this.Lineas(this.catalagos.idGru);
    this.ConstruirArray();
    this.ValidarDimensiones();
    console.log(this.catalagos);
  }

  ValidarDimensiones(){
    if(this.catalagos.Frente == null && this.catalagos.Altura == null && this.catalagos.Profundidad == null){
      this.dimension = false;
    }else{
      this.dimension = true;
    }
  }

  ConstruirArray(){
    var long = (this.ArrayString.length) - 2;
    this.ArrayString = this.ArrayString.substr(1,long);
    this.ArrayString = this.ArrayString.split(",");
    this.ArrayString.forEach(element => {
      this.AsignacionTuberia(element);
    });  
  }

  AsignacionTuberia(valor){
    switch(valor){
      case "0":
        this.catalagos.idPlomeria = 1;
      break;
      case "1":
        this.catalagos.idPlomeriaC = 1;
        this.checkedplo = true;
        this.optMul.push("1");
      break;
      case "2":
        this.catalagos.idPlomeriaF = 2;
        this.optMul.push("2");
        this.checkedplo = true;
      break;
      case "3":
        this.catalagos.idPlomeriaD = 3;
        this.optMul.push("3");
        this.checkedplo = true;
      break;
    }
  }

  Clasificaciones(){
    this.http.Clasificaciones().subscribe(result=>{
      this.Clasificacion = Object.values(result);
      this.Clasificacion.splice(2,1);
    },error=>{console.log(<any>error);});
  }

  Categorias(idClasificacion){
    this.http.Categoria(idClasificacion).subscribe(result=>{
      this.tipos = result;
    },error=>{console.log(<any>error);});
  }
  
  Marcas(){
    this.http.Marcas().subscribe(result=>{
      this.MarcasAuto = Object.values(result);
      this.FilterMarca = this.stateMarca.valueChanges.pipe(startWith(' '),map(mar => mar ? this.filterMar(mar):this.MarcasAuto.slice()))
    },error=>{console.log(<any>error);});
  }

  filterMar(value:string):Marcas[]{
    const filterValue = value.toLowerCase();
    return this.MarcasAuto.filter(mar => mar.marcArticulo.toLowerCase().indexOf(filterValue)===0);
  }

  ObtMarca(id){
    this.catalagos.idMarca = id;
  }

  UnidadMedida(){
    this.http.Unidades().subscribe(result=>{
      this.unidad = result;
    },error=>{console.log(<any>error);});
  }

  Departamento(){
    this.http.Departamentos().subscribe(result=>{
      this.departamento = result;
    },error=>{console.log(<any>error);});
  }

  Grupos(idDepartamento){
    this.http.Grupos(idDepartamento).subscribe(result=>{
      this.grupo = result;
    },error=>{console.log(<any>error);});
  }

  Lineas(idGrupo){
    this.http.Lineas(this.catalagos.idDep,idGrupo).subscribe(result=>{
      this.linea = result;
    },error=>{console.log(<any>error);});
  }

  Plomerias(){
    this.http.Plomerias().subscribe(result=>{
      this.plomerias = Object.values(result);
      this.plomerias.splice(0,1);
    },error=>{console.log(<any>error);});
  }

  Calor(){
    this.http.EnergiaCalor().subscribe(result=>{
      this.gaces = result;
    },error=>{console.log(<any>error);});
  }

  Energia(){
    this.http.EnergiaElectrica().subscribe(result=>{
      this.energias = result;
    },error=>{console.log(<any>error);});
  }

  Dimensiones(){
    if(this.dimension == false){
      this.catalagos.Frente = 0;
      this.catalagos.Altura = 0;
      this.catalagos.Profundidad = 0;
    }
  }

  TiposPlomerias(){
    if(this.checkedplo == false){
      this.optMulNuev = undefined;
      this.optMul = [];
      this.catalagos.idPlomeriaF = 0;
      this.catalagos.idPlomeriaC = 0;
      this.catalagos.idPlomeriaD = 0;
    }else {
      this.catalagos.idPlomeria = 0;
    }
  }  

  CopiArray(valor){
    this.optMulNuev = valor;
  }

  ValidarMultiSelect(){
    if(this.optMulNuev == undefined){
      this.catalagos.idPlomeria = 1;
    }else {
      if(this.optMulNuev.length != 0){
        this.optMulNuev.forEach(element=>{
          if(parseInt(element) == 1){
            this.catalagos.idPlomeriaC = 1;
          }else if(parseInt(element) == 2){
            this.catalagos.idPlomeriaD = 3;
          }else {
            this.catalagos.idPlomeriaF = 2;
          }
        });  
      }
    }
    this.Editar();
  }

  Editar(){
    console.log(this.catalagos);
    this.http.Editar(this.catalagos).subscribe(result=>{
      this.util.NavegacionDinamica(result,"catalago");
    },error=>{console.log(<any>error);});
  }

}

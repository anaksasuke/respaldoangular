import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ClientesService } from '../../../Servicios/clientes.service';
import { UtilService } from '../../../util.service';
import { Contactos } from '../../../Modelos/Contactos';

@Component({
  selector: 'app-contactos',
  templateUrl: './contactos.component.html',
  styleUrls: ['./contactos.component.scss']
})
export class ContactosComponent implements OnInit {

  constructor(public http:ClientesService, public util:UtilService,public dialog: MatDialog) { }
  Columns =['acciones','tipoContacto', 'preContacto','nomComContacto','cargoContacto','telCelContacto','emailContacto', 'fAltaContacto','fModContacto'];
  dataSource:any
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  TipoCon;
  contacto = new Contactos(0,0,0,'','','','',0,'',0,0);

  ngOnInit() {
    this.contacto.idCli = this.util.showDatos();
    this.contacto.idUser =  localStorage.Usuario;
    this.renderTable();
  }

  renderTable(){
    this.http.Contactos(this.contacto.idCli).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort
    },error=>{console.log(<any>error);});
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  ObtenerDatos(row){
    this.http.obtConAct(parseInt(row.idCliente),parseInt(row.idContacto)).subscribe(result=>{
        this.Cargar(result);
    },error=>{
      console.log(<any>error);
    });
  }

  Cargar(datos){
    datos.forEach(element => {
      this.contacto = {idCon:element.idContacto,idCli:element.idCliente,idT:element.idTipoContacto,nom:element.nomContacto,pat:element.patContacto,mat:element.matContacto,car:element.cargoContacto,cel:element.telCelContacto,email:element.emailContacto,def:element.defContacto,idUser:this.contacto.idUser}
      this.OpendDialog();
    });
  }

  OpendDialog(){
    const dialogRef = this.dialog.open(clienteContacto,{
      width:"auto",
      height:"auto",
      data: this.contacto
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.renderTable();
    });
  }

  Borrar(row){
    this.contacto.idCli = row.idCliente;
    this.contacto.idCon = row.idContacto;
    this.contacto.idUser = this.contacto.idUser;
    this.http.ContactoEliminar(this.contacto).subscribe(result=>{
      this.renderTable();
      console.log(result);
    },error=>{console.log(<any>error);});
  }

  Reset(){
    this.contacto = {idCon:0,idCli:this.contacto.idCli,idT:0,nom:'',pat:'',mat:'',car:'',cel:0,email:'',def:0,idUser:this.contacto.idUser}
  }
}


@Component({
  selector:"clienteContacto",
  templateUrl:"clienteContacto.html"
})

export class clienteContacto implements OnInit {
  constructor(public modal:MatDialogRef<clienteContacto>,public http:ClientesService,@Inject(MAT_DIALOG_DATA) public contacto:Contactos){}
  TipoCon; title;
  Default=[{valor:"1",vista:"Si"},{valor:"0",vista:"No"}];
  ngOnInit(){
    this.TipoContactos();
    if(this.contacto.idCon == 0)
      this.title = "Nuevo Contacto";
    else
      this.title = "Editar Contacto"
  }

  TipoContactos(){
    this.http.TipoContactos().subscribe(result=>{
      this.TipoCon = result;
    },error=>{console.log(<any>error);});
  }

  Nuevo(){ 
    this.http.ContactosCrear(this.contacto).subscribe(result=>{
      this.modal.close();
      console.log(result);
    },error=>{console.log(<any>error);});
  }

  Actualizar(){
    this.http.ContactoEditar(this.contacto).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },error=>{console.log(<any>error);})
  }

  Cancelar(){
    this.modal.close();
  }

}
import { Component, OnInit } from '@angular/core';
import { Cliente } from '../../../Modelos/Cliente';
import { MatDialogRef } from '@angular/material/dialog';
import { UtilService } from '../../../util.service';
import { ClientesService } from '../../../Servicios/clientes.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  constructor(public modal:MatDialogRef<CreateComponent>, private http:ClientesService, public util:UtilService) { }
  cliente = new Cliente(0,"",0,0,0);
  tipo; open;
 
  ngOnInit() {
    this.TipoCliente();    
    this.cliente.idUser=  localStorage.Usuario;
  }

  TipoCliente(){
    this.http.tipoCliente().subscribe(result=>{
      this.tipo = Object.values(result);
      this.tipo.splice(0,2);
    },
    error=>{ console.log(<any>error); });
  }

  Crear(){
    this.http.ClienteCrear(this.cliente).subscribe(result=>{
      this.util.NavegacionDinamica(result,"cliente");
      console.log(result);
      this.modal.close();
    },
    error=>{ console.log(<any>error); });
  }

  Cambio(param){
    if(param == 2 || param == 3){
      this.open = 1;
    }
  }

  Regreso(){
    this.modal.close();
  }
}
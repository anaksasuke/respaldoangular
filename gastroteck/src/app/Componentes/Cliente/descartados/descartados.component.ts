import { Component, OnInit,ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table'
import { ClientesService } from '../../../Servicios/clientes.service';
import { Cliente } from '../../../Modelos/Cliente';

@Component({
  selector: 'app-descartados',
  templateUrl: './descartados.component.html',
  styleUrls: ['./descartados.component.scss']
})
export class DescartadosComponent implements OnInit {

  constructor(public http:ClientesService) { }
  Columns =['acciones','idCliente','compCliente','tipoCliente','telFijoCliente'];
  dataSource:any
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  cliente = new Cliente(0,'',0,0,0)

  ngOnInit() {
    this.RenderTable();
    this.cliente.idUser =  localStorage.Usuario;
  }

  RenderTable(){
    this.http.ClientesDescartados().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    },error=>{console.log(<any>error);});
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  ReactivarCli(row){
    this.cliente.idCliente = row.idCliente;
    this.http.Reactivar(this.cliente).subscribe(result=>{
      this.RenderTable();
    },error=>{
      console.log(<any>error);
    });
  }

}

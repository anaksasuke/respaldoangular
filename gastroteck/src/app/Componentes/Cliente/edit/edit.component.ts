import { Component, OnInit } from '@angular/core';
import { ClienteDetalle } from '../../../Modelos/ClienteDetalle';
//import { ServiciosService } from '../../../servicios.service';
import { ClientesService } from '../../../Servicios/clientes.service'
import { UtilService } from '../../../util.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  constructor(public http:ClientesService,public util:UtilService) { }
  clienteDetalle = new ClienteDetalle(0,0,'',0,0,0,0,0,0,0,0,'','','',0,'',0,'','','',0);
  open; prueba; tipo; giro;especialidad; pais; estado; ciudad; paisTem; tipop;//agentes;

  ngOnInit() {
    this.CargarDatos();
    this.SelectTipo();   
    this.SelectGiro();
    this.SelectEspecialidad();
    this.SelectPais();
    this.TipoP();
    this.clienteDetalle.idUser =  localStorage.Usuario;
  }

  CargarDatos(){
    var datosSer = this.util.showDatos();
    datosSer.forEach(element => {
      this.clienteDetalle={
        idCliente:element.idCliente,
        idNuvCli:element.idCliente,
        compCliente:element.compCliente,
        telFijo:element.telFijoCliente,
        idTipoCli:element.idTipoCliente.toString(),
        idEspCli:element.idEspCliente,
        idGiroCli:element.idGiroCliente,
        idPais:element.idPais,
        idEstado:element.idEstado,
        idCiudad:element.idCiudad,
        idTipoPersona:element.idTipoPersona,
        rfcCli:element.rfcCliente,
        razSocCli:element.razSocCliente,
        calleCli:element.calleCliente,
        numCli:element.numCliente,
        colCli:element.colCliente,
        cpCli:element.cpCliente,
        pWebCli:element.pWebCliente,
        comCli:element.comCliente,
        refCli:element.refCliente,
        idUser:this.clienteDetalle.idUser
      }
    });
    this.SelectEstado(this.clienteDetalle.idPais);
    this.paisTem = this.clienteDetalle.idPais;
    this.SelectCiudad(this.clienteDetalle.idEstado);
  }

  SelectTipo(){
    this.http.tipoCliente().subscribe(result=>{
      this.limSelecTip(result);
    },
    error=>{ console.log(<any>error); });
  }

  limSelecTip(Tipos){
    if(this.clienteDetalle.idTipoCli == 1){
      this.tipo = Tipos.splice(1,1);
    }else if(this.clienteDetalle.idTipoCli == 2){
      this.tipo = Tipos.splice(1);
      this.tipo = this.tipo.splice(0,2);
    }else{
      var temp = Tipos.splice(3);
      this.tipo = Tipos.splice(1,1);
      temp.forEach(element => {
        this.tipo.push(element);
      });
    }
  }  

  SelectGiro(){
    this.http.Giro().subscribe(result=>{
      this.giro = result;
    },
    error=>{ console.log(<any>error); });
  }

  SelectEspecialidad(){
    this.http.Especialidad().subscribe(result=>{
      this.especialidad = result;
    },
    error=>{ console.log(<any>error); });
  }

  TipoP(){
    this.http.TipoPersonas().subscribe(result=>{
      this.tipop = result;
    }, 
    error =>{console.log(<any>error);
    });
  }

  SelectPais(){
    this.http.Paises().subscribe(result=>{
      this.pais = result;
    },
    error=>{ console.log(<any>error); });
  }

  SelectEstado(idPais){
    this.paisTem = idPais;
    this.http.EstadoFiltro(idPais).subscribe(result=>{
      this.estado = result;
    },
    error=>{ console.log(<any>error); });
  }

  SelectCiudad(idEstado){
    this.http.CiudadFiltro(idEstado,this.paisTem).subscribe(result=>{
      this.ciudad = result;
    },
    error=>{ console.log(<any>error); });
  }

  Actualizar(){
    this.http.ClienteEditar(this.clienteDetalle).subscribe(result=>{
      console.log(result);
      this.util.NavegacionDinamica(result,"cliente");
    },
    error=>{ console.log(<any>error); });
  }

  Cancelar(){
    this.clienteDetalle = null;
    this.util.navegacion("cliente");
  }
}
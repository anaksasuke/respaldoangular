import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { UtilService } from '../../../util.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ClientesService } from '../../../Servicios/clientes.service';
import { Descarte } from '../../../Modelos/ClienteDescarte';
import { CreateComponent } from '../create/create.component';
@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss']
})

export class ShowComponent implements OnInit {

  constructor(public http:ClientesService,public util:UtilService,public dialog:MatDialog) { }

  Columns =['acciones','compCliente','tipoCliente','telFijoCliente','giroCliente','espCliente','ciudad','fAltaCliente','fModCliente','usuario'];
  dataSource:any;
  valorTipoDes;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  descarte = new Descarte(0,0,0);

  ngOnInit() {
    this.renderTable();
    this.descarte.idUsuario = localStorage.Usuario;
  }

  renderTable(){
    this.http.Cliente().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, error=>{ console.log(<any>error); });
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  OpendDialog(row){
    this.descarte.idCliente = row.idCliente;
    const DialogRef = this.dialog.open(descartePop,{
      width:"auto",
      height:"auto",
      data:this.descarte
    });
    DialogRef.afterClosed().subscribe(result=>{
      this.renderTable();
    });
  }

  Cuenta(row){
    this.util.getDatos(row.idCliente);
    this.util.navegacion('configuraciones/cuentab');
  }

  Contacto(row){
    this.util.getDatos(row.idCliente);
    this.util.navegacion('cliente/contactos');
  }

  obtenerCli(row){
    this.descarte.idCliente = row.idCliente;
    this.http.DatosCliente(this.descarte.idCliente).subscribe(result=>{
      this.util.getDatos(result);
      this.util.navegacion('cliente/editar');
    },error=>{console.error(<any>error);});
  }

  Reset(){
    this.descarte = {idCliente:0,idDescarte:0,idUsuario:this.descarte.idUsuario};
  }

  Crear(){    
    const DialogRef = this.dialog.open(CreateComponent, {
      width:"auto",
      height:"auto"
    });
    DialogRef.afterClosed().subscribe(result=>{
      this.renderTable();
    });
  }
}


@Component({
  selector:"descartePop",
  templateUrl:"descartePop.html"
})

export class descartePop implements OnInit {
  constructor(public modal:MatDialogRef<descartePop>,public http:ClientesService,@Inject(MAT_DIALOG_DATA) public descarte:Descarte){}
  tipoDes;
  ngOnInit(){
    this.TipoDescarte();
  }

  TipoDescarte(){
    this.http.RazonDescarte().subscribe(result=>{
      this.tipoDes = Object.values(result);
    }, error => { console.log(<any>error); });
  }

  Descartar(){
    this.http.Descarte(this.descarte).subscribe(result=>{
     this.modal.close();
    }, error => { console.log(<any>error); });
  }

  Cancelar(){
    this.modal.close();
  }

}
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Aclasificaciones } from '../../../Modelos/Aclasificaciones';
import { UtilService } from '../../../util.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-aclasificaciones',
  templateUrl: './aclasificaciones.component.html',
  styleUrls: ['./aclasificaciones.component.scss']
})
export class AclasificacionesComponent implements OnInit {

  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog) { }
  public aclasificaciones = new Aclasificaciones ( 0, '', '', 0 );
  Columns = ['clasArticulo', 'Acciones'];
  dataSource:any;
  title="Nueva clasificación";
  @ViewChild(MatPaginator,{static:true})paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;


  ngOnInit() {
    this.RenderTable();
    this.aclasificaciones.idUser = localStorage.Usuario;
  }

  RenderTable(){
    this.http.Aclasificaciones().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;    
      this.dataSource.sort = this.sort;
    },
    error=>{
      console.log(<any>error);
    });
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    this.aclasificaciones.idclas = row.idClasArticulo;
    this.aclasificaciones.clas = row.clasArticulo;
    this.aclasificaciones.idUser = this.aclasificaciones.idUser;
    this.openDialog();
  }


  Reset(){
    this.aclasificaciones= { idclas:0, clas:"", nclas:"", idUser:this.aclasificaciones.idUser};
  }

  Eliminar(row){
    this.aclasificaciones.idclas = parseInt(row.idClasArticulo);
    this.aclasificaciones.clas = row.clasArticulo;
    this.aclasificaciones.nclas = row.nclas;
    this.http.AclasificacionesD(this.aclasificaciones).subscribe(result=>{
      this.RenderTable();
      console.log(result);
    },
    error=>{
      console.log(<any>error);});
  }

  openDialog():void{
    const dialogRef = this.dialog.open(aclasificacionesModal,{
      width:"400",
      height:"400",
      data: this.aclasificaciones
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    })
  }

  }

  @Component({
    selector:"aclasificacionesModal",
    templateUrl: "aclasificacionesModal.html"
  })
  export class aclasificacionesModal implements OnInit{
    constructor(public modal:MatDialogRef<aclasificacionesModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public aclasificaciones: Aclasificaciones){}

    ngOnInit(){

    }

    onNoClick():void{
      this.modal.close();
    }

    Nuevo(){
      this.http.AclasificacionesC(this.aclasificaciones).subscribe(result=>{
        console.log(result);
        this.modal.close();
      },
      error=>{
        console.log(<any>error);
      });
    }

    Editar(){
      this.http.AclasificacionesU(this.aclasificaciones).subscribe(result=>{
        console.log(result);
        this.modal.close();
      },
      error=>{
        console.log(<any>error);});
    }
  }
import { Component, OnInit,ViewChild, Inject} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Areas } from '../../../Modelos/Areas';
import { UtilService } from '../../../util.service';

@Component({
  selector: 'app-areas',
  templateUrl: './areas.component.html',
  styleUrls: ['./areas.component.scss']
})
export class AreasComponent implements OnInit {

  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog) { }
  area = new Areas(0,'',0);
  Columns = ['areaCotizacion','Acciones'];
  dataSource:any;
  title="Nueva Area";
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  
  ngOnInit() {
    this.RenderTable();
    this.area.idUser =  localStorage.Usuario;
  }

  RenderTable(){
    this.http.Areas().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{
      console.log(<any>error);
    });
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator){
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    this.area = {idAreaCotizacion:row.idAreaCotizacion,areaCotizacion:row.areaCotizacion,idUser:this.area.idUser}
    this.openDialog();
  }
  

  Reset(){
    this.area = {idAreaCotizacion:0,areaCotizacion:"",idUser:this.area.idUser};
  }

  Delet(row){
    this.area = {idAreaCotizacion:row.idAreaCotizacion,areaCotizacion:row.areaCotizacion,idUser:this.area.idUser}
    this.http.AreasDelet(this.area).subscribe(result=>{
     this.RenderTable();
     console.log(result);
     },error=>{
      console.log(<any>error);
     }); 
  }

  openDialog():void{
    const dialogRef = this.dialog.open(areaModal,{
      width:"400",
      height:"400",
      data: this.area
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    })
  }

}

@Component({
  selector:"areaModal",
  templateUrl:"areaModal.html"
})
export class areaModal implements OnInit{
constructor(public modal:MatDialogRef<areaModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public area:Areas){}

ngOnInit(){

}

onNoClick():void{
  this.modal.close();
}

Nuevo(){
  this.http.AreasCrear(this.area).subscribe(result=>{
    this.modal.close();},
  error=>{
    console.log(<any>error);});
}

Editar(){
  this.http.AreasEditar(this.area).subscribe(result=>{
    console.log(result);
    this.modal.close();
  },
  error=>{
    console.log(<any>error);});
}



}

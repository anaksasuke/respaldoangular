import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Banco } from '../../../Modelos/Banco';
import { UtilService } from '../../../util.service';


@Component({
  selector: 'app-banco',
  templateUrl: './banco.component.html',
  styleUrls: ['./banco.component.scss']
})
export class BancoComponent implements OnInit {

  constructor(public http:ConfiguracionesService, public util:UtilService,public dialog:MatDialog) { }
  public banco = new Banco(0,0,"","","",0);
  Columns = ['rfcBanco','nomBanco','razonBanco','Acciones'];
  dataSource:any;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;


  ngOnInit() {
    this.RenderTable();
    this.banco.idUser = localStorage.Usuario;
  }
  
  RenderTable(){
    this.http.Banco().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },error=>{console.log(<any>error);});
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  opendDialog(){
    const dialogRef = this.dialog.open(bancoPop,{
      width:'auto',
      height:'auto',
      data:this.banco
    });

    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    });
  }

  Cargar(row){
    this.banco=  {rfc:row.rfcBanco, nomBanco:row.nomBanco, razonBanco:row.razonBanco, idBanco: row.idBanco,idTipo:row.idTipoBanco, idUser:this.banco.idUser};
    this.opendDialog();
  }

  Reset(){
    this.banco=  {rfc:"", nomBanco:"", razonBanco:"", idBanco:0,idTipo:0, idUser:this.banco.idUser};
  }

  Eliminar(row){
    this.banco=  {rfc:row.rfcBanco, nomBanco:row.nomBanco, razonBanco:row.razonBanco, idBanco: row.idBanco,idTipo:row.idTipoBanco, idUser:this.banco.idUser};
    this.http.BancoDelet(this.banco).subscribe(result=>{
      this.util.Borrar();
      this.RenderTable();
    },error=>{console.log(<any>error);});
  }

}


@Component({
  selector:"bancoPop",
  templateUrl:"bancoPop.html"
})

export class bancoPop implements OnInit {
  constructor(public Modal:MatDialogRef<bancoPop>,public http:ConfiguracionesService,public util:UtilService,@Inject(MAT_DIALOG_DATA) public banco:Banco){}
  tipoB;
  ngOnInit(){
    this.TipoBanco();
  }

  TipoBanco(){
    this.http.BancoTipo().subscribe(result=>{
      this.tipoB = result;
    },error=>{console.log(<any>error);});
  }

  Salir(){
    this.Modal.close();
  }

  Nuevo(){
    this.http.BancoCrear(this.banco).subscribe(result=>{
      this.Salir();
      this.util.guardar();
      console.log(result);
     },error=>{console.log(<any>error);});
  }

  Editar(){
    this.banco.idUser = this.banco.idUser;
    this.http.BancoEditar(this.banco).subscribe(result=>{
      this.Salir();
      this.util.guardar();
      console.log(result);
    },error=>{console.log(<any>error);});
  }

}



import { Component, OnInit,ViewChild, Inject} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { CategoriaArticulo } from '../../../Modelos/CategoriasArticulo';
import { UtilService } from '../../../util.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-categorias-articulo',
  templateUrl: './categorias-articulo.component.html',
  styleUrls: ['./categorias-articulo.component.scss']
})

export class CategoriasArticuloComponent implements OnInit {

  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog) { }
  categoriArticulo = new CategoriaArticulo('',0,0);
  Columns = ['catArticulo','Acciones'];
  dataSource:any;
  title = "Nuevo Tipo";
  ;
  matris:any[]=[];
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  ngOnInit() {
    this.RenderTable();
    this.categoriArticulo.idUser = localStorage.Usuario;
  }

  RenderTable(){
    this.http.categoriasArticulo().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{
      console.log(<any>error);
    });
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter  = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    this.categoriArticulo = {categoria:row.catArticulo,idCategoria:row.idCatArticulo,idUser:this.categoriArticulo.idUser}
    this.openDialog();
  }

  Eliminar(row){
    this.categoriArticulo = {categoria:row.categoria,idCategoria:row.idCatArticulo,idUser:this.categoriArticulo.idUser}
    this.http.categoriasArticuloEliminar(this.categoriArticulo).subscribe(result=>{
    this.RenderTable();
      console.log(result);
      },error=>{
      console.log(<any>error); }); 
  }

  Reset(){
    this.categoriArticulo={idCategoria:0,idUser:this.categoriArticulo.idUser,categoria:""};
  }

  openDialog():void{
    const dialogRef = this.dialog.open(categoriaArticuloModal,{
      width:"400",
      height:"400",
      data: this.categoriArticulo
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    })
  }

}

  @Component({
    selector:"categoriaArticuloModal",
    templateUrl:"categoriaArticuloModal.html"
  })
  export class categoriaArticuloModal implements OnInit{
    constructor(public modal:MatDialogRef<categoriaArticuloModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public categoriArticulo: CategoriaArticulo){}

    ngOnInit(){
    }

    onNoClick():void{
      this.modal.close();
    }

    Nuevo(){
      this.http.categoriasArticuloCrear(this.categoriArticulo).subscribe(result=>{
        this.modal.close();
       },error=>{
         console.log(<any>error);});
     }

     Editar(){
      this.http.categoriasArticuloEditar(this.categoriArticulo).subscribe(result=>{
        console.log(result);
        this.modal.close();
      },
      error=>{
        console.log(<any>error);});
    }

  }

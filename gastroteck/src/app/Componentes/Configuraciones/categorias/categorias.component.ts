import { Component, OnInit,ViewChild, Inject} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Categorias } from '../../../Modelos/Categorias';
import { UtilService } from '../../../util.service';
@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.scss']
})
export class CategoriasComponent implements OnInit {

  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog) { }
  categorias = new Categorias(0,'',0);
  Columns = ['catPolitica','Acciones'];
  dataSource:any;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  ngOnInit() {
    this.RenderTable();
    this.categorias.idUser = localStorage.Usuario;
  }

  RenderTable(){
    this.http.Categorias().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{
      console.log(<any>error);
    });
  }
  
  Filtro(filtroStr:string){
  filtroStr = filtroStr.trim().toLowerCase();
  this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator){
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    this.categorias={idCatPolitica:row.idCatPolitica, catPolitica:row.catPolitica,idUser:this.categorias.idUser}
    this.openDialog();
  }

  Reset(){ 
    this.categorias={idCatPolitica:0, catPolitica:"",idUser:this.categorias.idUser}
  }

  Delet(row){
    this.categorias = {idCatPolitica:row.idCatPolitica,catPolitica:row.catPolitica,idUser:this.categorias.idUser}
    this.http.CategoriasDelet(this.categorias).subscribe(result=>{
    this.RenderTable();
      console.log(result);
      },error=>{
      console.log(<any>error);
    }); 
  }

  openDialog():void{
    const dialogRef = this.dialog.open(categoriaModal,{
      width:"400",
      height:"400",
      data: this.categorias
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    })
  }
}

  @Component({
    selector:"categoriaModal",
    templateUrl:"categoriaModal.html"
  })
  export class categoriaModal implements OnInit{
    constructor(public modal:MatDialogRef<categoriaModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public categorias:Categorias){}
    
    ngOnInit(){
    }

    onNoClick():void{
      this.modal.close();
    }

    Nuevo(){
      this.http.CategoriasCrear(this.categorias).subscribe(result=>{
        console.log(result);
        this.modal.close();
      },
      error=>{
        console.log(<any>error); });
    }

    Editar(){
      this.http.CategoriasEditar(this.categorias).subscribe(result=>{
        console.log(result);
        this.modal.close();
      },
      error=>{
        console.log(<any>error);});
    }
    

  }

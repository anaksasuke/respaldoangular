import { Component, OnInit, ViewChild, Inject} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Convertidor } from '../../../Modelos/Convertidor'; // Modelo a utilizar
import { UtilService } from '../../../util.service';

@Component({
  selector: 'app-convertidor',
  templateUrl: './convertidor.component.html',
  styleUrls: ['./convertidor.component.scss']
})

export class ConvertidorComponent implements OnInit {
 
  public conversiones = new Convertidor(0, 0, 0, 0, 0); // instancia de la clase
  Columns = ['origenMoneda', 'destinoMoneda', 'convMoneda', 'Acciones']; // columnas de la tabla/vista de la bdd
  dataSource:any;
  title = "Nuevo tipo de cambio";
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog) { }

  ngOnInit() {
    this.conversiones.idUsuario = localStorage.Usuario;
    this.RenderTable();
  }

  RenderTable(){
    this.http.Conversiones().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{
      console.log(<any>error);
    });
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    console.log(row);
    this.conversiones = {idUsuario:this.conversiones.idUsuario,oriMoneda:row.oriMoneda,desMoneda:row.desMoneda,convMoneda:row.convMoneda,convertidor:1}
    this.openDialog();
  }

  Reset(){
    this.conversiones= {oriMoneda:0, desMoneda:0, convMoneda:0, idUsuario:this.conversiones.idUsuario, convertidor:0};
  }

  Eliminar(row){
    this.conversiones.oriMoneda = row.oriMoneda;
    this.conversiones.desMoneda = row.desMoneda;
    this.conversiones.convMoneda = row.convMoneda;
    this.conversiones.idUsuario = this.conversiones.idUsuario;
    this.http.ConversionesEliminar(this.conversiones).subscribe(result=>{
      this.RenderTable();
    },
    error=>{
      console.log(<any>error);
    });
  }

  openDialog():void{
    const dialogRef = this.dialog.open(convertidorModal,{
      width:"400",
      height:"400",
      data: this.conversiones
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    });
  }
}

@Component({
  selector:"convertidorModal",
  templateUrl:"convertidorModal.html"
})

export class convertidorModal implements OnInit{
 oriMoneda;
  desMoneda;
  constructor(public modal:MatDialogRef<convertidorModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA) public conversiones: Convertidor){}

  ngOnInit(){
    this.Moneda();
    this.Ubicador();
  }

 Ubicador(){
    if(this.conversiones.convertidor !=0){
      this.Moneda2(this.conversiones.oriMoneda);
    }
  }

  Moneda(){ //seleccionar primera moneda
    this.http.Monedas().subscribe(result=>{ //llamar a la funcion de la vista
      this.oriMoneda = Object.values(result);//asignacion del resultado a un objeto
    }, error=>{
      console.log(<any>error);
    });  
  }

  Moneda2(idMoneda){
    this.http.ConversionesMoneda(idMoneda).subscribe(result=>{ //llama a la funcion del servicio 
      this.desMoneda = result;  
    }, error=>{
      console.log(<any>error);
    });
  }

  onNoClick():void{
    this.modal.close();
  }

  Nuevo(){
    this.http.ConversionesCrear(this.conversiones).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },
    error=>{
    console.log(<any>error);
    });
  }

  Editar(){
    this.http.ConversionesEditar(this.conversiones).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },
    error=>{
      console.log(<any>error);
    });
    
  }

}

import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Cuentab } from '../../../Modelos/Cuentab';
import { UtilService } from '../../../util.service';

@Component({
  selector: 'app-cuentab',
  templateUrl: './cuentab.component.html',
  styleUrls: ['./cuentab.component.scss']
})
export class CuentabComponent implements OnInit {

  constructor(public http:ConfiguracionesService,public util:UtilService, public dialog:MatDialog) { }
  public cuentab = new Cuentab(0,0,0,0,0,0,0);
  Columns = ['nomBanco','rfcBanco','ctaCheques','clabe','fModCuenta','Acciones'];
  dataSource:any;
  title ="Nueva cuenta";
  selectBanco;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  
  ngOnInit(){
    this.cuentab.idUser = localStorage.Usuario;
    this.cuentab.idCliente = parseInt(this.util.showDatos());
    if(this.cuentab.idCliente == undefined){
      this.cuentab = {idBanco:0,idCliente:0,idUser:this.cuentab.idUser,ctaResp:0,Cta:0,moduloC:0, condicion:0}
    }else{
      this.cuentab = {idBanco:0,idCliente:this.cuentab.idCliente,idUser:this.cuentab.idUser,ctaResp:0,Cta:0,moduloC:1, condicion:0}
    }
    this.RenderTable();
  }

  RenderTable(){
    this.http.Cuentab(this.cuentab.idCliente).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{
      console.log(<any>error);
    });
  }
  
  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    if(row.clabe == ""){
      this.cuentab ={idCliente:row.idCliente,idBanco:row.idBanco,idUser:this.cuentab.idUser,Cta:row.ctaCheques,ctaResp:row.ctaCheques,moduloC:0,condicion:1}
    }
    else {
      this.cuentab ={idCliente:row.idCliente,idBanco:row.idBanco,idUser:this.cuentab.idUser,Cta:row.clabe,ctaResp:row.clabe,moduloC:0,condicion:1}
    }
    if(this.cuentab.idCliente != 0){
      this.cuentab.moduloC = 1;
    }
   this.openDialog();
  }

  Reset(){
    this.title = "Nueva cuenta bancaria";
    if(this.cuentab.idCliente == 0){
      this.cuentab = {idCliente:0,idBanco:0,idUser:this.cuentab.idUser,Cta:0,ctaResp:0,moduloC:0, condicion:0}
    }else{
      this.cuentab = {idCliente:this.cuentab.idCliente,idBanco:0,idUser:this.cuentab.idUser,Cta:0,ctaResp:0,moduloC:1, condicion:0}
    }
  }

  Eliminar(row){
    this.cuentab ={idCliente:row.idCliente,idBanco:row.idBanco,idUser:this.cuentab.idUser,Cta:row.ctaCheques,ctaResp:row.ctaCheques,moduloC:0,condicion:1}
    this.http.CuentabDelet(this.cuentab).subscribe(result=>{
      console.log(result);
      this.RenderTable();
    },error=>{console.log(<any>error);})
  }

  Regresar(){
    this.util.navegacion("/cliente")
  }

  openDialog():void{
    const dialogRef = this.dialog.open(cuentabModal,{
      width:"400",
      height:"400",
      data: this.cuentab
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    })
  }

}
  
@Component({
  selector:"cuentabModal",
  templateUrl:"cuentabModal.html"
})
export class cuentabModal implements OnInit{
  constructor(public modal:MatDialogRef<cuentabModal>, public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public cuentab:Cuentab){}
  selectBanco;

  ngOnInit(){ 
    this.SelectBanco();
  }

  onNoClick():void{
    this.modal.close();
  }

  SelectBanco(){
    this.http.Banco().subscribe(result=>{
      this.selectBanco = result;
    },
    error=>{
      console.log(<any>error);
    })
  }

  Validar(param){
    var longitud = this.cuentab.Cta.toString().length;
    if((longitud >= 9 && longitud <= 11)||(longitud >= 16 && longitud < 19)){
      if(param == 1){
        this.Nuevo();
      }
      else {
        this.Editar();
      }
    }
    else{
      console.log("Error en clave");
    }
  }

  Nuevo(){
    this.http.CuentabCrear(this.cuentab).subscribe(result=>{
     this.modal.close();
    },
    error=>{
      console.log(<any>error);});
  }
  

  Editar(){
    this.http.CuentabEditar(this.cuentab).subscribe(result=>{
    this.modal.close();
    },
    error=>{
      console.log(<any>error);});    
  }
}
  
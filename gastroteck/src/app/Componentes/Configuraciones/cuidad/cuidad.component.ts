import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Ciudad } from '../../../Modelos/Ciudad';
import { UtilService } from '../../../util.service';


@Component({
  selector: 'app-cuidad',
  templateUrl: './cuidad.component.html',
  styleUrls: ['./cuidad.component.scss']
})
export class CuidadComponent implements OnInit {

  constructor(public http:ConfiguracionesService,public util:UtilService, public dialog:MatDialog) { }
  public ciudad = new Ciudad(0,0,0,0,"");
  Columns = ['pais','estado','ciudad','Acciones'];
  dataSource:any;
  title ="Nueva Ciudad";
  selPais;
  selEstado;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  ngOnInit() {
    this.RenderTable();
    this.ciudad.idUser =  localStorage.Usuario;
  }

  RenderTable(){
    this.http.Ciudad().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{
      console.log(<any>error);
    })
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter  = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    this.ciudad.idPais = row.idPais;
    this.ciudad.idEstado = row.idEstado;
    this.ciudad.idCiudad = row.idCiudad;
    this.ciudad.nomCiudad = row.ciudad;
    this.openDialog();
  }


  Eliminar(row){
    this.ciudad.idPais = row.idPais;
    this.ciudad.idEstado = row.idEstado;
    this.ciudad.idCiudad = row.idCiudad;
    this.http.CiudadDelet(this.ciudad).subscribe(result=>{
      this.RenderTable();
      console.log(result);
    },
    error=>{
      console.log(<any>error);
    });
  }

  Reset(){
    this.ciudad={idPais:0,idEstado:0,idCiudad:0,nomCiudad:"",idUser:this.ciudad.idUser}    
  }


  openDialog():void{
    const dialogRef = this.dialog.open(ciudadModal,{
      width:"400",
      height:"400",
      data: this.ciudad
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    });
  }

}

  @Component({
    selector:"ciudadModal",
    templateUrl:"ciudadModal.html"
  })
  export class ciudadModal implements OnInit{
    selPais; selEstado;
    constructor(public modal:MatDialogRef<ciudadModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA) public ciudad: Ciudad){}
    
    ngOnInit(){
      this.SelectPais();
      this.Ubicador();
    }

    Ubicador(){
      if(this.ciudad.idCiudad != 0){
        this.filtroEstado(this.ciudad.idPais);
      }
    }

    onNoClick():void{
      this.modal.close();
    
    }

    Nuevo(){
      this.http.CiudadCrear(this.ciudad).subscribe(result=>{
        console.log(result);
        this.modal.close();
      },
      error=>{
        console.log(<any>error); 
      });
    }

    Editar(){
      this.http.CiudadEditar(this.ciudad).subscribe(result=>{
        console.log(result);
        this.modal.close();
      },
      error=>{
        console.log(<any>error);
      });
    }

    SelectPais(){
      this.http.Paises().subscribe(result=>{
        this.selPais = result; 
      },
      error=>{
        console.log(<any>error);
      });
    }

    filtroEstado(idPais){
      this.http.EstadoFiltro(idPais).subscribe(result=>{
        this.selEstado = [];
        this.selEstado = result;
      },
      error=>{
        console.log(<any>error);
      });

    }
  }
import { Component, OnInit,ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Departamento } from '../../../Modelos/Departamento';
import { UtilService } from '../../../util.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-departamento',
  templateUrl: './departamento.component.html',
  styleUrls: ['./departamento.component.scss']
})

export class DepartamentoComponent implements OnInit {

  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog) { }
  departamento = new Departamento('',0,0);
  Columns = ['depArticulo','Acciones'];
  dataSource:any;
  title = "Nuevo Departamento";
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  ngOnInit() {
    this.RenderTable();
    this.departamento.idUser =  localStorage.Usuario;
  }

  RenderTable(){
    this.http.Departamento().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{
      console.log(<any>error);
    })
  }

  Filtro(filtro:string){
    filtro = filtro.trim().toLowerCase();
    this.dataSource.filter = filtro;
    if(this.dataSource.paginator){
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    this.departamento={idDep:row.idDepArticulo,nombre:row.depArticulo,idUser:this.departamento.idUser};
    this.openDialog();
  }

  Eliminar(row){
    this.http.DepartamentoDelet(this.departamento).subscribe(result=>{
      this.RenderTable();
      console.log(result);
    },
    error=>{
      console.log(<any>error);
    });
  }
  
  Reset(){
    this.departamento = {idDep:0,nombre:"",idUser:this.departamento.idUser}; 
  }

  openDialog():void{
    const dialogRef = this.dialog.open(departamentosModal,{
      width:"400",
      height:"400",
      data: this.departamento
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    })
  }
}

  @Component({
    selector:"departamentosModal",
    templateUrl:"departamentosModal.html"
  })
  export class departamentosModal implements OnInit{
    constructor(public modal:MatDialogRef<departamentosModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public departamento: Departamento){}

    ngOnInit(){
    }

    onNoClick():void{
      this.modal.close();
    }

    Editar(){
      this.http.DepartamentoEditar(this.departamento).subscribe(result=>{
        this.modal.close();
        console.log(result);
      },
      error=>{
        console.log(<any>error);
      })
    }
  
    Nuevo(){
      this.http.DepartamentoCrear(this.departamento).subscribe(result=>{
        this.modal.close();
        console.log(result);
      },
      error=>{
        console.log(<any>error);
      });
    }
  

  }
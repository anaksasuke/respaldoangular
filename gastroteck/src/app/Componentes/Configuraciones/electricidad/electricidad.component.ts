import { Component, OnInit, ViewChild, Inject} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import {Electricidad } from '../../../Modelos/electricidad';
import { UtilService } from '../../../util.service';
@Component({
  selector: 'app-electricidad',
  templateUrl: './electricidad.component.html',
  styleUrls: ['./electricidad.component.scss']
})
export class ElectricidadComponent implements OnInit {

  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog) { }
  public electricidad = new Electricidad(0,0,'');
  Columns = ['eneElectrica','Acciones'];
  dataSource:any;
  title ="Nueva Fuente electrica ";
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  ngOnInit() {
    this.RenderTable();
    this.electricidad.idUsuario =  localStorage.Usuario;
  }

  RenderTable(){
    this.http.Electricidad().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort  =this.sort;
    },
    error=>{
      console.log(<any>error);});
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter  = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    this.electricidad.nombre = row.eneElectrica;
    this.electricidad.idElectricidad = row.idEneElectrica;
    this.openDialog();
  }

  Reset(){
    this.electricidad= {nombre:"",idElectricidad:0,idUsuario:this.electricidad.idUsuario};
  }

  Eliminar(row){
    this.electricidad.idElectricidad = row.idEneElectrica;
    this.http.ElectricidadDelet(this.electricidad).subscribe(result=>{
      this.RenderTable();
      console.log(result);
    },
    error=>{
      console.log(<any>error);
    });
  }

  openDialog():void{
    const dialogRef = this.dialog.open(electricidadModal,{
      width:"400",
      height:"400",
      data: this.electricidad
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    })
  }
}

  @Component({
    selector:"electricidadModal",
    templateUrl:"electricidadModal.html"
  })
  export class electricidadModal implements OnInit{
    constructor(public modal:MatDialogRef<electricidadModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public electricidad:Electricidad){}

    ngOnInit(){
    }

    onNoClick():void{
      this.modal.close();
    }

    Nuevo(){
      this.http.ElectricidadCrear(this.electricidad).subscribe(result=>{
        console.log(result);
        this.modal.close();
      },
      error=>{
        console.log(<any>error);});
    }

    Editar(){
      
      this.http.ElectricidadEdit(this.electricidad).subscribe(result=>{
        console.log(result);
        this.modal.close();
       },
      error=>{
        console.log(<any>error);});
    }
  }


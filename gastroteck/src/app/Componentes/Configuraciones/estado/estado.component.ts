import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Estado } from '../../../Modelos/Estado';
import { EstadoInter } from '../../../Interfaces/Estado';
import { UtilService } from '../../../util.service';


@Component({
  selector: 'app-estado',
  templateUrl: './estado.component.html',
  styleUrls: ['./estado.component.scss']
})
export class EstadoComponent implements OnInit {

  constructor(public http:ConfiguracionesService, public util:UtilService,public dialog:MatDialog) { }
  estado = new Estado(0,0,"",0,"");
  Columns = ['pais','estado','Acciones'];
  dataSource:any;
  title ="Nuevo Pais";
  selPais;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  ngOnInit() {
    this.RenderTable();
    this.estado.idUsuario = localStorage.Usuario;
  }


  RenderTable(){
    this.http.Estados().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort  =this.sort;
    },
    error=>{
      console.log(<any>error);
    });
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter  = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    this.title = "Editar Pais";
    this.estado = {idPais:row.idPais,idEstado:row.idEstado,EstadoNom:row.estado,idUsuario:this.estado.idUsuario,PaisNom:""};
    this.estado.idPais = row.idPais;
    this.estado.idEstado = row.idEstado;
    this.estado.EstadoNom = row.estado;
    this.openDialog(2); //El 2 indica que sera editado, El 1 Es para uno nuevo, El evento para el nuevo esta en el html.. Se hacen dos eventos clicks en el button de nuevo 
  }


  Eliminar(row){
    this.estado.idPais = parseInt(row.idPais);
    this.estado.idEstado = parseInt(row.idEstado);
    this.http.EstadosDelet(this.estado).subscribe(result=>{
      this.RenderTable();
    },
    error=>{
      console.log(<any>error);
    })
  }

  Reset(){
    this.estado = {idUsuario:this.estado.idUsuario,idPais:0,idEstado:0,PaisNom:"",EstadoNom:""};
  }

  openDialog(param):void { //Llamado al modal
    const dialogRef = this.dialog.open(ModalEstado,{
      width: '370px',
      height:'300px',
      data: {idUsuario:this.estado.idUsuario,idPais:this.estado.idPais,idEstado:this.estado.idEstado,EstadoNom:this.estado.EstadoNom,title:param}
    });

    dialogRef.afterClosed().subscribe(result =>{ //Evento que hace para cerrar el modal 
      console.log(result);
      this.RenderTable();
    });

  }
}


  //Aqui generamos el nuevo componente para el Material dialog 

  @Component({
    selector:'estadoModelo',
    templateUrl:'./estadoModelo.html',
  })

  export class ModalEstado implements OnInit{
    selPais;
    title;
    constructor(
      public Modal:MatDialogRef<ModalEstado>,public http:ConfiguracionesService,
      @Inject(MAT_DIALOG_DATA) public estado:EstadoInter){}

      ngOnInit(){
        this.SelectPais(); //Cargamos los paises 
        this.ValidadorEvento(); //Validamos para saber si editamos o cargamos 
        //Para hacer disabled un select hazlo desde el html [disabled]="elemento que valide si edita != 1"
      }
  
      ValidadorEvento(){
        if(this.estado.title == "1"){
            this.title = "Nuevo Estado";
            document.getElementById("Editar").style.display="none";
        }
        else{
            this.title = `Editando Estado ${this.estado.EstadoNom}`;
            document.getElementById("Guardar").style.display ="none";
        }
      }

      onNoClick():void {
        this.Modal.close();
      }

      SelectPais(){
        this.http.Paises().subscribe(result=>{
          this.selPais = result;
        },
        error=>{
          console.log(<any>error);
        });
      }

      Editar(){
        this.http.EstadosEdit(this.estado).subscribe(result=>{
          this.Modal.close();
          console.log(result);
        },error=>{console.log(<any>error);});
      }

      Nuevo(){
        this.http.EstadosCrear(this.estado).subscribe(result=>{
          this.Modal.close();
          console.log(result);
        },
        error=>{
          console.log(<any>error);
        });
      }

  }


import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Excepcion } from '../../../Modelos/Excepcion';
import { UtilService } from '../../../util.service';

@Component({
  selector: 'app-excepcion',
  templateUrl: './excepcion.component.html',
  styleUrls: ['./excepcion.component.scss']
})
export class ExcepcionComponent implements OnInit {
  excepciones = new Excepcion(0, '', '', 0, 0, 0);
  Columns = ['excepcion', 'nomExcepcion', 'codSatExcepcion', 'margExcepcion', 'Acciones'];
  dataSource:any;
  title = "Nueva Excepcion";
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  constructor(public http:ConfiguracionesService, public util:UtilService) { }

  ngOnInit() {
    this.RenderTable();
    this.excepciones.idUsuario =  localStorage.Usuario;
  }

  RenderTable(){
    this.http.Excepciones().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{
      console.log(<any>error);
    });
  }

  Filtro(filtro:string){
    filtro = filtro.trim().toLowerCase();
    this.dataSource.filter = filtro;
    if(this.dataSource.paginator){
      this.dataSource.paginator.firstPage();
    }
  }

  Nuevo(){
    this.http.ExcepcionCrear(this.excepciones).subscribe(result=>{
      console.log(result);
      this.RenderTable();
    },
    error=>{
      console.log(<any>error);
    });
  }

  Cargar(row){
    this.excepciones.idExcepcion = row.idExcepcion;
    this.excepciones.excepcion = row.excepcion;
    this.excepciones.nomExcepcion = row.nomExcepcion;
    this.excepciones.codSatExcepcion = row.codSatExcepcion;
    this.excepciones.margExcepcion = row.margExcepcion;
    this.excepciones.idUsuario = this.excepciones.idUsuario;
    this.title = "Editar Excepcion";
    document.getElementById("Actualizar").style.display="inline";
    document.getElementById("Nuevo").style.display="none";
  }

  Editar(){
    this.http.ExcepcionEditar(this.excepciones).subscribe(result=>{
      this.RenderTable();
      console.log(result);
    },
    error=>{
      console.log(<any>error);
    });
  }

  Reset(){
    this.excepciones = { idExcepcion:null, excepcion:null, nomExcepcion:null, codSatExcepcion:null, margExcepcion:null, idUsuario:this.excepciones.idUsuario }
    this.title = "Nueva Excepcion";
    document.getElementById("Nuevo").style.display="inline";
    document.getElementById("Actualizar").style.display="none";
  }

  Eliminar(row){
    this.excepciones.idExcepcion = row.idExcepcion;
    this.http.ExcepcionEliminar(this.excepciones).subscribe(result=>{
      this.ngOnInit();
    }, 
    error=>{
      console.log(<any>error);
    });
  }
}
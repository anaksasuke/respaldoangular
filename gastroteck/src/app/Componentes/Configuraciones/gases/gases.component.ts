import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Gases } from '../../../Modelos/gases';
import { UtilService } from '../../../util.service';
@Component({
  selector: 'app-gases',
  templateUrl: './gases.component.html',
  styleUrls: ['./gases.component.scss']
})
export class GasesComponent implements OnInit {
  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog){}
  public gases = new Gases(0,0,'');
  Columns = ['eneCalorifica','Acciones'];
  dataSource:any;
  title ="Nuevo Gas";
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;


  ngOnInit(){
    this.RenderTable();
    this.gases.idUsuario=  localStorage.Usuario;
  }

  RenderTable(){
    this.http.Calor().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{
      console.log(<any>error);
    });
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    this.title = "Editar gases";
    this.gases.nombre = row.eneCalorifica;
    this.gases.idGases = row.idEneCalorifica;
    this.openDialog();
  }
  
  Reset(){
      this.gases= {nombre:"", idGases:0, idUsuario:this.gases.idUsuario};
      this.title = "Nuevo gas";
      }

  Eliminar(row){
      this.gases.idGases = row.idEneCalorifica;      
      this.http.CalorDelet(this.gases).subscribe(result=>{
        this.RenderTable();
        console.log(result);
      },
      error=>{
        console.log(<any>error);
      });
  }

  openDialog():void{
    const dialogRef = this.dialog.open(gasesModal,{
      width:"400",
      height:"400",
      data: this.gases
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    })
  }
  
}

  @Component({
    selector:"gasesModal",
    templateUrl:"gasesModal.html"
  })
  export class gasesModal implements OnInit{
    constructor(public modal:MatDialogRef<gasesModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public gases:Gases){}

    ngOnInit(){
    }

    onNoClick():void{
      this.modal.close();
    }

    Nuevo(){
      this.http.CalorCrear(this.gases).subscribe(result=>{
        console.log(result);
        this.modal.close();
      },
      error=>{
      console.log(<any>error);});
    }

    Editar(){
      this.http.CalorEditar(this.gases).subscribe(result=>{
      console.log(result);
      this.modal.close();
      },
      error=>{
        console.log(<any>error);});
    }
  }
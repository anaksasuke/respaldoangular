import { Component, OnInit,ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Grupos } from '../../../Modelos/Grupos';
import { UtilService } from '../../../util.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-grupo',
  templateUrl: './grupo.component.html',
  styleUrls: ['./grupo.component.scss']
})
export class GrupoComponent implements OnInit {
  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog) { }
  grupo = new Grupos(0,-1 ,'',0, 0);
  Columns = ['depArticulo','gruArticulo','Acciones'];
  dataSource:any;
  title = "Nuevo Groupo";
  SelectDep;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  ngOnInit() {
    this.RenderTable();
    this.grupo.idUser =  localStorage.Usuario;
  }

  RenderTable(){
    this.http.Grupos().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{
      console.log(<any>error);
    });
  }

  Filtro(filtro:string){
    filtro = filtro.trim().toLowerCase();
    this.dataSource.filter = filtro;
    if(this.dataSource.paginator){
      this.dataSource.paginator.firstPage();
    }
  }


  Cargar(row){
    this.grupo={idGroup:row.idGruArticulo,idDep:row.idDepArticulo,idUser:this.grupo.idUser,nombreGroup:row.gruArticulo, grupos:1};
    this.openDialog();
  }

  Eliminar(row){
    this.grupo={idGroup:row.idGruArticulo,idDep:row.idDepArticulo,idUser:this.grupo.idUser,nombreGroup:row.gruArticulo, grupos:row.grupo};
    this.http.GruposDel(this.grupo).subscribe(result=>{
      this.RenderTable();
      console.log(result);
    },error=>{
      console.log(<any>error);
    })
  }

  Reset(){
    this.grupo={idGroup:0,idDep:0,idUser:this.grupo.idUser,nombreGroup:"", grupos:0};
  }

  openDialog():void{
    const dialogRef = this.dialog.open(grupoModal,{
      width:"400",
      height:"400",
      data: this.grupo
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    })
  }
}

  @Component({
    selector:"grupoModal",
    templateUrl:"grupoModal.html"
  })
  export class grupoModal implements OnInit{
    constructor(public modal:MatDialogRef<grupoModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public grupo:Grupos){}
    SelectDep;
    ngOnInit(){
      this.selectDepartamento();
    }

    onNoClick():void{
      this.modal.close();
    }

    Nuevo(){
      this.http.GruposCrear(this.grupo).subscribe(result=>{
        this.modal.close();
        console.log(result);
      },
      error=>{
        console.log(<any>error);
      });
    }

    Editar(){
      this.http.GruposEditar(this.grupo).subscribe(result=>{
        this.modal.close();
        console.log(result);
      },
      error=>{
        console.log(<any>error);
      });
    }

    selectDepartamento(){
      this.http.Departamento().subscribe(result=>{
        this.SelectDep = Object.values(result);
      },
      error=>{
        console.log(<any>error);
      });
    }
    

  }
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Iva } from '../../../Modelos/Iva';
import { UtilService } from '../../../util.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-iva',
  templateUrl: './iva.component.html',
  styleUrls: ['./iva.component.scss']
})
export class IvaComponent implements OnInit {
  ivas = new Iva(0, 0, 0);
  Columns = ['iva', 'Acciones'];
  dataSource:any;
  title = "Nuevo Iva";
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog) { }

  ngOnInit() {
    this.RenderTable();
    this.ivas.idUsuario =  localStorage.Usuario;
  }

  RenderTable(){
    this.http.Ivas().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{
      console.log(<any>error);
    });
  }

  Filtro(filtro:string){
    filtro = filtro.trim().toLowerCase();
    this.dataSource.filter = filtro;
    if(this.dataSource.paginator){
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    this.ivas.idIva = row.idIva;
    this.ivas.iva = row.iva;
    this.openDialog();
  }

  Reset(){
    this.ivas = {idIva:0, iva:0, idUsuario:this.ivas.idUsuario}
  }

  Eliminar(row){
    this.ivas.idIva = row.idIva;
    this.ivas.iva = row.iva;
    this.http.IvasEliminar(this.ivas).subscribe(result=>{
      console.log(result);
      this.RenderTable();
    }, 
    error=>{
      console.log(<any>error);
    });
  }

  openDialog():void{
    const dialogRef = this.dialog.open(ivaModal,{
      width:"400",
      height:"400",
      data: this.ivas
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    })
  }
}

@Component({
  selector:"ivaModal",
  templateUrl:"ivaModal.html"
})
export class ivaModal implements OnInit{
  constructor(public modal:MatDialogRef<ivaModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public ivas:Iva){}

  ngOnInit(){

  }

  onNoClick():void{
    this.modal.close();
  }

  Nuevo(){
    this.http.IvaCrear(this.ivas).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },
    error=>{
      console.log(<any>error);
    });
    console.log(this.ivas);
  }

  Editar(){
    this.http.IvasEditar(this.ivas).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },
    error=>{
      console.log(<any>error);
    });
  }


}
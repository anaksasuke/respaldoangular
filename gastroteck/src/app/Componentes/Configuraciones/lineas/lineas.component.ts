import { Component, OnInit, ViewChild, Inject} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Linea } from '../../../Modelos/Lineas';
import { UtilService } from '../../../util.service';
@Component({
  selector: 'app-lineas',
  templateUrl: './lineas.component.html',
  styleUrls: ['./lineas.component.scss']
})
export class LineasComponent implements OnInit {
  linea = new Linea(-1,-1,0,'',0, 0);
  Columns = ['depArticulo','gruArticulo','linArticulo','Acciones'];
  dataSource:any;
  title='Nueva Linea';
  SelectDep;
  SelectGro;
  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog) { }
 
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  ngOnInit() {
    this.RenderTable();
    this.linea.idUser =  localStorage.Usuario;
  }

  RenderTable(){
    this.http.Lineas().subscribe(result=>{  
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this,this.dataSource.sort = this.sort;
    },error=>{
      console.log(<any>error);
    });
  }

  Filtro(filtro:string){
    filtro = filtro.trim().toLowerCase();
    this.dataSource.filter = filtro;
    if(this.dataSource.paginator){
      this.dataSource.paginator.firstPage();
    }
  }


  Cargar(row){
    this.linea = {idDepar:row.idDepArticulo,idGrupo:row.idGruArticulo,idLin:row.idLinArticulo,nombLin:row.linArticulo,idUser:this.linea.idUser, linea:1};
    this.openDialog();
  }

  Eliminar(row){
    this.linea = {idDepar:row.idDepArticulo,idGrupo:row.idGruArticulo,idLin:row.idLinArticulo,nombLin:row.linArticulo,idUser:this.linea.idUser,linea:row.linea};
    this.http.LineasDelete(this.linea).subscribe(result=>{
      this.RenderTable();
      console.log(result);
    },
    error=>{
      console.log(<any>error);
    });
  }

  Reset(){
    this.linea = {idDepar:0,idGrupo:0,idLin:0,nombLin:"",idUser:this.linea.idUser, linea:0};
  }

  openDialog():void{
    const dialogRef = this.dialog.open(lineasModal,{
      width:"400",
      height:"400",
      data: this.linea
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    });
  }
}

  @Component({
    selector:"lineasModal",
    templateUrl:"lineasModal.html"
  })
  export class lineasModal implements OnInit{
    constructor(public modal:MatDialogRef<lineasModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public linea:Linea){}
    SelectDep;
    SelectGro;

    ngOnInit(){
      this.selectDepartamento();
      this.Ubicador();
    }

    onNoClick():void{
      this.modal.close();
    }

    Ubicador(){
      if(this.linea.idLin !=0){
        this.selectGroupo(this.linea.idDepar);
      }
    }

    Nuevo(){
      this.http.LineasCrear(this.linea).subscribe(result=>{
        this.modal.close();
        console.log(result);
      },
      error=>{
        console.log(<any>error);
      });
    }

    Editar(){

      this.http.LineasEditar(this.linea).subscribe(result=>{
        this.modal.close();
        console.log(result);
      },
      error=>{
        console.log(<any>error);
      });
    }

    selectDepartamento(){
      this.http.Departamento().subscribe(result=>{
        this.SelectDep = Object.values(result);
      },
      error=>{
        console.log(<any>error);
      });
    }
  
    selectGroupo(idDepartamento){
      this.http.ObtenerGrupo(idDepartamento).subscribe(result=>{
        this.SelectGro = Object.values(result);
      },
      error=>{
        console.log(<any>error);
      });
    }
  }

import { Component, OnInit, ViewChild, Inject} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import{ Marcas } from '../../../Modelos/Marcas';
import { UtilService } from '../../../util.service';
@Component({
  selector: 'app-marcas',
  templateUrl: './marcas.component.html',
  styleUrls: ['./marcas.component.scss']
})
export class MarcasComponent implements OnInit {

  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog) { }
  public marcas = new Marcas(0, '', 0);
  Columns = ['marcArticulo', 'Acciones'];
  dataSource:any;
  title="Nuevo";
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  
  
  ngOnInit() {
    this.RenderTable();
    this.marcas.idUser =  localStorage.Usuario;
  }

  RenderTable(){
    this.http.Marcas().subscribe(result=>{ //agregar al servicios.service.ts
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{ console.log(<any>error); });
  }
  
  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator){
      this.dataSource.paginator.firstPage();
    }
  }


  Cargar(row){
    this.marcas.idMarcArticulo = row.idMarcArticulo;
    this.marcas.marcArticulo = row.marcArticulo;
    this.marcas.idUser = this.marcas.idUser;
    this.openDialog();
  }

  Reset(){
    this.marcas= {idMarcArticulo:0, marcArticulo:"", idUser:this.marcas.idUser};
  }

  Eliminar(row){
    this.marcas.idMarcArticulo = parseInt(row.idMarcArticulo);
    this.marcas.marcArticulo = row.marcArticulo;
    this.http.MarcasDelet(this.marcas).subscribe(result=>{ //crearlo en servicios.service.ts
      this.RenderTable();
      console.log(result);
    },
    error=>{ console.log(<any>error); });
  }

  openDialog():void{
    const dialogRef = this.dialog.open(marcasModal,{
      width:"400",
      height:"400",
      data: this.marcas
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    })
  }
}

  @Component({
    selector:"marcasModal",
    templateUrl:"marcasModal.html"
  })
  export class marcasModal implements OnInit{
    constructor(public modal:MatDialogRef<marcasModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public marcas:Marcas){}

    ngOnInit(){
    }

    onNoClick():void{
      this.modal.close();
    }

    Nuevo(){
      this.http.MarcasCrear(this.marcas).subscribe(result=>{ //crearlas en el servicio
        console.log(result);
        this.modal.close();
      },
      error=>{ console.log(<any>error); });
    }

    Editar(){
      this.http.MarcasEditar(this.marcas).subscribe(result=>{ //crear y agregar en el servicios.service
       console.log(result);
       this.modal.close();
      },
      error=>{ console.log(<any>error); });
    }

  }

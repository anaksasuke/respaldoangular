import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Medida } from '../../../Modelos/Medida';
import { UtilService } from '../../../util.service';
@Component({
  selector: 'app-medida',
  templateUrl: './medida.component.html',
  styleUrls: ['./medida.component.scss']
})
export class MedidaComponent implements OnInit {

  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog) { }
  public medida = new Medida(0, "", "", 0);
  Columns = ['medArticulo', 'cveMedArticulo', 'Acciones'];
  dataSource:any;
  title ="Nueva Medida";
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  ngOnInit() {
  this.RenderTable();
  this.medida.idUser =  localStorage.Usuario;
  }

  RenderTable(){
    this.http.Medida().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{
      console.log(<any>error);
    });
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  /*Nuevo(){
    this.http.MedidaCrear(this.medida).subscribe(result=>{
      this.RenderTable();
      console.log(result);
    },
    error=>{
      console.log(<any>error);
    });
  }*/

  Cargar(row){
    this.medida.idMedArticulo = row.idMedArticulo;
    this.medida.cveMedArticulo = row.cveMedArticulo;
    this.medida.medArticulo = row.medArticulo;
    this.openDialog();
  }

  /*Editar(){
    this.http.MedidaEditar(this.medida).subscribe(result=>{
      console.log(result);
      this.RenderTable();
    },
    error=>{
      console.log(<any>error);
    });
  }*/

  Reset(){
    this.medida= {idMedArticulo:0, cveMedArticulo:"", medArticulo:"", idUser:this.medida.idUser};
  }

  Eliminar(row){
    this.medida.idMedArticulo = parseInt(row.idMedArticulo);
    this.medida.cveMedArticulo = row.cveMedArticulo;
    this.medida.medArticulo = row.medArticulo;
    this.http.MedidaDelet(this.medida).subscribe(result=>{
      this.RenderTable();
      console.log(result);
    },
    error=>{
      console.log(<any>error);
    });
  }

  openDialog():void{
    const dialogRef = this.dialog.open(medidaModal,{
      width:"400",
      height:"400",
      data: this.medida
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    })
  }
}
@Component({
  selector:"medidaModal",
  templateUrl:"medidaModal.html"
})
export class medidaModal implements OnInit{
  constructor(public modal:MatDialogRef<medidaModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public medida:Medida){}

  ngOnInit(){
  }

  onNoClick():void{
    this.modal.close();
  }

  Nuevo(){
    this.http.MedidaCrear(this.medida).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },
    error=>{
      console.log(<any>error); });
  }

  Editar(){
    this.http.MedidaEditar(this.medida).subscribe(result=>{
      console.log(result);
      this.modal.close();
       },
    error=>{
      console.log(<any>error);});
  }

}
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Moneda } from '../../../Modelos/Moneda';
import { UtilService } from '../../../util.service';

@Component({
  selector: 'app-moneda',
  templateUrl: './moneda.component.html',
  styleUrls: ['./moneda.component.scss']
})

export class MonedaComponent implements OnInit {
  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog){}
  public monedas = new Moneda(0, 0, '', '');
  Columns = ['moneda', 'cveMoneda', 'Acciones'];
  dataSource:any;
  title = "Nueva Moneda";
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  ngOnInit() {
    this.RenderTable();
    this.monedas.idUsuario =  localStorage.Usuario;
  }

  RenderTable(){
    this.http.Monedas().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{
      console.log(<any>error);
    });
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    this.monedas.moneda = row.moneda;
    this.monedas.idMoneda = row.idMoneda;    
    this.monedas.cveMoneda = row.cveMoneda;
    this.openDialog();
  }

  Reset(){
    this.monedas= {moneda:"", idMoneda:0, cveMoneda:"", idUsuario:this.monedas.idUsuario};
  }

  Eliminar(row){
    this.monedas.idMoneda = row.idMoneda;
    this.monedas.moneda = row.moneda;
    this.monedas.cveMoneda = row.cveMoneda;
    this.http.MonedasEliminar(this.monedas).subscribe(result=>{
      console.log(result);
      this.RenderTable();
    },
    error=>{
      console.log(<any>error);
    });
  }

  openDialog():void{
    const dialogRef = this.dialog.open(monedaModal,{
      width:"400",
      height:"400",
      data: this.monedas
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    })
  }
}

@Component({
  selector:"monedaModal",
  templateUrl:"monedaModal.html"
})

export class monedaModal implements OnInit{
  constructor(public modal:MatDialogRef<monedaModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public monedas:Moneda){}

  ngOnInit(){
  }

  onNoClick():void{
    this.modal.close();
  }

  Nuevo(){
    this.http.MonedasCrear(this.monedas).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },
    error=>{
    console.log(<any>error);
    });
  }

  Editar(){
    this.http.MonedasEditar(this.monedas).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },
    error=>{
      console.log(<any>error);
    });
  }


}


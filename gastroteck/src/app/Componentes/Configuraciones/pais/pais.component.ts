import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Pais } from '../../../Modelos/Pais';
import { UtilService } from '../../../util.service';


@Component({
  selector: 'app-pais',
  templateUrl: './pais.component.html',
  styleUrls: ['./pais.component.scss']
})
export class PaisComponent implements OnInit {

  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog) { }
  public pais = new Pais(0,0,'');
  Columns = ['pais','Acciones'];
  dataSource:any;
  title ="Nuevo Pais";
    @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  ngOnInit() {
    this.RenderTable();
    this.pais.idUsuario =  localStorage.Usuario;
  }


  RenderTable(){
    this.http.Paises().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort  =this.sort;
    },
    error=>{
      console.log(<any>error);
    });
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter  = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    this.pais.nombre = row.pais;
    this.pais.idPais =parseInt(row.idPais); 
    this.openDialog();
  }
  
  Reset(){
    this.pais = {nombre:"",idPais:0,idUsuario:this.pais.idUsuario};    
  }

  Eliminar(row){
    this.pais = {idPais:parseInt(row.idPais),nombre:row.pais,idUsuario:this.pais.idUsuario};
    this.http.PaisDelet(this.pais).subscribe(result=>{
      this.RenderTable();
      console.log(result);
    },
    error=>{
      console.log(<any>error);});
  }

  openDialog():void{
    const dialogRef = this.dialog.open(PaisModal,{
    width:"400",
    height:"400",
    data: {idUsuario:this.pais.idUsuario,idPais:this.pais.idPais,nombre:this.pais.nombre}
      });

      dialogRef.afterClosed().subscribe(result=>{
        this.RenderTable();
      });
  }

}

  @Component({
    selector:"PaisModal",
    templateUrl:"PaisModal.html"
  })

  export class PaisModal implements OnInit{
    constructor(public modal:MatDialogRef<PaisModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA) public PaisModal:Pais){}

    ngOnInit(){
    }

    onNoClick():void{
      this.modal.close();    
    }
    
    Nuevo(){
      this.http.PaisCrear(this.PaisModal).subscribe(result=>{
        console.log(result);
        this.modal.close();
      },error=>{
        console.log(<any>error);});
    }

   Editar(){
      this.http.PaisEditPrueba(this.PaisModal).subscribe(result=>{
        this.modal.close();
        console.log(result);
      },
      error=>{
        console.log(<any>error);
      });
    }

  

}

import { Component, OnInit, ViewChild, Inject} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import{ Plomerias } from '../../../Modelos/plomerias';
import { UtilService } from '../../../util.service';
@Component({
  selector: 'app-plomerias',
  templateUrl: './plomerias.component.html',
  styleUrls: ['./plomerias.component.scss']
})
export class PlomeriasComponent implements OnInit {

  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog) { }
  public plomerias = new Plomerias(0,0,'');
  Columns = ['tuberia','Acciones'];
  dataSource:any;
  title ="Nueva Plomeria";
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  ngOnInit() {
    this.RenderTable();
    this.plomerias.idUsuario =  localStorage.Usuario;
  }

  RenderTable(){
    this.http.Tuberias().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort  =this.sort;
    },
    error=>{
      console.log(<any>error);
   });
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter  = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
  this.title = "Editar Tuberia";
   this.plomerias.nombre=row.tuberia;
   this.plomerias.idPlomerias=row.idTuberia;
   this.openDialog();
  }

  Reset(){
    this.plomerias = {nombre:"",idPlomerias:0,idUsuario:this.plomerias.idUsuario};
  }

  Eliminar(row){
    this.plomerias.idPlomerias = row.idTuberia;
    this.http.TuberiaDelet(this.plomerias).subscribe(result=>{
      this.RenderTable();
      console.log(result);
    },
    error=>{
      console.log(<any>error);
    });
  }

  openDialog():void{
    const dialogRef = this.dialog.open(plomeriasModal,{
      width:"400",
      height:"400",
      data: this.plomerias
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    })
  }
}

  @Component({
    selector:"plomeriasModal",
    templateUrl:"plomeriasModal.html"
  })
  export class plomeriasModal implements OnInit{
    constructor(public modal:MatDialogRef<plomeriasModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public plomerias:Plomerias){}

    ngOnInit(){
    }

    onNoClick():void{
      this.modal.close();
    }

    Nuevo(){
      this.http.TuberiaCrear(this.plomerias).subscribe(result=>{
        console.log(result);
        this.modal.close();
      },
      error=>{
        console.log(<any>error);
      });
    }

    Editar(){
      this.http.TuberiaEditar(this.plomerias).subscribe(result=>{
        console.log(result);
        this.modal.close();
      },
      error=>{
        console.log(<any>error);
      });
    }
  }


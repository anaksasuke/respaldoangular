import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { Politicas } from '../../../Modelos/politicas';
import { UtilService } from '../../../util.service';
@Component({
  selector: 'app-politicas',
  templateUrl: './politicas.component.html',
  styleUrls: ['./politicas.component.scss']
})
export class PoliticasComponent implements OnInit {
  politicas = new Politicas(0, 0, '', '', '', '', 0);
  Columns = ['politica', 'catPolitica', 'descPolitica', 'prioPolitica', 'defPolitica', 'Acciones'];
  dataSource:any;
  title = "Nueva Política";
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
 
  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog) { }

  ngOnInit() {
    this.RenderTable();
    this.politicas.idUsuario =  localStorage.Usuario;
  }

  RenderTable(){
    this.http.Politicas().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{
      console.log(<any>error);
    }); 
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    this.politicas = {idPolitica:row.idPolitica,politica:row.politica,descPolitica:row.descPolitica,prioPolitica:row.prioPolitica,defPolitica:row.prioPolitica,idCatPolitica:row.idCatPolitica,idUsuario:this.politicas.idUsuario};
    this.openDialog();
  }

  Reset(){
    this.politicas = { idPolitica:0, politica:"", descPolitica:"", prioPolitica:"", defPolitica:"", idCatPolitica:0, idUsuario:this.politicas.idUsuario }
  }

  Eliminar(row){
    this.politicas.idPolitica = row.idPolitica;
    this.politicas.idUsuario =  this.politicas.idUsuario;
    this.http.PoliticasEliminar(this.politicas).subscribe(result=>{
      this.RenderTable();
      console.log(result);
    },
    error=>{
      console.log(<any>error);
    });
  }

  openDialog():void{
    const dialogRef = this.dialog.open(politicasModal,{
      width:"400",
      height:"400",
      data: this.politicas
    });
    dialogRef.afterClosed().subscribe(resul=>{
      this.RenderTable();
    })
  }

}

  @Component({
    selector:"politicasModal",
    templateUrl:"politicasModal.html"
  })
  export class politicasModal implements OnInit{
    constructor(public modal:MatDialogRef<politicasModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public politicas:Politicas){}
    selCat;
    ngOnInit(){
      this.SelectCat();
    }

    onNoClick():void{
      this.modal.close();
    }

    Nuevo(){
      this.http.PoliticasCrear(this.politicas).subscribe(result=>{
       this.modal.close();
       console.log(result);
       },
       error=>{
         console.log(<any>error);
       });
     }

     Editar(){
      this.http.PoliticasEditar(this.politicas).subscribe(result=>{
         this.modal.close();
         console.log(result);
       },
       error=>{
         console.log(<any>error);
       })
     }

     SelectCat(){
      this.http.Categorias().subscribe(result=>{
        this.selCat = result;
      },
      error=>{
        console.log(<any>error);
      });
    }
   
  }

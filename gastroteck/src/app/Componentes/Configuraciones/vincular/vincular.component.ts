import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ConfiguracionesService } from 'src/app/Servicios/configuraciones.service';
import { Vincular } from '../../../Modelos/vincular';
import { UtilService } from '../../../util.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector:'app-vincular',
  templateUrl: './vincular.component.html',
  styleUrls: ['./vincular.component.scss']
})
export class  VincularComponent implements OnInit {
  constructor(public http:ConfiguracionesService, public util:UtilService, public dialog:MatDialog){}
  vincular = new Vincular(0,0,0,0,0,0);
  Columns = ['idClasArticulo', 'idCatArticulo', 'Acciones'];
  dataSource: any;
  selCategoria;
  selClas;
  @ViewChild(MatPaginator,{static:true})paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  ngOnInit(){
  this.RenderTable(),
  this.vincular.idUser =  localStorage.Usuario;
  }

  RenderTable() {
    this.http.Vincular().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{
      console.log(<any>error);
     });
  }
  
  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    this.vincular={idCategoria:row.idCatArticulo, idClas:row.idClasArticulo, idUser:this.vincular.idUser, nidCategoria:row.idCatArticulo, nidClas:row.idClasArticulo, link:1};
    this.openDialog();
  } 
  
  Reset(){
    this.vincular = { idUser:this.vincular.idUser, idCategoria:0, idClas:0, nidCategoria:0, nidClas:0, link:0}
  }
  
  Eliminar(row){
    this.vincular.idCategoria = parseInt(row.idCatArticulo);
    this.vincular.idClas = parseInt(row.idClasArticulo);
    this.http.VincularEliminar(this.vincular).subscribe(result=>{
      this.RenderTable();
      console.log(result);
    },
    error=>{
      console.log(<any>error);
    });
  }

  
  openDialog():void{
    const dialogRef = this.dialog.open(vincularModal,{
      width:"400",
      height:"400",
      data: this.vincular
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    });
  }
}

  @Component({
    selector:"vincularModal",
    templateUrl:"vincularModal.html"
  })
  export class vincularModal implements OnInit{
    constructor(public modal:MatDialogRef<vincularModal>,public http:ConfiguracionesService,@Inject(MAT_DIALOG_DATA)public vincular:Vincular){}
    selCategoria;
    selClas;
    ngOnInit(){
      this.SelectCat();
      this.SelectClas();
      console.log(this.vincular);
    }

    onNoClick():void{
      this.modal.close();
    }

    Nuevo() {
      this.http.VincularCrear(this.vincular).subscribe(result=>{
        this.modal.close();
        console.log(result);
      },
      error=>{
        console.log(<any>error);
      }); 
    }
        
    SelectCat(){
      this.http.categoriasArticulo().subscribe(result=>{
        this.selCategoria = result;
      },
      error=>{
        console.log(<any>error);
      });
    }
    
    SelectClas(){
      this.http.Aclasificaciones().subscribe(result=>{
        this.selClas = result;
      },
      error=>{
        console.log(<any>error);
      });
    }

    Editar(){
      console.log(this.vincular);
      this.http.VincularEditar(this.vincular).subscribe(result=>{
        this.modal.close();
        console.log(result);  
      },
      error=>{
        console.log(<any>error);
      });
    }
  }
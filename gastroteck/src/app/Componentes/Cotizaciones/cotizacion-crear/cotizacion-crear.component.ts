import { Component, OnInit,ViewChild } from '@angular/core';
import { CotizacionesService } from '../../../Servicios/cotizaciones.service';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { ClienteInter } from '../../../Interfaces/Cliente';
import { ArticuloInter } from '../../../Interfaces/Articulo'
import { CotArticulo } from '../../../Modelos/CotArticulo';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { CotPolitica } from '../../../Modelos/CotPolitica';
import { UtilService } from '../../../util.service';
import { CotDatosGen } from '../../../Modelos/CotDatosGen';
import { Agente } from '../../../Interfaces/Agente';
import { MatAccordion } from '@angular/material';

@Component({
  selector: 'app-cotizacion-crear',
  templateUrl: './cotizacion-crear.component.html',
  styleUrls: ['./cotizacion-crear.component.scss']
})
export class CotizacionCrearComponent implements OnInit {
  //Tabla de articulos
  Columns=['acciones','idPartida','areaCotizacion','modArticulo','nomArticulo','marcArticulo','catArticulo','nomProveedor','cantArticulo','cosArticulo','medArticulo','margArticulo','dctoArticulo','subArticulo'];
  dataSource:any;
  ColumnsPol = ['acciones','politica','catPolitica','descPolitica'];
  dataSourcePol:any; 
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild('accordion',{static:true}) Accordion:MatAccordion;
  @ViewChild('accordionPol',{static:true}) AccordionPol:MatAccordion;
  stateCtrl = new FormControl();
  FilteredCliente:Observable<ClienteInter[]>;
  Cliente:ClienteInter[]=[];
  stateCtrlArt = new FormControl();
  FilteredArt:Observable<ArticuloInter[]>;
  Articulo:ArticuloInter[]=[];   
  stateCtrlAgente = new FormControl();
  FilteredAgente:Observable<Agente[]>;
  Agente:Agente[]=[];
  Proyecto; TipoArticulo; AreasCot; Politicas;
  Ivas; Provedores; Monedas;  NombreArt=""; TiempoEntrega="";
  subTotal; Total; iva=0; ModelAuto; medArticulo;
  //Variables Independientes para politicas (descripcion)
  PolDesc="";  polDesc='';
  //Variables para tabla politicas (idUsuario,idCot,version) como es nueva la idCot y version iran en 0;
  version = 0; idCot=0;
  //Varaibales para totales 
  longArea; longMoneda; MarcaTotales;
  constructor(public http:CotizacionesService,public util:UtilService) { }
  cotArticulo = new CotArticulo(0,0,0,0,0,0,0,2,0,0,0,0,0);
  cotPolitica = new CotPolitica(0,0,'',0);
  cotDatos = new CotDatosGen(0,0,'','',0,0,0,0);

  ngOnInit() {
    this.cotDatos.idUsuario = localStorage.Usuario;
    this.cotArticulo.idUser = this.cotDatos.idUsuario;
    this.cotPolitica.idUsuario = this.cotDatos.idUsuario;
    this.Clientes();
    this.Agentes();
    this.Articulos();
    this.AgregarPoliticasDefault();
    this.CargarTablaArt();
    this.CargarArea();
    this.CargarIVA();
    this.CargarMonedas();
    this.CargarPoliticas();
    this.OpendAccordion();
  }

  OpendAccordion(){
    this.Accordion.multi = true;
    this.AccordionPol.multi = true;
    this.Accordion.openAll();
    this.AccordionPol.openAll();
  }

  Clientes(){
    this.http.AutoCliente().subscribe(result=>{
      this.Cliente = Object.values(result);
      this.FilteredCliente = this.stateCtrl.valueChanges.pipe(startWith(''),map(clientes => clientes ? this.filterCli(clientes):this.Cliente.slice()));
    },error=>{console.log(<any>error);});
  }
  
  filterCli(value:string):ClienteInter[]{
      const filterValue = value.toLowerCase();
      return this.Cliente.filter(clientes =>clientes.compCliente.toLowerCase().indexOf(filterValue)===0);
  }
  
  ObtCliente(idCliente){
    this.cotDatos.idCliente = idCliente;
    this.http.AutoProyecto(idCliente).subscribe(result=>{
      this.Proyecto = result;
    },error=>{console.log(<any>error);});
  }
  
  Agentes(){
    this.http.AutoAgente().subscribe(result=>{
      this.Agente = Object.values(result);
      this.FilteredAgente = this.stateCtrlAgente.valueChanges.pipe(startWith(''),map(agente =>  agente ? this.FilterAge(agente):this.Agente.slice()));
    },error=>{console.log(<any>error);});
  }

  FilterAge(value:string):Agente[]{
    const filterValue = value.toLowerCase();
    return this.Agente.filter(agente => agente.nomComUsuario.toLowerCase().indexOf(filterValue) === 0);
  }

  ObtenerAgente(id){
    this.cotDatos.idAgente = id;
  }

  CargarArea(){
    this.http.Areas().subscribe(result=>{
      this.AreasCot = result;
    },error=>{console.log(<any>error);});
  }
  
  CargarIVA(){
    this.http.IVA().subscribe(result=>{
      this.Ivas = result;
    },error=>{console.log(<any>error);});
  }

  CargarMonedas(){
    this.http.Moneda().subscribe(result=>{
      this.Monedas = result;
    },error=>{console.log(<any>error);});
  }
  
  Articulos(){
    this.http.Articulos().subscribe(result=>{
      this.Articulo = Object.values(result);
      this.FilteredArt = this.stateCtrlArt.valueChanges.pipe(startWith(''),map(articulo => articulo ? this.filterArt(articulo):this.Articulo.slice()));
    },error=>{console.log(<any>error);});
  }
  
  filterArt(value:string):ArticuloInter[]{
      const filterValue = value.toLowerCase();
      return this.Articulo.filter(articulos => articulos.modArticulo.toLowerCase().indexOf(filterValue)===0);
  }
  
  ObtArticulo(Articulo){
    this.NombreArt = Articulo.nomArticulo;
    this.cotArticulo.idArticulo = Articulo.idArticulo;
    this.medArticulo = Articulo.cveMedArticulo;
    this.CargarProveedor(Articulo.idArticulo);
  }
  
  CargarProveedor(idArt){
    this.http.Proveedores(idArt).subscribe(result=>{
      this.Provedores = result;
    },error=>{console.log(<any>error);});
  }
  
  DatosArticulo(idProveedor){
    this.http.ProveedorArt(idProveedor,this.cotArticulo.idArticulo).subscribe(result=>{
      Object.values(result).forEach(element =>{
        this.cotArticulo.precio = element.cBaseArticulo;
        this.cotArticulo.idMoneda = element.idMoneda;
        this.cotArticulo.precioRep = element.cRepArticulo;
        this.TiempoEntrega = element.tEntArticulo;
      });
    },error=>{console.log(<any>error);});
  }
  
  CargarTablaArt(){
    this.http.TablaArt(this.cotDatos.idUsuario).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
    },error=>{console.log(<any>error);});
  }
  
  Operaciones(){
    this.subTotal = (this.cotArticulo.cantidad * (this.cotArticulo.precioRep / (1 - (this.cotArticulo.marge / 100)) + (this.cotArticulo.precio - this.cotArticulo.precioRep)) * (1 - (this.cotArticulo.descuento / 100))).toFixed(2);
    this.iva = this.iva;
    this.AgregoIva(this.iva);
  }
  
  AgregoIva(param){
    this.iva = parseFloat(param);
    this.subTotal = parseFloat(this.subTotal);
    this.Total = parseFloat((this.subTotal * this.iva) + this.subTotal).toFixed(2);
  }
  
  GuardarArt(){
    if(this.medArticulo == 'E48')
      this.cotArticulo.precioRep = this.cotArticulo.precio;
    this.http.AgregarArt(this.cotArticulo).subscribe(result=>{
      console.log(result);
      this.CargarTablaArt();
      this.Cancelar();
    },error=>{console.log(<any>error);});
  }
  
  Cancelar(){
    this.cotArticulo = {idAreaCot:this.cotArticulo.idAreaCot,idArticulo:0,idIva:this.cotArticulo.idIva,idMoneda:0,idProveedor:0,idUser:this.cotDatos.idUsuario,descuento:0,cantidad:0,marge:this.cotArticulo.marge,precio:0,precioRep:0,ubicador:0,idPartida:0};
    this.TiempoEntrega = "";
    this.NombreArt = "";
    this.medArticulo = '';
    this.iva = 0;
    this.stateCtrlArt.setValue('');
    this.stateCtrlArt.enable();
  }
  
  ObtenerRowArt(row){
    this.TiempoEntrega = row.tEntArticulo;
    this.NombreArt = row.nomArticulo;
    this.ModelAuto = row.modArticulo;
    this.cotArticulo = {idAreaCot:row.idAreaCotizacion,idPartida:row.idPartida,idArticulo:row.idArticulo,idMoneda:row.idMoneda,idIva:row.idIva,idProveedor:row.idProveedor,idUser:this.cotArticulo.idUser,descuento:row.dctoArticulo,cantidad:row.cantArticulo,marge:row.margArticulo,precio:row.cosArticulo,precioRep:row.cosArticulo,ubicador:1};
    this.stateCtrlArt.setValue(row.modArticulo + ' (' + row.nomArticulo  + ')');
    this.stateCtrlArt.disable();
    this.CargarProveedor(this.cotArticulo.idArticulo.toString());
    this.Operaciones();
    this.AgregoIva(this.cotArticulo.idIva);
  }

  ActualizarArt(){
    this.http.ActualizarArt(this.cotArticulo).subscribe(result=>{
      this.CargarTablaArt();
      this.Cancelar();
    },error=>{console.log(<any>error);});
  }
  
  EliminarArt(row){
    this.cotArticulo = {idAreaCot:row.idAreaCotizacion,idPartida:row.idPartida,idArticulo:row.idArticulo,idMoneda:row.idMoneda,idIva:row.idIva,idProveedor:row.idProveedor,idUser:this.cotArticulo.idUser,descuento:row.dctoArticulo,cantidad:row.cantArticulo,marge:row.margArticulo,precio:row.cosArticulo,precioRep:row.cosArticulo,ubicador:1};
    this.http.EliminarArt(this.cotArticulo).subscribe(result=>{
      this.CargarTablaArt();
      this.Cancelar();
    },error=>{console.log(<any>error);});
  }
  
  AgregarPoliticasDefault(){
    this.http.CargarPloticas(this.cotDatos.idUsuario,this.idCot,this.version).subscribe(result=>{
      this.dataSourcePol = new MatTableDataSource();
      this.dataSourcePol.data = result;
      this.dataSourcePol.paginator = this.paginator;
    },error=>{console.log(<any>error);});
  }
  
  TablaPolitica(){
    this.http.TablaPol(this.cotDatos.idUsuario).subscribe(result=>{
      this.dataSourcePol = new MatTableDataSource();
      this.dataSourcePol.data = result;
      this.dataSourcePol.paginator = this.paginator;
    },error=>{console.log(<any>error);});
  }
  
  CargarPoliticas(){
    this.http.politicas(this.cotDatos.idUsuario).subscribe(result=>{
      this.Politicas = result;
    },error=>{console.log(<any>error);});
  }
  
  GuardarPol(){
    this.http.GuardarPoliticas(this.cotPolitica).subscribe(result=>{
      this.TablaPolitica();
      this.CancelarPolitica();
    },error=>{console.log(<any>error);});
  }

  ObtRowPolitica(row){
    this.cotPolitica = {idUsuario:this.cotDatos.idUsuario,idPolitica:row.idPolitica,valPolitica:row.valPolitica,ubicador:1};
    this.TodasPoliticas();
    this.DatosPolitica(this.cotPolitica.idPolitica,row.descPolitica);
    if(this.cotPolitica.valPolitica != ""){
      this.ReplazoDeCaracter();
    }
  }
  
  TodasPoliticas(){
    this.http.PoliticasActualizar().subscribe(result=>{
      this.Politicas = result;
    },error=>{console.log(<any>error);});
  }

  DatosPolitica(id,descripcion){
    this.cotPolitica.idPolitica = id;
    this.PolDesc = descripcion;
    this.polDesc = descripcion;
  }
  
  CancelarPolitica(){
    this.cotPolitica = {idUsuario:this.cotDatos.idUsuario,idPolitica:0,valPolitica:"",ubicador:0};
    this.PolDesc = "";
    this.polDesc = "";
    this.CargarPoliticas();
  }
  
  ReplazoDeCaracter(){
    var n1 = this.cotPolitica.valPolitica.toString().replace( / /g,"");
    var n2 = n1.split(',');
    var pol = this.polDesc.split('');
    var j = 0;
    for(var i=0; i<pol.length; i++){
      if(pol[i] == "#"){
        pol[i] = n2[j];
        j++;
      }
    }
    this.PolDesc = pol.join('');
  }

  ReemplazoPolitica(row){
    var n1 = row.valPolitica.toString().replace( / /g,"");
    var n2 = n1.split(',');
    var pol = row.descPolitica.split('');
    var j = 0;
    for(var i=0; i<pol.length; i++){
      if(pol[i] == "#"){
        pol[i] = n2[j];
        j++;
      }
    }
    return pol.join('');
  }
  
  ActualizarPol(){
    this.http.ActualizarPoliticas(this.cotPolitica).subscribe(result=>{
      this.TablaPolitica();
      this.CancelarPolitica();
    },error=>{console.log(<any>error);});
  }
  
  EliminarPolitica(row){
    this.http.EliminarPolitica(row).subscribe(result=>{
      this.TablaPolitica();
      console.log(result);
    },error=>{console.log(<any>error);});
  }
  
  GuardarCotizacion(){
    this.http.GuardarCotizacion(this.cotDatos).subscribe(result=>{
      this.util.NavegacionDinamica(result,'cotizacion');
    },error=>{console.log(<any>error);});
  } 

  ResetAll(){
    this.http.TablaArtActulizado(0,0,this.cotDatos.idUsuario).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator
    }, error=>{ console.log(<any>error); });
    this.http.politicas(this.cotDatos.idUsuario).subscribe(result=>{
      this.Politicas = result;
    }, error=>{ console.log(<any>error); });
  }

}
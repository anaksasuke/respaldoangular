import { Component, OnInit,ViewChild } from '@angular/core';
import { UtilService } from '../../../util.service';
import { CotizacionesService } from '../../../Servicios/cotizaciones.service';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { CotDatosGen } from '../../../Modelos/CotDatosGen';
import { CotArticulo } from '../../../Modelos/CotArticulo';
import { CotPolitica } from '../../../Modelos/CotPolitica';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { CotMonedaCom } from '../../../Modelos/CotMonedaCom';
import { ClienteInter } from '../../../Interfaces/Cliente';
import { Agente } from '../../../Interfaces/Agente';
import { ArticuloInter } from '../../../Interfaces/Articulo';

@Component({
  selector: 'app-cotizacion-ver-version',
  templateUrl: './cotizacion-ver-version.component.html',
  styleUrls: ['./cotizacion-ver-version.component.scss']
})
export class CotizacionVerVersionComponent implements OnInit {

  constructor(public util:UtilService,public http:CotizacionesService) { }
   //SELECT
  Proyecto; AreasCot; Provedores; Ivas; Monedas; Politicas; Moneda1; Moneda2;
   //VARIABLES 
  TiempoEntrega=""; subTotal; iva=0; NombreArt=""; Total; expanded =0; expandedPolitica=0; polDesc; PolDesc;
  //Tabla para articulos 
  Columns=['idPartida','areaCotizacion','modArticulo','nomArticulo','marcArticulo','catArticulo','nomProveedor','cantArticulo','cosArticulo','medArticulo','margArticulo','dctoArticulo','subArticulo'];
  dataSource:any;    
  //Tabla para politicas 
  ColumnsPol = ['politica','catPolitica','descPolitica'];
  dataSourcePol:any;     
  stateCtrl = new FormControl();
  FilteredCliente:Observable<ClienteInter[]>;
  Cliente:ClienteInter[]=[];    
  stateCtrlAgente = new FormControl();
  FilteredAgente:Observable<Agente[]>;
  Agente:Agente[]=[];  
  stateCtrlArt = new FormControl();
  FilteredArt:Observable<ArticuloInter[]>;
  Articulo:ArticuloInter[]=[];
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  cotDatos = new CotDatosGen(0,0,'','',0,0,0,0);
  cotArticulo = new CotArticulo(0,0,0,0,0,0,0,0,0,0,0,0,0);
  cotPolitica  = new CotPolitica(0,0,'',0);
  cotMonedaCom = new CotMonedaCom(0,0,0,0); 
  
  ngOnInit() {
    this.cotDatos.idUsuario = localStorage.Usuario;
    this.cotArticulo.idUser = this.cotDatos.idUsuario;
    this.cotPolitica.idUsuario = this.cotDatos.idUsuario;
    this.CargarDatos();
    this.Clientes();
    this.Agentes();
    this.CargarIVA();
    this.CargarArea();
  }

  CargarDatos(){
    var datos = this.util.showDatos();
    this.CargarProveedores();
    this.cotDatos = {idCot:datos.idCotizacion,version:datos.versCotizacion,idCliente:datos.idCliente,idProyect:datos.idProyecto,idAgente:datos.idAgente,idUsuario:this.cotDatos.idUsuario,referencia:datos.refCotizacion,division:datos.divCotizacion};
    this.ObtCliente(this.cotDatos.idCliente);
    this.stateCtrl.setValue(datos.compCliente);
    this.stateCtrl.disable();
    this.stateCtrlAgente.setValue(datos.agente);
    this.stateCtrlAgente.disable();
    this.CargarArticulos();
    this.CargarPoliticasCot();
    this.CargarMonedas();
  }

  ObtCliente(idCliente){
    this.http.AutoProyecto(idCliente).subscribe(result=>{
      this.Proyecto = result;
    },error=>{console.log(<any>error);});
  }

  CargarProveedores(){
    this.http.AutoProyecto(this.cotDatos.idCliente).subscribe(result=>{
      this.Proyecto = result;
    },error=>{console.log(<any>error);});
  }

  Clientes(){
    this.http.AutoCliente().subscribe(result=>{
      this.Cliente = Object.values(result);
      this.FilteredCliente = this.stateCtrl.valueChanges.pipe(startWith(''),map(clientes => clientes ? this.filterCli(clientes):this.Cliente.slice()));
    },error=>{console.log(<any>error);});
  }
  
  filterCli(value:string):ClienteInter[]{
      const filterValue = value.toLowerCase();
      return this.Cliente.filter(clientes =>clientes.compCliente.toLowerCase().indexOf(filterValue)===0);
  }

  Agentes(){
    this.http.AutoAgente().subscribe(result=>{
      this.Agente = Object.values(result);
      this.FilteredAgente = this.stateCtrlAgente.valueChanges.pipe(startWith(''),map(agente =>  agente ? this.FilterAge(agente):this.Agente.slice()));
    },error=>{console.log(<any>error);});
  }

  FilterAge(value:string):Agente[]{
    const filterValue = value.toLowerCase();
    return this.Agente.filter(agente => agente.nomComUsuario.toLowerCase().indexOf(filterValue) === 0);
  }

  ObtenerAgente(id){
    this.cotDatos.idAgente = id;
  }

  CargarArticulos(){
    this.http.TablaArtActulizado(this.cotDatos.idCot,this.cotDatos.version,this.cotDatos.idUsuario).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator
    },error=>{console.log(<any>error);});
  }

  CargarArea(){
    this.http.Areas().subscribe(result=>{
      this.AreasCot = result;
    },error=>{console.log(<any>error);});
  }
  
  CargarIVA(){
    this.http.IVA().subscribe(result=>{
      this.Ivas = result;
    },error=>{console.log(<any>error);});
  }

  CargarMonedas(){
    this.http.Moneda().subscribe(result=>{
      this.Monedas = result;
    },error=>{console.log(<any>error);});
  }

  Articulos(){
    this.http.Articulos().subscribe(result=>{
      this.Articulo = Object.values(result);
      this.FilteredArt = this.stateCtrlArt.valueChanges.pipe(startWith(''),map(articulo => articulo ? this.filterArt(articulo):this.Articulo.slice()));
    },error=>{console.log(<any>error);});
  }
  
  filterArt(value:string):ArticuloInter[]{
      const filterValue = value.toLowerCase();
      return this.Articulo.filter(articulos => articulos.modArticulo.toLowerCase().indexOf(filterValue)===0);
  }

  CargarProveedor(idArt){
    this.http.Proveedores(idArt).subscribe(result=>{
      this.Provedores = result;
    },error=>{console.log(<any>error);});
  }

  DatosArticulo(idProveedor){
    this.http.ProveedorArt(idProveedor,this.cotArticulo.idArticulo).subscribe(result=>{
      Object.values(result).forEach(element =>{
        this.cotArticulo.precio = element.cBaseArticulo;
        this.cotArticulo.idMoneda = element.idMoneda;
        this.TiempoEntrega = element.tEntArticulo;
      });
    },error=>{console.log(<any>error);});
  }
  
  CargarTablaArt(){
    this.http.TablaArt(this.cotDatos.idUsuario).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
    },error=>{console.log(<any>error);});
  }

  CargarPoliticasCot(){
    this.http.CargarPloticas(this.cotDatos.idUsuario,this.cotDatos.idCot,this.cotDatos.version).subscribe(result=>{
      this.dataSourcePol = new MatTableDataSource();
      this.dataSourcePol.data = result;
      this.dataSourcePol.paginator = this.paginator;
    },error=>{console.log(<any>error);});
  }

  Operaciones(){
    this.subTotal = (this.cotArticulo.cantidad * ((this.cotArticulo.precio/(1 - this.cotArticulo.marge / 100)) * (1 - (this.cotArticulo.descuento / 100)))).toFixed(2);
    this.iva = this.iva;
    this.AgregoIva(this.iva);
  }
  
  AgregoIva(param){
    this.iva = parseFloat(param);
    this.subTotal = parseFloat(this.subTotal);
    this.Total = parseFloat((this.subTotal * this.iva) + this.subTotal).toFixed(2);
  }

  Cancelar(){
    this.cotArticulo = {idAreaCot:0,idArticulo:0,idIva:0,idMoneda:0,idProveedor:0,idUser:this.cotDatos.idUsuario,descuento:0,cantidad:0,marge:0,precio:0,precioRep:0,ubicador:0,idPartida:0};
    this.TiempoEntrega = "";
    this.NombreArt = "";
    this.stateCtrlArt.setValue('');
    this.stateCtrlArt.enable();
    this.expanded = 0;
  }

  ObtenerRowArt(row){
    this.TiempoEntrega = row.tEntArticulo;
    this.NombreArt = row.nomArticulo;
    this.cotArticulo = {idAreaCot:row.idAreaCotizacion,idPartida:row.idPartida,idArticulo:row.idArticulo,idMoneda:row.idMoneda,idIva:row.idIva,idProveedor:row.idProveedor,idUser:this.cotArticulo.idUser,descuento:row.dctoArticulo,cantidad:row.cantArticulo,marge:row.margArticulo,precio:row.cosArticulo,precioRep:row.cosArticulo,ubicador:1};
    this.stateCtrlArt.setValue(row.nomArticulo + ' (' + row.modArticulo + ')');
    this.stateCtrlArt.disable();
    this.CargarProveedor(this.cotArticulo.idArticulo);
    this.expanded = 1;
  }
  
  ActualizarArt(){
    this.http.ActualizarArt(this.cotArticulo).subscribe(result=>{
      this.CargarTablaArt();
      this.Cancelar();
    },error=>{console.log(<any>error);});
  }

  TablaPolitica(){
    this.http.TablaPol(this.cotDatos.idUsuario).subscribe(result=>{
      this.dataSourcePol = new MatTableDataSource();
      this.dataSourcePol.data = result;
      this.dataSourcePol.paginator = this.paginator;
    },error=>{console.log(<any>error);});
  }

  ReplazoDeCaracter(){
    var n1 = this.cotPolitica.valPolitica.toString().replace( / /g,"");
    var n2 = n1.split(',');
    var pol = this.polDesc.split('');
    var j = 0;
    for(var i=0; i<pol.length; i++){
      if(pol[i] == "#"){
        pol[i] = n2[j];
        j++;
      }
    }
    this.PolDesc = pol.join('');
  }

  ReemplazoPolitica(row){
    var n1 = row.valPolitica.toString().replace( / /g,"");
    var n2 = n1.split(',');
    var pol = row.descPolitica.split('');
    var j = 0;
    for(var i=0; i<pol.length; i++){
      if(pol[i] == "#"){
        pol[i] = n2[j];
        j++;
      }
    }
    return pol.join('');
  }

  ObtRowPolitica(row){
    this.cotPolitica = {idUsuario:this.cotDatos.idUsuario,idPolitica:row.idPolitica,valPolitica:row.valPolitica,ubicador:1};
    this.TodasPoliticas();
    this.DatosPolitica(this.cotPolitica.idPolitica,row.descPolitica);
    if(this.cotPolitica.valPolitica != ""){
      this.ReplazoDeCaracter();
    }
  }

  TodasPoliticas(){
    this.http.PoliticasActualizar().subscribe(result=>{
      this.Politicas = result;
    },error=>{console.log(<any>error);});
  }

  DatosPolitica(id,descripcion){
    this.cotPolitica.idPolitica = id;
    this.PolDesc = descripcion;
    this.polDesc = descripcion;
  }

  CancelarPolitica(){
    this.cotPolitica = {idUsuario:this.cotDatos.idUsuario,idPolitica:0,valPolitica:"",ubicador:0};
    this.PolDesc = "";
    this.polDesc = "";
    this.TodasPoliticas();
  }
  
  ActualizarPol(){
    this.http.ActualizarPoliticas(this.cotPolitica).subscribe(result=>{
      this.TablaPolitica();
      this.CancelarPolitica();
    }, error=>{ console.log(<any>error); });
  }

  ActualizarCotizacion(){
    this.http.CotizacionEditar(this.cotDatos).subscribe(result=>{
      this.util.NavegacionDinamica(result,'cotizacion');
    }, error=>{ console.log(<any>error); });
  }

  ResetAll(){
    this.http.TablaArtActulizado(0,0,this.cotDatos.idUsuario).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator
    }, error=>{ console.log(<any>error); });
    this.http.politicas(this.cotDatos.idUsuario).subscribe(result=>{
      this.Politicas = result;
    }, error=>{ console.log(<any>error); });
  }
  
}
import { Component, OnInit,ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CotizacionesService } from '../../../Servicios/cotizaciones.service';
import { UtilService } from '../../../util.service';
import { CotEstatus } from '../../../Modelos/CotEstatus';
import { VersionCot } from '../../../Interfaces/VersionCot';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-cotizacionshow',
  templateUrl: './cotizacionshow.component.html',
  styleUrls: ['./cotizacionshow.component.scss']
})
export class CotizacionshowComponent implements OnInit {

  constructor(public http:CotizacionesService,public util:UtilService,public dialog:MatDialog) { }
  Columns=['acciones',"nomCotizacion","estOperacion","compCliente","nomProyecto",'refCotizacion',"totCotizacion",'tCamCotizacion',"fVenCotizacion","fModCotizacion", 'usuario'];
  dataSource:any;
  cotEstatus = new CotEstatus(0,0,0,0);
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    this.renderTable();
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator){
      this.dataSource.paginator.firstPage();
    }
  }

  renderTable(){
    this.http.Cotizaciones().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, error => { console.log(<any>error); });
  }


  EditarVersion(row){
    this.util.getDatos(row);
    this.util.navegacion("/cotizacion/editar");
  }

  NuevaVersion(row){
    this.util.getDatos(row);
    this.util.navegacion("/cotizacion/nueva-version");
  }

  CambiarEstatus(row){
    this.cotEstatus = {idEstatus:row.idEstOperacion,idCot:row.idCotizacion,version:row.versCotizacion,idUsuario:localStorage.Usuario};
    this.OpenEstatus();
  }

  OpenEstatus(){
    const dialogRef = this.dialog.open(CotizacionPop, {
      width:'auto',
      height:"auto",
      data:this.cotEstatus
    });
    dialogRef.afterClosed().subscribe(result => {
      this.renderTable();
    })
  }

  OpenModal(row):void {
    const dialogRef = this.dialog.open(CotizacionVersiones, {
      width:"1000",
      height:"auto",
      data:row.idCotizacion
    });
    dialogRef.afterClosed().subscribe(result => {
      this.renderTable();
    });
  }

  Eliminar(row){
    this.cotEstatus = {idEstatus:row.idEstOperacion,idCot:row.idCotizacion,version:row.versCotizacion,idUsuario:localStorage.Usuario};
    this.http.EliminarCotizacion(this.cotEstatus).subscribe(result=>{
      this.renderTable();
      console.log(result);
    }, error => { console.log(<any>error); });
  }

  Observar(row){
    this.util.getDatos(row);
    this.util.navegacion("cotizacion/ver-version");
  }

}

@Component({
  selector:"CotizacionVersiones",
  templateUrl:"CotizacionVersiones.html",
  styleUrls: ['./cotizacionshow.component.scss']
})

export class CotizacionVersiones implements OnInit{
  constructor(public modal:MatDialogRef<CotizacionVersiones>, public http:CotizacionesService, @Inject(MAT_DIALOG_DATA) public version:VersionCot){}
  Columns=["acciones","nomCotizacion","totCotizacion","fVenCotizacion","fModCotizacion"];
  dataSource:any;
  @ViewChild(MatPaginator, {static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit(){
    this.renderTable();
  }

  renderTable(){
    this.http.Versiones(this.version).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, error => { console.log(<any>error); })
  }

}

@Component({
  selector:"CotizacionPop",
  templateUrl:"CotizacionPop.html",
  styleUrls: ['./cotizacionshow.component.scss']
})

export class CotizacionPop implements OnInit {
  constructor(public modal:MatDialogRef<CotizacionPop>, public http:CotizacionesService, @Inject(MAT_DIALOG_DATA) public estatus:CotEstatus){}
  Estus;

  ngOnInit(){
    this.Estatus();
  }

  Estatus(){
    this.http.Estatus().subscribe(result=>{
      this.Estus = result;
    },error => { console.log(<any>error); });
  }

  GuardarEstatus(){
    this.http.CambioEst(this.estatus).subscribe(result=>{
      this.modal.close();
      console.log(result);
    }, error => { console.log(<any>error); });
  }

  Cancelar(){
    this.modal.close();
  }

}
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map,startWith } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { EntradasService } from '../../../Servicios/entradas.service';
import { UtilService } from '../../../util.service';
import { ProveedorInter } from '../../../Interfaces/Proveedor';
import { OrdenInter } from '../../../Interfaces/Orden';
import { Entrada } from 'src/app/Modelos/Entrada';
import { EntradaCarArt } from 'src/app/Modelos/EntradaCarArt';
import { EntradaArticulo } from '../../../Modelos/EntradaArticulo';
import * as moment from 'moment';

@Component({
  selector: 'app-entrada-crear',
  templateUrl: './entrada-crear.component.html',
  styleUrls: ['./entrada-crear.component.scss']
})
export class EntradaCrearComponent implements OnInit {
  //AutoComplit Proveedor
  stateCtrl = new FormControl();
  FilteredPro:Observable<ProveedorInter[]>;
  proveedor:ProveedorInter[]=[];
  //AutoComplit Ordenes
  stateCtrlOrd = new FormControl();
  FilteredOrd:Observable<OrdenInter[]>
  orden:OrdenInter[]=[];
  //Validador Date 
  minDate = new Date(Date.now());
  validadorHtml=0;
  constructor(public http:EntradasService,public util:UtilService) { }
  //Tabla Articulos
  Columns = ['nomOrden','nomCotizacion','areaCotizacion','idPartida','modArticulo','nomArticulo','resArticulo','cantArticulo','impArticulo','cveMoneda','Acciones'];
  dataSource:any;
  ArticuloFaltante=0; disabled=0; area="";
  entrada = new Entrada(0,0,'','','','',0,0,0,0,0);
  entradaCargarArticulos  = new EntradaCarArt(0,0,0);
  entradaArticulo = new  EntradaArticulo('','','','',0,0,0,0,0,0,0,0,0,0,"",0,"",0);

  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;

  ngOnInit() {
    this.Proveedor();
    this.entrada.idUsuario = localStorage.Usuario;
  }

  Proveedor(){
    this.http.Proveedores().subscribe(result=>{
      this.proveedor = Object.values(result);
      this.FilteredPro = this.stateCtrl.valueChanges.pipe(startWith(''),map(pro => pro ? this.FilterPro(pro):this.proveedor.slice()));
    },error=>{console.log(<any>error);});
  }

  FilterPro(value:string):ProveedorInter[]{
    const filter = value.toLowerCase();
    return this.proveedor.filter(pro => pro.nomProveedor.toLowerCase().indexOf(filter)===0);
  }

  ObtProveedor(idProveedor){
    if(this.entrada.idProveedor != 0 && this.entrada.idProveedor != idProveedor ){
      this.entrada.idProveedor = idProveedor;
      this.CambioProveedor();
      this.Orden();
    }else {
      this.entrada.idProveedor = idProveedor;
      this.Orden();
    }
  }

  CambioProveedor(){
    this.http.CambioProveedor(this.entrada.idUsuario).subscribe(result=>{
      console.log(result);
      this.RenderTablaArt();
    },error=>{console.log(<any>error);});
  }

  Orden(){
    this.http.Ordenes(this.entrada.idProveedor).subscribe(result=>{
      this.orden = Object.values(result);
      this.FilteredOrd = this.stateCtrlOrd.valueChanges.pipe(startWith(''),map(ord => ord ? this.FilterOrd(ord):this.orden.slice()));
    },error=>{console.log(<any>error);});
  }

  FilterOrd(value:string):OrdenInter[]{
    const filter = value.toLowerCase();
    return this.orden.filter(ord => ord.nomOrden.toLowerCase().indexOf(filter)==0);
  }

  RellenarTablaArt(idOrd,Version){
    this.entradaCargarArticulos = {idUsuario:this.entrada.idUsuario,idOrd:idOrd,Version:Version};
    this.http.RellenarTabla(this.entradaCargarArticulos).subscribe(result=>{
      console.log(result);
      this.RenderTablaArt();
    },error=>{console.log(<any>error);});
  }

  RenderTablaArt(){
    this.http.VistaArticulos(this.entrada.idUsuario).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
    },error=>{console.log(<any>error);});
  }

  CargarDatos(row,param){
    console.log(row);
    switch(param){
      case 1:
        this.validadorHtml =1;
        this.ArticuloFaltante = row.resArticulo;
        this.area = row.areaCotizacion;
        this.entradaArticulo = {nomOrd:row.nomOrden,nomCot:row.nomCotizacion,idArea:row.idAreaCotizacion,idPartida:row.idPartida,modeloArt:row.modArticulo,nomArt:row.nomArticulo,idOrd:row.idOrden,ordVer:row.versOrden,idCot:row.idCotizacion,cotVer:row.versCotizacion,idArt:row.idArticulo,cantidad:row.cantArticulo,idIva:row.idIva,iva:row.iva,idMoneda:row.idMoneda,moneda:row.cveMoneda,precio:row.cosArticulo,idUsuario:this.entrada.idUsuario};
        break;
      case 2:
        this.area = row.areaCotizacion;
        this.entradaArticulo = {nomOrd:row.nomOrden,nomCot:row.nomCotizacion,idArea:row.idAreaCotizacion,idPartida:row.idPartida,modeloArt:row.modArticulo,nomArt:row.nomArticulo,idOrd:row.idOrden,ordVer:row.versOrden,idCot:row.idCotizacion,cotVer:row.versCotizacion,idArt:row.idArticulo,cantidad:row.cantArticulo,idIva:row.idIva,iva:row.iva,idMoneda:row.idMoneda,moneda:row.cveMoneda,precio:row.cosArticulo,idUsuario:this.entrada.idUsuario};
        this.Eliminar();
      break;
    }
  }

  ValidarButton(){
    if(parseFloat(this.entradaArticulo.cantidad.toString())  > parseFloat(this.ArticuloFaltante.toString())  || this.entradaArticulo.cantidad == 0 || (this.entradaArticulo.cantidad.toString()) ==""){
      this.disabled = 1;
    }else {
      this.disabled = 0;
    }
  }

  Actualizar(){
    this.http.ActualizarArt(this.entradaArticulo).subscribe(result=>{
      console.log(result);
      this.RenderTablaArt();
    },error=>{console.log(<any>error);});
  }

  Eliminar(){
    this.http.BorrarArt(this.entradaArticulo).subscribe(result=>{
      console.log(result);
      this.RenderTablaArt();
    },error=>{console.log(<any>error);});
  }

  CancelarArt(){
    this.entradaArticulo = {nomOrd:"",nomCot:"",idArea:0,idPartida:0,modeloArt:"",nomArt:"",idOrd:0,ordVer:0,idCot:0,cotVer:0,idArt:0,cantidad:0,idIva:0,iva:"",idMoneda:0,moneda:"",precio:0,idUsuario:this.entrada.idUsuario};
    this.validadorHtml = 0;
    this.ArticuloFaltante = 0;
    this.area = "";
  }

  GuardarEntrada(){
    this.entrada.fecha = moment(this.entrada.fecha).format('YYYY-MM-DD');
    this.http.Guardar(this.entrada).subscribe(result=>{
      console.log(result);
      this.util.NavegacionDinamica(result,'entradas');
    },error=>{console.log(<any>error);});
  }

  CancelarEntrada(){
    this.util.navegacion("entradas");
  }

}

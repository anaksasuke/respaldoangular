import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map,startWith } from 'rxjs/operators';
import * as moment from 'moment';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { UtilService } from '../../../util.service';
import { EntradasService } from 'src/app/Servicios/entradas.service';
import { Entrada } from 'src/app/Modelos/Entrada';
import { EntradaArticulo } from 'src/app/Modelos/EntradaArticulo';
import { EntradaCarArt } from '../../../Modelos/EntradaCarArt';
import { OrdenInter } from '../../../Interfaces/Orden';
@Component({
  selector: 'app-entrada-new-version',
  templateUrl: './entrada-new-version.component.html',
  styleUrls: ['./entrada-new-version.component.scss']
})
export class EntradaNewVersionComponent implements OnInit {

  constructor(public util:UtilService,public http:EntradasService) { }
  stateCtrlOrd = new FormControl();
  FilteredOrd:Observable<OrdenInter[]>
  orden:OrdenInter[]=[];
  entrada = new Entrada(0,0,'','','','',0,0,0,0,0);
  entradaArticulo = new  EntradaArticulo('','','','',0,0,0,0,0,0,0,0,0,0,"",0,"",0);
  entradaCarArt = new EntradaCarArt(0,0,0);
  Columns = ['nomOrden','nomCotizacion','areaCotizacion','idPartida','modArticulo','nomArticulo','resArticulo','cantArticulo','impArticulo','cveMoneda','Acciones'];
  dataSource:any;
  nomPro = ""; validadorHtml = 0; ArticuloFaltante=0; disabled=0; area="";
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;

  ngOnInit() {
    var datos = this.util.showDatos();
    this.entrada.idUsuario = localStorage.Usuario;
    this.CargarDatos(datos);
  }

  CargarDatos(datos){
    this.nomPro = datos.nomProveedor;
    this.entrada = {idUsuario:this.entrada.idUsuario,idProveedor:datos.idProveedor,Facturas:datos.factEntrada,Referencia:datos.refEntrada,gastos:datos.gtosEntrada,fecha:moment(datos.fRecEntrada).format("YYYY-MM-DD"),idEntrada:datos.idEntrada,versionEntrada:datos.versEntrada,observaciones:datos.obsEntrada,empaque:datos.empEntrada,instalacion:datos.insEntrada};
    this.CargarEntrada();
    this.Orden();
  }

  CargarEntrada(){
    this.http.CargarEntradaArt(this.entrada).subscribe(result=>{
      console.log(result);
      this.renderTable();
    },error=>{console.log(<any>error);});
  }

  Orden(){
    this.http.Ordenes(this.entrada.idProveedor).subscribe(result=>{
      this.orden = Object.values(result);
      this.FilteredOrd = this.stateCtrlOrd.valueChanges.pipe(startWith(''),map(ord => ord ? this.FilterOrd(ord):this.orden.slice()));
    },error=>{console.log(<any>error);});
  }

  FilterOrd(value:string):OrdenInter[]{
    const filter = value.toLowerCase();
    return this.orden.filter(ord => ord.nomOrden.toLowerCase().indexOf(filter)==0);
  }

  RellenarTablaArt(idOrd,Version){
    this.entradaCarArt = {idUsuario:this.entrada.idUsuario,idOrd:idOrd,Version:Version};
    this.http.RellenarTabla(this.entradaCarArt).subscribe(result=>{
      console.log(result);
      this.renderTable();
    },error=>{console.log(<any>error);});
  }

  renderTable(){
    this.http.VistaArticulos(this.entrada.idUsuario).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      console.log(this.dataSource.data);
      this.dataSource.paginator = this.paginator;
    },error=>{console.log(<any>error);});
  }

  CargarDatosArt(row,param){
    switch(param){
      case 1:
      this.validadorHtml = 1; 
      this.ArticuloFaltante = row.resArticulo;
      this.area = row.areaCotizacion;
      this.entradaArticulo = {nomOrd:row.nomOrden,nomCot:row.nomCotizacion,idArea:row.idAreaCotizacion,idPartida:row.idPartida,modeloArt:row.modArticulo,nomArt:row.nomArticulo,idOrd:row.idOrden,ordVer:row.versOrden,idCot:row.idCotizacion,cotVer:row.versCotizacion,idArt:row.idArticulo,cantidad:row.cantArticulo,idIva:row.idIva,iva:row.iva,idMoneda:row.idMoneda,moneda:row.cveMoneda,precio:row.cosArticulo,idUsuario:this.entrada.idUsuario};
      break;
      case 2:
        this.area = row.areaCotizacion;
        this.entradaArticulo = {nomOrd:row.nomOrden,nomCot:row.nomCotizacion,idArea:row.idAreaCotizacion,idPartida:row.idPartida,modeloArt:row.modArticulo,nomArt:row.nomArticulo,idOrd:row.idOrden,ordVer:row.versOrden,idCot:row.idCotizacion,cotVer:row.versCotizacion,idArt:row.idArticulo,cantidad:row.cantArticulo,idIva:row.idIva,iva:row.iva,idMoneda:row.idMoneda,moneda:row.cveMoneda,precio:row.cosArticulo,idUsuario:this.entrada.idUsuario};
        this.Eliminar();
      break;
    }
  }

  Actualizar(){
    this.http.ActualizarArt(this.entradaArticulo).subscribe(result=>{
      console.log(result);
      this.renderTable();
    },error=>{console.log(<any>error);});
  }

  Eliminar(){
    this.http.BorrarArt(this.entradaArticulo).subscribe(result=>{
      console.log(result);
      this.renderTable();
    },error=>{console.log(<any>error);});
  }

  ValidarButton(){
    if(parseFloat(this.entradaArticulo.cantidad.toString())  > parseFloat(this.ArticuloFaltante.toString())  || this.entradaArticulo.cantidad == 0 || (this.entradaArticulo.cantidad.toString()) ==""){
      this.disabled = 1;
    }else {
      this.disabled = 0;
    }
  }

  CancelarArt(){
    this.entradaArticulo = {nomOrd:"",nomCot:"",idArea:0,idPartida:0,modeloArt:"",nomArt:"",idOrd:0,ordVer:0,idCot:0,cotVer:0,idArt:0,cantidad:0,idIva:0,iva:"",idMoneda:0,moneda:"",precio:0,idUsuario:this.entrada.idUsuario};
    this.validadorHtml = 0;
    this.area = "";
  }

  GuardarEntrada(){
    this.entrada.fecha = moment(this.entrada.fecha).format('YYYY-MM-DD');
    this.http.Guardar(this.entrada).subscribe(result=>{
      console.log(result);
      this.util.NavegacionDinamica(result,'entradas');
    },error=>{console.log(<any>error);});
  }

  CancelarEntrada(){
    this.util.navegacion("entradas");
  }

}

import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UtilService } from '../../../util.service';
import { EntradasService } from '../../../Servicios/entradas.service';
import { EntradaEstatus } from 'src/app/Modelos/EntradaEstatus';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-entrada-show',
  templateUrl: './entrada-show.component.html',
  styleUrls: ['./entrada-show.component.scss']
})
export class EntradaShowComponent implements OnInit {
  constructor(public util:UtilService,public http:EntradasService,public dialog:MatDialog) { }
  entradaStatus = new EntradaEstatus(0,0,0,0);
  estatus;
  Columns =['nomEntrada','ordEntrada','cotEntrada','estOperacion','factEntrada','refEntrada','Acciones'];
  dataSource:any;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    this.renderTable();
    this.entradaStatus.idUsuario = localStorage.Usuario;
  }

  renderTable(){
    this.http.Entradas().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },error=>{console.log(<any>error);});
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  CargarDatos(row){
    this.entradaStatus = {idUsuario:this.entradaStatus.idUsuario,idEntrada:row.idEntrada,verEntrada:row.versEntrada,idEstatus:row.idEstOperacion};
    console.log(this.entradaStatus);
    this.OpendModal();
  }

  OpendModal(){
    const dialog = this.dialog.open(entradaEstatus,{
      width:'auto',
      height:'auto',
      data:this.entradaStatus
    });
    dialog.afterClosed().subscribe(result=>{
      this.renderTable();
    });
  }

  EliminarOrden(row){
    this.entradaStatus = {idUsuario:this.entradaStatus.idUsuario,idEntrada:row.idEntrada,verEntrada:row.versEntrada,idEstatus:row.idEstOperacion};
    this.http.EliminarEntradas(this.entradaStatus).subscribe(result=>{
      console.log(result);
      this.renderTable();
    },error=>{console.log(<any>error);});
  }

  TraspasarDatos(row,param){
    this.util.getDatos(row);
    switch(param){
      case 1:
        this.util.navegacion("entradas/editar");
      break;
      case 2:
        this.util.navegacion("entradas/nueva-version");
      break;
      case 3:
        this.util.navegacion("entrada/ver-version");
      break;
    }
  }
}

@Component({
  selector:"entradaEstatus",
  templateUrl:"entradaEstatus.html"
})

export class entradaEstatus implements OnInit {
  constructor(public modal:MatDialogRef<entradaEstatus>,public http:EntradasService,@Inject(MAT_DIALOG_DATA) public status:EntradaEstatus){}
  estatus;
  ngOnInit(){
    this.Estatus();
  }

  Estatus(){
    this.http.Estatus().subscribe(result=>{
      this.estatus = result;
    },error=>{console.log(<any>error);});
  }

  CambiarEstatus(){
    this.http.EstatusCambio(this.status).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },error=>{console.log(<any>error);});
  }

  Cancelar(){
    this.modal.close();
  }

}

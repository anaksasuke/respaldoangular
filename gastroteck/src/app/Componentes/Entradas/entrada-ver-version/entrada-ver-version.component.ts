import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { UtilService } from '../../../util.service';
import { EntradasService } from 'src/app/Servicios/entradas.service';
import { Entrada } from 'src/app/Modelos/Entrada';
import { EntradaArticulo } from 'src/app/Modelos/EntradaArticulo';


@Component({
  selector: 'app-entrada-ver-version',
  templateUrl: './entrada-ver-version.component.html',
  styleUrls: ['./entrada-ver-version.component.scss']
})
export class EntradaVerVersionComponent implements OnInit {

  constructor(public util:UtilService,public http:EntradasService) { }
  entrada = new Entrada(0,0,'','','','',0,0,0,0,0);
  entradaArticulo = new  EntradaArticulo('','','','',0,0,0,0,0,0,0,0,0,0,"",0,"",0);
  Columns = ['nomOrden','nomCotizacion','areaCotizacion','idPartida','modArticulo','nomArticulo','resArticulo','cantArticulo','impArticulo','cveMoneda'];
  dataSource:any;
  validadorHtml = 0; nomPro =''; ArticuloFaltante=0; disabled=0;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;

  ngOnInit() {
    var datos = this.util.showDatos();
    this.entrada.idUsuario = localStorage.Usuario;
    this.CargarDatos(datos);
  }

  CargarDatos(datos){ 
    this.nomPro = datos.nomProveedor;
    this.entrada = {idUsuario:this.entrada.idUsuario,idProveedor:datos.idProveedor,Facturas:datos.factEntrada,Referencia:datos.refEntrada,gastos:datos.gtosEntrada,fecha:moment(datos.fRecEntrada).format("YYYY-MM-DD"),idEntrada:datos.idEntrada,versionEntrada:datos.versEntrada,observaciones:datos.obsEntrada,empaque:datos.empEntrada,instalacion:datos.insEntrada};
    this.CargarEntrada();
  }

  CargarEntrada(){
    this.http.CargarEntradaArt(this.entrada).subscribe(result=>{
      console.log(result);
      this.renderTable();
    },error=>{console.log(<any>error);});
  }

  renderTable(){
    this.http.VistaArticulos(this.entrada.idUsuario).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
    },error=>{console.log(<any>error);});
  }

  CargarDatosArt(row){
    this.validadorHtml =1; 
    this.ArticuloFaltante = row.resArticulo;
    this.entradaArticulo = {nomOrd:row.nomOrden,nomCot:row.nomCotizacion,idArea:row.areaCotizacion,idPartida:row.idPartida,modeloArt:row.modArticulo,nomArt:row.nomArticulo,idOrd:row.idOrden,ordVer:row.versOrden,idCot:row.idCotizacion,cotVer:row.versCotizacion,idArt:row.idArticulo,cantidad:row.cantArticulo,idIva:row.idIva,iva:row.iva,idMoneda:row.idMoneda,moneda:row.cveMoneda,precio:row.cosArticulo,idUsuario:this.entrada.idUsuario};
  }

  ValidarButton(){
    if(parseFloat(this.entradaArticulo.cantidad.toString())  > parseFloat(this.ArticuloFaltante.toString())  || this.entradaArticulo.cantidad == 0 || (this.entradaArticulo.cantidad.toString()) ==""){
      this.disabled = 1;
    }else {
      this.disabled = 0;
    }
  }

  Actualizar(){
    this.http.ActualizarArt(this.entradaArticulo).subscribe(result=>{
      console.log(result);
      this.renderTable();
      this.CancelarArt();
    },error=>{console.log(<any>error);});
  }

  CancelarArt(){
    this.entradaArticulo = {nomOrd:"",nomCot:"",idArea:0,idPartida:0,modeloArt:"",nomArt:"",idOrd:0,ordVer:0,idCot:0,cotVer:0,idArt:0,cantidad:0,idIva:0,iva:"",idMoneda:0,moneda:"",precio:0,idUsuario:this.entrada.idUsuario};
    this.validadorHtml = 0;
    this.ArticuloFaltante = 0;
  }

  EditarEntrada(){
    this.http.EditarEntrada(this.entrada).subscribe(result=>{
      console.log(result);
      this.util.NavegacionDinamica(result,'entradas');
    },error=>{console.log(<any>error);});
  }

  CancelarEntrada(){
    this.util.navegacion("entradas");
  }

}

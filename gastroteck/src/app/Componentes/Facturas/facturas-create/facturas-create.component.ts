import { Component, OnInit, ViewChild, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FacturasService } from '../../../Servicios/facturas.service';
import { UtilService } from '../../../util.service';
import { ClienteFacturaInter } from '../../../Interfaces/ClienteFactura';
import { CotizacionInter } from '../../../Interfaces/Cotizacion';
import { FacturasCarArt } from '../../../Modelos/FacturasCarArt';
import { Facturas } from '../../../Modelos/Facturas';
import { ArticuloInter } from '../../../Interfaces/Articulo';
import { FactArticulos } from '../../../Modelos/FactArticulos';
import { FacturaPop } from '../../../Modelos/FacturasPop';

export class FacturaTotales{
  constructor(
    public subArticulos:number, 
    public ivArticulos:number,
    public retArticulos:number,
    public impArticulos:number
  ){}
}

@Component({
  selector: 'app-facturas-create',
  templateUrl: './facturas-create.component.html',
  styleUrls: ['./facturas-create.component.scss']
})
export class FacturasCreateComponent implements OnInit {
  //Arreglos estaticos para pagos
  TipoDePago = [{id:"1",vista:"Contado"},{id:"2",vista:"Credito"}];
  Retenciones = [{id:"1",vista:"No"},{id:"2",vista:"Si"}];
  MetodoDePago = [{id:"1",vista:"PUE - pago con solo una existencia"},{id:"2",vista:"PPD - pago parcial o diferido"}];
  //Variables para validar o guardar datos
  tF=0; nomArt = ""; ubicador=0;
  //Arreglos para selects, rellenados con la base de datos
  Areas; Monedas; Ivas; Proveedor; tFactura; FormaDePago; cfdi; retencion = 1;
  //Totales
  totales = new FacturaTotales(0,0,0,0);
  stateArticulo = new FormControl();
  FilterArticulo:Observable<ArticuloInter[]>;
  Articulos:ArticuloInter[]=[];

  stateCliente = new FormControl();
  FilteredCliente:Observable<ClienteFacturaInter[]>;
  Cliente:ClienteFacturaInter[]=[];

  stateCotizacion = new FormControl();
  FilteredCotizacion:Observable<CotizacionInter[]>;
  Cotizacion:CotizacionInter[]=[];

  Columns=['acciones','nomCotizacion', 'idPartida', 'codSatArticulo', 'modArticulo', 'nomArticulo','nomProveedor','cantArticulo',
            'cosArticulo', 'iva', 'cveMedArticulo', 'subArticulo', 'ivArticulo', 'retArticulo', 'impArticulo'];
  dataSource:any;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;

  constructor(public http:FacturasService,public util:UtilService,public dialog:MatDialog) { }
  facturaCarArt = new FacturasCarArt(0,0,0);
  factura = new Facturas(0,0,0,0,0,0,0,'',0,0,0,'',0,0);
  facturaLib = new FactArticulos (0,0,0,0,0,0,0,0,0,0,0,0,0,0);
  facturaPop = new FacturaPop(0,0,0,0,0,0,0);
  ngOnInit(){
    this.factura.idUsuario = localStorage.Usuario;
    this.facturaCarArt.idUsuario = this.factura.idUsuario;
    this.facturaLib.idUser = this.factura.idUsuario;
    this.Clientes();
    this.FormasPago();
    this.CFDI();
    this.TipoFactura();
    this.TablaArticulos();
  }

  Clientes(){
    this.http.Clientes().subscribe(result => {
      this.Cliente = Object.values(result);
      this.FilteredCliente = this.stateCliente.valueChanges.pipe(startWith(''),map(cli => cli ? this.filtercli(cli):this.Cliente.slice()));
    }, error => { console.log(<any>error); });
  }

  filtercli(value:String):ClienteFacturaInter[]{
    const filterValue = value.toLowerCase();
    return this.Cliente.filter(cli => cli.razSocCliente.toLowerCase().indexOf(filterValue)===0);
  }

  ObtCliente(cliente){
    if(this.factura.idCliente != cliente && this.factura.idCliente != 0){
      this.factura.idCliente = cliente;
      this.LimpiadoTabla();
    } else{
      this.factura.idCliente = cliente;
    }
  }

  FormasPago(){
    this.http.FormaPago().subscribe(result => {
      this.FormaDePago = result;
    }, error => { console.log(<any>error); });
  }

  CFDI(){
    this.http.CFDI().subscribe(result => {
      this.cfdi = result;
    }, error => { console.log(<any>error); });
  }

  TipoFactura(){
    this.http.TipoFactura().subscribe(result => {
      this.tFactura = result;
    }, error => { console.log(<any>error); });
  }

  ValidarTipo(tipo){
    switch(parseInt(tipo)){
      case 1 : //Por Cotizacion 
          this.Cotizaciones();
      break;
      case 3 : //Libre
          this.CargarServicios();
      break;
      case 4 : //Anticipo
          this.Cotizaciones();
      break;
      case 5 : //Finiquito 
          this.Cotizaciones();
      break;
    }
  }

  CambioRet(idRet){
    this.retencion = idRet;
    this.TablaArticulos();
  }

  CambioTipo(idTipo){
    if(this.tF != idTipo && this.tF != 0){
        this.tF = idTipo;
        this.LimpiadoTabla();
        this.ValidarTipo(idTipo);
        this.Cancelar();
    } else{
      this.tF = idTipo;
      this.ValidarTipo(idTipo);
    }
  }

  Cotizaciones(){
    this.http.CotizacionCli(this.factura.idCliente).subscribe(result => {
        this.Cotizacion = Object.values(result);
        this.FilteredCotizacion = this.stateCliente.valueChanges.pipe(startWith(''),map(cot => cot ? this.filterCot(cot):this.Cotizacion.slice()));
    }, error => { console.log(<any>error); });
  }

  filterCot(value:string):CotizacionInter[]{
    const filterValue = value.toLowerCase();
    return this.Cotizacion.filter(cot => cot.nomCotizacion.toLowerCase().indexOf(filterValue)===0);
  }

  ObtCotizacion(cotizacion){
    this.facturaCarArt = {idUsuario:this.factura.idUsuario,idCot:cotizacion.idCotizacion,version:cotizacion.versCotizacion};
    switch(parseInt(this.factura.tFactura.toString())){
      case 1: //Basado en cotizacion
        this.FacturaCotizacion(); 
      break;
      case 4: //Anticipo
        this.FacturaAnticipo();
      break;
      case 5: //Finiquito
        this.ConteoMon();
      break;
    }
  }

  FacturaCotizacion(){
    this.http.RellenarTable(this.facturaCarArt).subscribe(result => {
      console.log(result);
      this.TablaArticulos();
    }, error => { console.log(<any>error); });
  }  

  FacturaAnticipo(){
    this.http.FacturaAnticipo(this.facturaCarArt).subscribe(result => {
      console.log(result);
      this.TablaArticulos();
    }, error => { console.log(<any>error); });
  }

  ConteoMon(){
    this.http.ConteoMoneda(this.facturaCarArt.idCot,this.facturaCarArt.version).subscribe(result => {
      if(Object.values(result).length > 1){
        this.OpendDialog(4);
      } else{
          Object.values(result).forEach(element => {
            this.facturaPop.idMoneda = element.idMoneda;
          });
        this.facturaPop = {idUsuario:this.factura.idUsuario,idCotizacion:this.facturaCarArt.idCot,version:this.facturaCarArt.version,idMoneda:this.facturaPop.idMoneda,idMonedaDestino:0,valorCambio:0,ubicador:4}
        this.FacturaFiniquito();
      }
    }, error => { console.log(<any>error); });
  }

  FacturaFiniquito(){
    this.http.Finiquito(this.facturaPop).subscribe(result => {
      console.log(result);
      this.TablaArticulos();
    }, error => { console.log(<any>error); });
  }

  TablaArticulos(){
    this.http.TablaArticulos(this.factura.idUsuario).subscribe(result => {
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator; 
      this.getTotales();     
    }, error=>{console.log(<any>error);});   
  }

  getTotales(){
    this.totales.subArticulos = this.dataSource.data.map(t => t.subArticulo).reduce((acc, value) => acc + parseFloat(value), 0);
    this.totales.ivArticulos = this.dataSource.data.map(t => t.ivArticulo).reduce((acc, value) => acc + parseFloat(value), 0);
    this.totales.retArticulos = this.dataSource.data.map(t => t.retArticulo).reduce((acc, value) => acc + parseFloat(value), 0);
    //if(this.retencion == 1){
      this.totales.impArticulos = this.dataSource.data.map(t => t.impArticulo).reduce((acc, value) => acc + parseFloat(value), 0);
    /*} else {
      this.totales.impArticulos = this.dataSource.data.map(t => t.impArticulo).reduce((acc, value) => acc + parseFloat(value), 0) - this.totales.retArticulos;
    }*/
  }

  //Servicios para cotizaciones Libres 

  CargarServicios(){
    this.ArticulosSat();
    this.Iva();
    this.Moneda();
  }

  ArticulosSat(){
    this.http.CodSat().subscribe(result => {
      this.Articulos = Object.values(result);
      this.FilterArticulo = this.stateArticulo.valueChanges.pipe(startWith(''),map(art => art ? this.filterArt(art):this.Articulos.slice()));
    }, error => { console.log(<any>error); });
  }

  filterArt(value:string):ArticuloInter[]{
    const filterValue = value.toLowerCase();
    return this.Articulos.filter(art => art.codSatArticulo.indexOf(filterValue)===0);
  }

  ObtProveedor(Art){
    this.nomArt = Art.nomArticulo;
    this.facturaLib.idArticulo = Art.idArticulo;
    this.http.ArticuloPro(Art.idArticulo).subscribe(result => {
      this.Proveedor = result;
    }, error => { console.log(<any>error); });
  }

  ObtInformacionArt(idProveedor){
    this.http.DatosArticulo(idProveedor,this.facturaLib.idArticulo).subscribe(result => {
      Object.values(result).forEach(element => {
        this.facturaLib.precio = element.cBaseArticulo;
        this.facturaLib.idMoneda = element.idMoneda;
      });
    }, error => { console.log(<any>error); });
  }

  Iva(){
    this.http.Ivas().subscribe(result => {
      this.Ivas = result;
    }, error => { console.log(<any>error); });
  }

  Moneda(){
    this.http.Monedas().subscribe(result => {
      this.Monedas = result;
    }, error => { console.log(<any>error); });
  }

  GuardarArticulo(){
    this.http.AgregarArticulo(this.facturaLib).subscribe(result => {
      console.log(result);
      this.TablaArticulos();
      this.Cancelar();
    }, error => { console.log(<any>error); });
  }
  
  // Terminan metodos para facuturas libres 

  LimpiadoTabla(){
    this.http.CambioCliente(this.factura).subscribe(result => {
      this.TablaArticulos();
    }, error => { console.log(<any>error); });
  }

  CargarDatos(row){
    if(this.factura.tFactura == 1 || this.factura.tFactura == 3){
      this.ubicador = 1;
      this.facturaLib = {idAreaCot:row.idAreaCotizacion,idArticulo:row.idArticulo,idCot:row.idCotizacion,versionCot:row.versCotizacion,idProveedor:row.idProveedor,idPartida:row.idPartida,idMoneda:row.idMoneda,cantidad:row.cantArticulo,precio:row.cosArticulo,idIva:row.idIva,marge:row.margArticulo,descuento:row.dctoArticulo,idUser:this.factura.idUsuario,CodigoSAT:row.codSatArticulo};
      this.stateArticulo.setValue(row.codSatArticulo + ' (' + row.nomArticulo + ' - ' + row.modArticulo + ')');
      this.stateArticulo.disable();
      this.ObtProveedor(row);
      if(this.factura.tFactura == 3)
        this.ObtInformacionArt(this.facturaLib.idProveedor);
    } else{
      this.CargarServicios();
      this.ubicador = 1;
      this.facturaLib = {idAreaCot:row.idAreaCotizacion,idArticulo:row.idArticulo,idCot:row.idCotizacion,versionCot:row.versCotizacion,idProveedor:row.idProveedor,idPartida:row.idPartida,idMoneda:row.idMoneda,cantidad:row.cantArticulo,precio:row.cosArticulo,idIva:row.idIva,marge:row.margArticulo,descuento:row.dctoArticulo,idUser:this.factura.idUsuario,CodigoSAT:row.codSatArticulo};
      this.ObtProveedor(row);
    }
  }

  EditarArticulo(){
    this.http.EditarArticulo(this.facturaLib).subscribe(result => {
      console.log(result);
      this.TablaArticulos();
      this.Cancelar();
    }, error => { console.log(<any>error); });
  }

  Eliminar(row){
    this.facturaLib = {idAreaCot:row.idAreaCotizacion,idArticulo:row.idArticulo,idCot:row.idCotizacion,versionCot:row.versCotizacion,idProveedor:row.idProveedor,idPartida:row.idPartida,idMoneda:row.idMoneda,cantidad:row.cantArticulo,precio:row.cosArticulo,idIva:row.idIva,marge:row.margArticulo,descuento:row.dctoArticulo,idUser:this.factura.idUsuario,CodigoSAT:row.codSatArticulo};
    this.http.EliminarArticulo(this.facturaLib).subscribe(result => {
      console.log(result);
      this.TablaArticulos();
    }, error => { console.log(<any>error); });
  }

  Cancelar(){
    this.nomArt = "";
    this.facturaLib = {idAreaCot:0,idArticulo:0,idCot:0,versionCot:0,idProveedor:0,idPartida:0,idMoneda:0,cantidad:0,precio:0,idIva:0,marge:0,descuento:0,idUser:this.factura.idUsuario,CodigoSAT:0};
    this.ubicador = 0;
    this.stateArticulo.reset();
    if(this.factura.tFactura == 3)
      this.stateArticulo.enable();
  } 
  
  CancelarF(){
    this.http.CambioCliente(this.factura).subscribe(result => {
      this.TablaArticulos();
    }, error => { console.log(<any>error); });
  }

  OpendDialog(valor:number){
    this.facturaPop = {idUsuario:this.factura.idUsuario,idCotizacion:this.facturaCarArt.idCot,version:this.facturaCarArt.version,idMoneda:0,idMonedaDestino:0,valorCambio:0,ubicador:valor}
    const dialogRef = this.dialog.open(facturaMod,{
      width:'auto',
      height:'auto',
      data:this.facturaPop
    });
    dialogRef.afterClosed().subscribe(result => {
      this.TablaArticulos();
    });
  }

  Guardar(){
    this.http.GuardarFactura(this.factura).subscribe(result => {
      console.log(result);
      this.util.FacturaNavegacion(result,'facturas');
    }, error => { console.log(<any>error); });
  }
}

@Component({
  selector:"facturaMod",
  templateUrl:"facturaMod.html",
  styleUrls: ['./facturas-create.component.scss']
})

export class facturaMod implements OnInit {
  constructor(public modal:MatDialogRef<facturaMod>,public http:FacturasService,@Inject(MAT_DIALOG_DATA) public generico:FacturaPop){}
  moneda; moneda2; cotizaciones; 
  // Ubicador 1 -- Borrar por moneda;
  // Ubicador 2 -- Borrar por Cotizacion;
  // Ubicador 3 -- Convertidor de moneda;
  // Ubicador 4 -- Finiquito
  ngOnInit(){
    this.Ubicador();
  }

  Ubicador(){
    switch(this.generico.ubicador){
      case 1:
        this.Monedas();
      break;
      case 2:
        this.Cotiazion();
      break;
      case 3:
        this.Monedas();
      break;
      case 4:
        this.MonedasFiniquito();
      break;
    }
  }

  Monedas(){
    this.http.MondaFactura(this.generico.idUsuario).subscribe(result => {
      this.moneda = result;
    }, error => { console.log(<any>error); });
  }

  BorrarMoneda(){
    this.http.BorrarMoneda(this.generico).subscribe(result => {
      this.modal.close();
    }, error => { console.log(<any>error); });
  }

  MonedaDestino(){
    this.http.OtraMoneda(this.generico).subscribe(result =>{
        if(Object.values(result).length == 0){
          this.http.Monedas().subscribe(result => {
            this.moneda2 = result;
          }, error => { console.log(<any>error); });
        } else{
          this.moneda2 = result;
        }
    }, error => { console.log(<any>error); });
  }

  ObtValor(){
    this.http.ValorConversion(this.generico.idMoneda,this.generico.idMonedaDestino).subscribe(result => {
      Object.values(result).forEach(element => {
        this.generico.valorCambio = element.convMoneda;
      })
    }, error => { console.log(<any>error); });
  }

  Convertir(){
    this.http.Conversion(this.generico).subscribe(result => {
      console.log(result);
      this.modal.close();
    }, error => { console.log(<any>error); });
  }

  Cotiazion(){
    this.http.CotizacionFactura().subscribe(result => {
      this.cotizaciones = result;
    }, error => { console.log(<any>error); });
  }

  ObtVersion(version){
    this.generico.version = version;
  }

  BorrarCotizacion(){
    this.http.BorrarCotizacion(this.generico).subscribe(result => {
      console.log(result);
      this.modal.close();
    }, error => { console.log(<any>error); });
  }

  MonedasFiniquito(){
    this.http.ConteoMoneda(this.generico.idCotizacion,this.generico.version).subscribe(result => {
      console.log(result);
      this.moneda = result;
    }, error => { console.log(<any>error); });
  }

  Finiquito(){
    this.http.Finiquito(this.generico).subscribe(result => {
      console.log(result);
      this.modal.close();
    }, error => { console.log(<any>error); });
  }

  Cancelar(){
    this.modal.close();
  }

} 

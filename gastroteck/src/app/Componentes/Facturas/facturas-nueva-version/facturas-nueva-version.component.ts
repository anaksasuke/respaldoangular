import { Component, OnInit ,ViewChild} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { UtilService } from '../../../util.service';
import { FacturasService } from '../../../Servicios/facturas.service';
import { Facturas } from '../../../Modelos/Facturas';
import { FacturasCarArt } from '../../../Modelos/FacturasCarArt';
import { FactArticulos } from '../../../Modelos/FactArticulos';
import { ArticuloInter } from '../../../Interfaces/Articulo';
import { CotizacionInter } from '../../../Interfaces/Cotizacion';
import { ClienteFacturaInter } from '../../../Interfaces/ClienteFactura';
import { FacturaPop } from '../../../Modelos/FacturasPop';
import { facturaMod } from '../facturas-create/facturas-create.component';
@Component({
  selector: 'app-facturas-nueva-version',
  templateUrl: './facturas-nueva-version.component.html',
  styleUrls: ['./facturas-nueva-version.component.scss']
})
export class FacturasNuevaVersionComponent implements OnInit {
  //Arreglos estaticos para pagos
  TipoDePago = [{id:"1",vista:"Contado"},{id:"2",vista:"Credito"}];
  MetodoDePago = [{id:"1",vista:"PUE - pago con solo una existencia"},{id:"2",vista:"PPD - pago parcial o diferido"}];
  //Variables para validar o guardar datos
  tF=0; nomArt = ""; ubicador=0;
  //Arreglos para selects, rellenados con la base de datos
  Areas; Monedas; Ivas; Proveedor; tFactura; FormaDePago; cfdi;  
  stateArticulo = new FormControl();
  FilterArticulo:Observable<ArticuloInter[]>;
  Articulos:ArticuloInter[]=[];

  stateCliente = new FormControl();
  FilteredCliente:Observable<ClienteFacturaInter[]>;
  Cliente:ClienteFacturaInter[]=[];

  stateCotizacion = new FormControl();
  FilteredCotizacion:Observable<CotizacionInter[]>;
  Cotizacion:CotizacionInter[]=[];  
  constructor(public util:UtilService,public http:FacturasService,public dialog:MatDialog) { }
  Columns=['nomCotizacion','areaCotizacion','codSatArticulo','nomArticulo','nomProveedor','cantArticulo','subArticulo','impArticulo','iva','cveMoneda',"Acciones"];
  dataSource:any;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  factura = new Facturas(0,0,0,0,0,0,0,'',0,0,0,'',0,0);
  facturaCarArt = new FacturasCarArt(0,0,0);
  facturaLib = new FactArticulos (0,0,0,0,0,0,0,0,0,0,0,0,0,0);
  facturaPop = new FacturaPop(0,0,0,0,0,0,0);
  ngOnInit() {
    this.CargarFactura();
    this.FormasPago();
    this.CFDI();
    this.TipoFactura();
  }

  CargarFactura(){
    var datos = this.util.showDatos();
    this.factura = {idCliente:datos.idCliente,idFactura:datos.idFactura,versionFactura:datos.versFactura,tPago:datos.tPago,mPago:datos.mPago,fPago:datos.idFormP,plazo:datos.plazo,folioFis:this.factura.folioFis,CFDI:datos.idCFDI,CFDIRel:this.factura.CFDIRel,anticipo:datos.anticipo,referencia:datos.referencia,tFactura:datos.idTipoF,idUsuario:localStorage.Usuario};
    this.stateCliente.setValue(datos.razSocCliente);
    this.stateCliente.disable();
    this.stateCotizacion.setValue(datos.cotFactura);
    this.stateCotizacion.disable();
    this.facturaCarArt = {idUsuario:this.factura.idUsuario,idCot:this.factura.idFactura,version:this.factura.versionFactura};
    this.CargarArticuloFactura();
    this.ValidarTipo(this.factura.tFactura);
  }

  CargarArticuloFactura(){
    this.http.CargarArticulos(this.facturaCarArt).subscribe(result=>{
      console.log(result);
      this.TablaArticulos();
    },error=>{console.log(<any>error);});
  }

  ValidarTipo(tipo){
    switch(parseInt(tipo)){
      case 1 :
          //Por Cotizacion 
          this.Cotizaciones();
      break;
      case 3 :
          //Libre
          this.CargarServicios();
      break;
    }
  }

  Cotizaciones(){
    this.http.CotizacionCli(this.factura.idCliente).subscribe(result=>{
        this.Cotizacion = Object.values(result);
        this.FilteredCotizacion = this.stateCliente.valueChanges.pipe(startWith(''),map(cot => cot ? this.filterCot(cot):this.Cotizacion.slice()));
    },error=>{console.log(<any>error);});
  }

  filterCot(value:string):CotizacionInter[]{
    const filterValue = value.toLowerCase();
    return this.Cotizacion.filter(cot => cot.nomCotizacion.toLowerCase().indexOf(filterValue)===0);
  }

  ObtCotizacion(cotizacion){
    this.facturaCarArt = {idUsuario:this.factura.idUsuario,idCot:cotizacion.idCotizacion,version:cotizacion.versCotizacion};
    switch(parseInt(this.factura.tFactura.toString())){
      case 1: 
        this.FacturaCotizacion(); 
      break;
    }
  }

  FacturaCotizacion(){
    this.http.RellenarTable(this.facturaCarArt).subscribe(result=>{
      console.log(result);
      this.TablaArticulos();
    },error=>{console.log(<any>error);});
  }  

  FormasPago(){
    this.http.FormaPago().subscribe(result=>{
      this.FormaDePago = result;
    },error=>{console.log(<any>error);});
  }

  CFDI(){
    this.http.CFDI().subscribe(result=>{
      this.cfdi = result;
    },error=>{console.log(<any>error);});
  }

  TipoFactura(){
    this.http.TipoFactura().subscribe(result=>{
      this.tFactura = result;
    },error=>{console.log(<any>error);});
  }

  TablaArticulos(){
    this.http.TablaArticulos(this.factura.idUsuario).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
    },error=>{console.log(<any>error);});
  }

  LimpiadoTabla(){
    this.http.CambioCliente(this.factura).subscribe(result=>{
      this.TablaArticulos();
    },error=>{console.log(<any>error);});
  }

  CargarServicios(){
    this.ArticulosSat();
    this.Iva();
    this.Moneda();
  }

  ArticulosSat(){
    this.http.CodSat().subscribe(result=>{
      this.Articulos = Object.values(result);
      this.FilterArticulo = this.stateArticulo.valueChanges.pipe(startWith(''),map(art => art ? this.filterArt(art):this.Articulos.slice()));
    },error=>{console.log(<any>error);});
  }

  filterArt(value:string):ArticuloInter[]{
    const filterValue = value.toLowerCase();
    return this.Articulos.filter(art => art.codSatArticulo.indexOf(filterValue)===0);
  }

  ObtProveedor(Art){
    this.nomArt = Art.nomArticulo;
    this.facturaLib.idArticulo = Art.idArticulo;
    this.http.ArticuloPro(Art.idArticulo).subscribe(result=>{
      this.Proveedor = result;
    },error=>{console.log(<any>error);});
  }

  ObtInformacionArt(idProveedor){
    this.http.DatosArticulo(idProveedor,this.facturaLib.idArticulo).subscribe(result=>{
      Object.values(result).forEach(element=>{
        this.facturaLib.precio = element.cRepArticulo;
        this.facturaLib.idMoneda = element.idMoneda;
      });
    },error=>{console.log(<any>error);});
  }

  Iva(){
    this.http.Ivas().subscribe(result=>{
      this.Ivas = result;
    },error=>{console.log(<any>error);});
  }

  Moneda(){
    this.http.Monedas().subscribe(result=>{
      this.Monedas = result;
    },error=>{console.log(<any>error);});
  }

  GuardarArticulo(){
    this.http.AgregarArticulo(this.facturaLib).subscribe(result=>{
      console.log(result);
      this.TablaArticulos();
      this.Cancelar();
    },error=>{console.log(<any>error);});
  }

  CargarDatos(row){
    this.CargarServicios();
    if(this.factura.tFactura == 1 || this.factura.tFactura == 3){
      this.ubicador = 1;
      this.facturaLib = {idAreaCot:row.idAreaCotizacion,idArticulo:row.idArticulo,idCot:row.idCotizacion,versionCot:row.versCotizacion,idProveedor:row.idProveedor,idPartida:row.idPartida,idMoneda:row.idMoneda,cantidad:row.cantArticulo,precio:row.cosArticulo,idIva:row.idIva,marge:row.margArticulo,descuento:row.dctoArticulo,idUser:this.factura.idUsuario,CodigoSAT:row.codSatArticulo};
      this.stateArticulo.setValue(row.codSatArticulo + ' (' + row.nomArticulo + ')');
      this.stateArticulo.disable();
      this.ObtProveedor(row);
      this.ObtInformacionArt(this.facturaLib.idProveedor);
    }else {
      this.ubicador = 1;
      this.stateArticulo.setValue(row.codSatArticulo + ' (' + row.nomArticulo + ')');
      this.stateArticulo.disable();
      this.facturaLib = {idAreaCot:row.idAreaCotizacion,idArticulo:row.idArticulo,idCot:row.idCotizacion,versionCot:row.versCotizacion,idProveedor:row.idProveedor,idPartida:row.idPartida,idMoneda:row.idMoneda,cantidad:row.cantArticulo,precio:row.cosArticulo,idIva:row.idIva,marge:row.margArticulo,descuento:row.dctoArticulo,idUser:this.factura.idUsuario,CodigoSAT:row.codSatArticulo};
      this.ObtProveedor(row);
    }
  }

  EditarArticulo(){
    this.http.EditarArticulo(this.facturaLib).subscribe(result=>{
      console.log(result);
      this.TablaArticulos();
      this.Cancelar();
    },error=>{console.log(<any>error);});
  }

  Eliminar(row){
    this.facturaLib = {idAreaCot:row.idAreaCotizacion,idArticulo:row.idArticulo,idCot:row.idCotizacion,versionCot:row.versCotizacion,idProveedor:row.idProveedor,idPartida:row.idPartida,idMoneda:row.idMoneda,cantidad:row.cantArticulo,precio:row.cosArticulo,idIva:row.idIva,marge:row.margArticulo,descuento:row.dctoArticulo,idUser:this.factura.idUsuario,CodigoSAT:row.codSatArticulo};
    this.http.EliminarArticulo(this.facturaLib).subscribe(result=>{
      console.log(result);
      this.TablaArticulos();
    },error=>{console.log(<any>error);});
  }

  Cancelar(){
    this.nomArt = "";
    this.facturaLib = {idAreaCot:0,idArticulo:0,idCot:0,versionCot:0,idProveedor:0,idPartida:0,idMoneda:0,cantidad:0,precio:0,idIva:0,marge:0,descuento:0,idUser:this.factura.idUsuario,CodigoSAT:0};
    this.ubicador = 0;
    this.stateArticulo.reset();
  } 

  OpendDialog(valor:number){
    this.facturaPop = {idUsuario:this.factura.idUsuario,idCotizacion:this.facturaCarArt.idCot,version:this.facturaCarArt.version,idMoneda:0,idMonedaDestino:0,valorCambio:0,ubicador:valor}
    const dialogRef = this.dialog.open(facturaMod,{
      width:'auto',
      height:'auto',
      data:this.facturaPop
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.TablaArticulos();
    });
  }

  Guardar(){
    this.http.GuardarFactura(this.factura).subscribe(result=>{
      console.log(result);
      this.util.FacturaNavegacion(result,'facturas');
    },error=>{console.log(<any>error);});
  }

}

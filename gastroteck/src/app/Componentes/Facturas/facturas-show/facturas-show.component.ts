import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UtilService } from '../../../util.service';
import { FacturasService } from '../../../Servicios/facturas.service';
import { FacturaEliminar } from '../../../Modelos/FacturaEliminar';
import { CambiarEstatusFactura } from '../../../Modelos/CambiarEstatusFact';
import { Facturas } from 'src/app/Modelos/Facturas';
import { FacturaFolio } from '../../../Modelos/FacturaFolio';


@Component({
  selector: 'app-facturas-show',
  templateUrl: './facturas-show.component.html',
  styleUrls: ['./facturas-show.component.scss']
})
export class FacturasShowComponent implements OnInit {
  Columns=["nomFactura", 'folio', "estOperacion","cotFactura","relFactura","cliente","total","pagos", 'pagado', 'restante', "acciones"];
  dataSource:any;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  constructor(public util:UtilService,public http:FacturasService,public dialog:MatDialog) { }
  FactBorrar =  new FacturaEliminar(0,0,0);
  FacCambiarEstatus = new CambiarEstatusFactura(0,0,0,0,0);
  FacturaFolio = new FacturaFolio(0,0,0,'');
  ngOnInit() {
    this.renderTable();
    this.FactBorrar.idUsuario = localStorage.Usuario;
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  renderTable(){
    this.http.Facturas().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },error=>{console.log(<any>error);});
  }

  EditarVersion(row){
    this.util.getDatos(row);
    this.util.navegacion("facturas/editar-version");
  }

  VerVersion(row){
    this.util.getDatos(row);
    this.util.navegacion("facturas/ver-version");
  }

  NuevaVersion(row){
    this.util.getDatos(row);
    this.util.navegacion("facturas/nueva-version");
  }

  BorrarFactura(row){
    this.FactBorrar = {idFactura:row.idFactura,idUsuario:this.FactBorrar.idUsuario,version:row.versFactura};
    this.http.BorrarFacturas(this.FactBorrar).subscribe(result=>{
      console.log(result);
    },error=>{console.log(<any>error);});
  }


  OpenModal():void {
    const dialogRef = this.dialog.open(EstatusFac,{
      width:"400px",
      height:"400px",
      data:this.FacCambiarEstatus
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.renderTable();
    });
  }

  AgregarFolio(row){
    this.FacturaFolio = {idUsuario:this.FactBorrar.idUsuario,idFactura:row.idFactura,version:row.versFactura,folioFis:row.folio};
    const dialogRef = this.dialog.open(FolioFiscalPop,{
      width:"400px",
      height:"300px",
      data:this.FacturaFolio
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.renderTable();
    });
  }

  CargarEstatus(row){
    this.FacCambiarEstatus = {idUsuario:this.FactBorrar.idUsuario,idFactura:row.idFactura,version:row.versFactura,idEstatus:row.idEstOperacion,idTipo:row.idTipoF};
    this.OpenModal();
  }

}

@Component({
  selector:"EstatusFac",
  templateUrl:"EstatusFac.html",
  styleUrls: ['./facturas-show.component.scss']
})

export class EstatusFac implements OnInit{
  estatus;
  constructor(public modal:MatDialogRef<EstatusFac>,public http:FacturasService,@Inject(MAT_DIALOG_DATA) public EstatusFac:CambiarEstatusFactura){}
  
  ngOnInit(){
    this.Estatus();
  }

  Estatus(){
    this.http.Estatus().subscribe(result=>{
      this.estatus = result;
    },error=>{console.log(<any>error);});
  }
  
  onNoClick():void{
    this.modal.close();
  }

  CambiarEstatus(){
    this.http.CambiarEstatus(this.EstatusFac ).subscribe(result=>{
      this.modal.close();
      console.log(result);
    },error=>{console.log(<any>error);});
  }

}

@Component({
  selector:"FolioFiscalPop",
  templateUrl:"FolioFiscalPop.html",
  styleUrls: ['./facturas-show.component.scss']
})

export class FolioFiscalPop implements OnInit {
  constructor(public modal:MatDialogRef<FolioFiscalPop>,public http:FacturasService,@Inject(MAT_DIALOG_DATA) public facturas:FacturaFolio){}
  folioButton; factura;
  ngOnInit(){
    if(this.facturas.folioFis == "Pendiente"){
      this.folioButton = 0;
    }
  }

  ObtnerFiles(evt:any){
    var archivo = evt.target.files[0];
    var lector = new FileReader();
    this.factura = archivo.name;
    console.log(this.factura);
    lector.readAsText(archivo);
    lector.onload = function (e:any) {
      var contenido = e.target.result;
      (<HTMLInputElement>document.getElementById("xml")).value = contenido.toString();
    };
  }

  Parceo(){
    var xml = (<HTMLInputElement>document.getElementById("xml")).value;
    var renglon = xml.substring(xml.indexOf('<cfdi:Complemento>'),xml.indexOf('</cfdi:Complemento>'));
    renglon = renglon.substring(renglon.indexOf('UUID="') + 6,renglon.indexOf('UUID="')+42);
    this.facturas.folioFis = renglon;
    if(this.facturas.folioFis != "Pendiente"){
      this.folioButton = 1;
    }
  }

  Guardar(){
    this.http.AgregarFolio(this.facturas).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },error=>{console.log(<any>error);});
  }

  Cancelar(){
    this.modal.close();
  }

}
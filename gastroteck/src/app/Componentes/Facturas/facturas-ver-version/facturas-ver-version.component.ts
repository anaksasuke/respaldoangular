import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FacturasService } from '../../../Servicios/facturas.service';
import { UtilService } from '../../../util.service';
import { Facturas } from '../../../Modelos/Facturas';
import { FacturasCarArt } from '../../../Modelos/FacturasCarArt';
import { FactArticulos } from '../../../Modelos/FactArticulos';
import { ArticuloInter } from '../../../Interfaces/Articulo';
import { CotizacionInter } from '../../../Interfaces/Cotizacion';
import { ClienteFacturaInter } from '../../../Interfaces/ClienteFactura';

@Component({
  selector: 'app-facturas-ver-version',
  templateUrl: './facturas-ver-version.component.html',
  styleUrls: ['./facturas-ver-version.component.scss']
})
export class FacturasVerVersionComponent implements OnInit {

  
  //Arreglos estaticos para pagos
  TipoDePago = [{id:"1",vista:"Contado"},{id:"2",vista:"Credito"}];
  MetodoDePago = [{id:"1",vista:"PUE - pago con solo una existencia"},{id:"2",vista:"PPD - pago parcial o diferido"}];
  //Variables para validar o guardar datos
  tF=0; nomArt = ""; ubicador=0;
  //Arreglos para selects, rellenados con la base de datos
  Areas; Monedas; Ivas; Proveedor; tFactura; FormaDePago; cfdi;  
  stateArticulo = new FormControl();
  FilterArticulo:Observable<ArticuloInter[]>;
  Articulos:ArticuloInter[]=[];

  stateCliente = new FormControl();
  FilteredCliente:Observable<ClienteFacturaInter[]>;
  Cliente:ClienteFacturaInter[]=[];

  stateCotizacion = new FormControl();
  FilteredCotizacion:Observable<CotizacionInter[]>;
  Cotizacion:CotizacionInter[]=[];
  
  constructor(public http:FacturasService,public util:UtilService) { }
  factura = new Facturas(0,0,0,0,0,0,0,'',0,0,0,'',0,0);
  facturaCarArt = new FacturasCarArt(0,0,0);
  facturaLib = new FactArticulos (0,0,0,0,0,0,0,0,0,0,0,0,0,0);
  Columns=['nomCotizacion','areaCotizacion','codSatArticulo','nomArticulo','nomProveedor','cantArticulo','subArticulo','impArticulo','iva','cveMoneda'];
  dataSource:any;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  
  ngOnInit() {
    this.CargarFactura();
    this.FormasPago();
    this.CFDI();
    this.TipoFactura();
  }

  CargarFactura(){
    var datos = this.util.showDatos();
    this.factura = {idCliente:datos.idCliente,idFactura:datos.idFactura,versionFactura:datos.versFactura,tPago:datos.tPago,mPago:datos.mPago,fPago:datos.idFormP,plazo:datos.plazo,folioFis:this.factura.folioFis,CFDI:datos.idCFDI,CFDIRel:this.factura.CFDIRel,anticipo:datos.anticipo,referencia:datos.referencia,tFactura:datos.idTipoF,idUsuario:localStorage.Usuario};
    this.stateCliente.setValue(datos.razSocCliente);
    this.stateCliente.disable();
    this.stateCotizacion.setValue(datos.cotFactura);
    this.stateCotizacion.disable();
    this.facturaCarArt = {idUsuario:this.factura.idUsuario,idCot:this.factura.idFactura,version:this.factura.versionFactura};
    this.CargarArticuloFactura();
  }

  CargarArticuloFactura(){
    this.http.CargarArticulos(this.facturaCarArt).subscribe(result=>{
      console.log(result);
      this.TablaArticulos();
    },error=>{console.log(<any>error);});
  }

  FormasPago(){
    this.http.FormaPago().subscribe(result=>{
      this.FormaDePago = result;
    },error=>{console.log(<any>error);});
  }

  CFDI(){
    this.http.CFDI().subscribe(result=>{
      this.cfdi = result;
    },error=>{console.log(<any>error);});
  }

  TipoFactura(){
    this.http.TipoFactura().subscribe(result=>{
      this.tFactura = result;
    },error=>{console.log(<any>error);});
  }

  TablaArticulos(){
    this.http.TablaArticulos(this.factura.idUsuario).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
    },error=>{console.log(<any>error);});
  }

  CargarServicios(){
    this.Iva();
    this.Moneda();
  }

  Iva(){
    this.http.Ivas().subscribe(result=>{
      this.Ivas = result;
    },error=>{console.log(<any>error);});
  }

  Moneda(){
    this.http.Monedas().subscribe(result=>{
      this.Monedas = result;
    },error=>{console.log(<any>error);});
  }

  ObtProveedor(Art){
    this.nomArt = Art.nomArticulo;
    this.facturaLib.idArticulo = Art.idArticulo;
    this.http.ArticuloPro(Art.idArticulo).subscribe(result=>{
      this.Proveedor = result;
    },error=>{console.log(<any>error);});
  }

  ObtInformacionArt(idProveedor){
    this.http.DatosArticulo(idProveedor,this.facturaLib.idArticulo).subscribe(result=>{
      Object.values(result).forEach(element=>{
        this.facturaLib.precio = element.cRepArticulo;
        this.facturaLib.idMoneda = element.idMoneda;
      });
    },error=>{console.log(<any>error);});
  }

  CargarDatos(row){
    this.CargarServicios();
    if(this.factura.tFactura == 1 || this.factura.tFactura == 3){
      this.ubicador = 1;
      this.facturaLib = {idAreaCot:row.idAreaCotizacion,idArticulo:row.idArticulo,idCot:row.idCotizacion,versionCot:row.versCotizacion,idProveedor:row.idProveedor,idPartida:row.idPartida,idMoneda:row.idMoneda,cantidad:row.cantArticulo,precio:row.cosArticulo,idIva:row.idIva,marge:row.margArticulo,descuento:row.dctoArticulo,idUser:this.factura.idUsuario,CodigoSAT:row.codSatArticulo};
      this.stateArticulo.setValue(row.codSatArticulo);
      this.stateArticulo.disable();
      this.ObtProveedor(row);
      this.ObtInformacionArt(this.facturaLib.idProveedor);
    }else {
      this.ubicador = 1;
      this.stateArticulo.setValue(row.codSatArticulo);
      this.stateArticulo.disable();
      this.facturaLib = {idAreaCot:row.idAreaCotizacion,idArticulo:row.idArticulo,idCot:row.idCotizacion,versionCot:row.versCotizacion,idProveedor:row.idProveedor,idPartida:row.idPartida,idMoneda:row.idMoneda,cantidad:row.cantArticulo,precio:row.cosArticulo,idIva:row.idIva,marge:row.margArticulo,descuento:row.dctoArticulo,idUser:this.factura.idUsuario,CodigoSAT:row.codSatArticulo};
      this.ObtProveedor(row);
    }
  }

  EditarArticulo(){
    this.http.EditarArticulo(this.facturaLib).subscribe(result=>{
      console.log(result);
      this.TablaArticulos();
      this.Cancelar();
    },error=>{console.log(<any>error);});
  }

  Cancelar(){
    this.nomArt = "";
    this.facturaLib = {idAreaCot:0,idArticulo:0,idCot:0,versionCot:0,idProveedor:0,idPartida:0,idMoneda:0,cantidad:0,precio:0,idIva:0,marge:0,descuento:0,idUser:this.factura.idUsuario,CodigoSAT:0};
    this.ubicador = 0;
    this.stateArticulo.reset();
  } 

  CancelarF(){
    this.http.CambioCliente(this.factura).subscribe(result => {
      this.TablaArticulos();
    }, error => { console.log(<any>error); });
  }

  Editar(){
    this.http.EditarFactura(this.factura).subscribe(result=>{
      this.util.FacturaNavegacion(result,"facturas");
    },error=>{console.log(<any>error);});
  }

}

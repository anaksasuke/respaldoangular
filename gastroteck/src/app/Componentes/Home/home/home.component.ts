import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { ServiciosService } from '../../../servicios.service';
import { Pais } from '../../../Modelos/PaisInter';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  stateCtrl = new FormControl();
  filteredStates:Observable<Pais[]>;
  paisesP:Pais[]=[];
  constructor(public http:ServiciosService) { }

    ngOnInit(){
      this.http.Paises().subscribe(result=>{
        this.paisesP = Object.values(result);
        this.filteredStates = this.stateCtrl.valueChanges.pipe(startWith(''),map(paises => paises ? this._filterPais(paises):this.paisesP.slice()));
      });
    }

    _filterPais(value:string):Pais[]{
      const filterValue = value.toLowerCase();
      return this.paisesP.filter(paises => paises.pais.toLowerCase().indexOf(filterValue)===0);
    }

    prueba(valor){
      console.log(valor);
    }
}

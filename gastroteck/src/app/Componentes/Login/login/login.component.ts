import { Component, OnInit } from '@angular/core';
import { UtilService } from '../../../util.service';
import { ConfiguracionesService } from '../../../Servicios/configuraciones.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private http:ConfiguracionesService,private util:UtilService) { }

  ngOnInit() {
    //quita el header
    document.getElementById("sidenav").style.display="none";
  }

  datos(event){
    var username;
    var password;
    event.preventDefault();
    const target = event.target;
    username=target.querySelector('#username').value;
    password=target.querySelector('#password').value;
    this.validacion(username,password);
  }

  validacion(username,password){
    this.http.Login(username,password).subscribe(result=>{
      this.Entrar(result);
    },
    error=>{
      console.log(<any>error);
    });
  }

  Entrar(respuesta){
    var idUser,Username,rol;
    var long = respuesta.length;
    if(long == 0 || long == undefined){
      this.util.ErrorLogeo();
    }
    else{
      respuesta.forEach(element => {
        idUser = element.idUsuario;
        Username = element.usuario;
        rol = element.idRolUsuario;
      });
    this.SessionVar(idUser,Username,rol)
    }


  }

  SessionVar(id,nombre,rol){
    localStorage.Usuario = id;
    localStorage.Rol = rol;
    this.util.getSession(id,nombre);
    this.util.navegacion("/cliente");
    document.getElementById("sidenav").style.display="inline";
  }

}

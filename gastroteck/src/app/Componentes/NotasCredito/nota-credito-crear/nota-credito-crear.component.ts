import { Component, OnInit, ViewChild } from '@angular/core';
import { NotaCreditoService } from '../../../Servicios/notacredito.service';
import { FormControl} from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ClienteFacturaInter } from 'src/app/Interfaces/ClienteFactura';
import { FacturaClienteInter } from 'src/app/Interfaces/FacturaCliente';
import { NotaCredito } from 'src/app/Modelos/NotaCredito';
import { UtilService } from 'src/app/util.service';
import { MatTableDataSource } from '@angular/material';
import { NotaCreditoFactura } from 'src/app/Modelos/NotaCreditoFactura';
import { NotaCreditoArticulos } from 'src/app/Modelos/NotaCreditoArticulos';

@Component({
  selector: 'app-nota-credito-crear',
  templateUrl: './nota-credito-crear.component.html',
  styleUrls: ['./nota-credito-crear.component.scss']
})

export class NotaCreditoCrearComponent implements OnInit {
  //arreglos para los dropdown list
  TipoDePago = [{id:"1",vista:"Contado"}, {id:"2",vista:"Credito"}];
  MetodoDePago = [{id:"1",vista:"PUE - pago con solo una existencia"}, {id:"2",vista:"PPD - pago parcial o diferido"}];
  FormaDePago; CFDI; CFDIRel;
  ValPlazo = 0; Ivas; nomProveedor = ""; Monedas; nomArt="";

  StateCliente = new FormControl();
  FilterCliente:Observable<ClienteFacturaInter[]>;
  Cliente:ClienteFacturaInter[]=[];
    
  facturacliente = new FormControl();
  FilteredFacturacliente:Observable<FacturaClienteInter[]>;
  Facturacliente:FacturaClienteInter[]=[];
    
  Columns=['nomCotizacion','areaCotizacion','codSatArticulo','nomArticulo','nomProveedor','cantArticulo','subArticulo','impArticulo','iva','cveMoneda',"Acciones"];
  dataSource : any; validadorHtml = 0;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static:true}) sort:MatSort;
   
  constructor(public http:NotaCreditoService,public util:UtilService) { }
  notacredito = new NotaCredito(0,0,0,0,0,0,0,"",0,0,0,0,'');
  notacreditoFactura = new NotaCreditoFactura(0,0,0);
  notacreditocart = new NotaCreditoArticulos(0,0,0,0,0,0,0,'',0,0,'',0,'',0,0,0,0,0);
  selNotaCredito;
  usuario; 
   
  ngOnInit() {
    this.notacredito.idUsuario = localStorage.Usuario;
    this.Clientes();
    this.CargarForPago();
    this.CargarCFDI();
    this.CargarCFDIRel();
    this.Iva();
    this.Moneda();
  }

  Clientes(){
    this.http.Clientes().subscribe(result=>{
      this.Cliente = Object.values(result);
      this.FilterCliente = this.StateCliente.valueChanges.pipe(startWith(''),map(clientes => clientes ? this.filterCli(clientes):this.Cliente.slice()));
    },error=>{console.log(<any>error);});
  }

  filterCli(value:string):ClienteFacturaInter[]{
    const filter = value.toLowerCase();
    return this.Cliente.filter(clientes => clientes.razSocCliente.toLowerCase().indexOf(filter)===0);    
  }

  ObtCliente(idCliente){
    if(this.notacredito.idCliente == idCliente && this.notacredito.idCliente != 0){
      this.LimpiadoCliente();
      this.notacredito.idCliente = idCliente;
      this.Facturas();
    }else{
      this.notacredito.idCliente = idCliente;
      this.Facturas();
    }
  }

  LimpiadoCliente(){
    this.http.CambioCliente(this.notacredito).subscribe(result=>{
      this.RenderTable();
    },error=>{console.log(error);});
  }

  Facturas(){
    this.http.Facturas(this.notacredito.idCliente).subscribe(result=>{
      this.Facturacliente = Object.values(result);
      this.FilteredFacturacliente = this.facturacliente.valueChanges.pipe(startWith(''),map(facturaclientes => facturaclientes ? this.filterFac(facturaclientes):this.Facturacliente.slice()));
    },error=>{console.log(<any>error);});
  }

  filterFac(value:string):FacturaClienteInter[]{
    const filter = value.toLowerCase();
    return this.Facturacliente.filter(facturaclientes => facturaclientes.nomFactura.toLowerCase().indexOf(filter)===0);
  }

  CargarForPago(){
    this.http.FormaPago().subscribe(result=>{
      this.FormaDePago = result;
    },error=>{console.log(<any>error);});
  }
    
  CargarCFDI(){
    this.http.CFDI().subscribe(result=>{
      this.CFDI = result;
    },error=>{console.log(<any>error);});
  }

  CargarCFDIRel(){
    this.http.CFDIRelacional().subscribe(result=>{
      this.CFDIRel = result;
    },error=>{console.log(<any>error);});
  }

  ObtFactura(factura){
    if(this.notacreditoFactura.idFactura == factura || this.notacreditoFactura.idFactura != 0){
      this.notacreditoFactura = {idUsuario:this.notacredito.idUsuario,idFactura:factura.idFactura,Version:factura.versFactura};
      this.LimpiadoCliente();
      this.CargarDatos();
    }else{
      this.notacreditoFactura = {idUsuario:this.notacredito.idUsuario,idFactura:factura.idFactura,Version:factura.versFactura};
      this.CargarDatos();
    }
    this.ArticulosPolitica();
  }

  ArticulosPolitica(){
    this.http.RellenarTabla(this.notacreditoFactura).subscribe(result=>{
      this.RenderTable();
    },error=>{console.log(<any>error);});
  }  

  RenderTable(){
    this.http.Tabla(this.notacredito.idUsuario).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },error=>{console.log(<any>error);});
  }

  CargarDatos(){
    this.http.DatosFactura(this.notacreditoFactura.idFactura,this.notacreditoFactura.Version).subscribe(result=>{
      Object.values(result).forEach(element=>{
        this.notacredito = {idNotaCredito: this.notacredito.idNotaCredito, versNotaCredito:this.notacredito.versNotaCredito,idTipoCfdi:element.idTipoCfdi, idRelCfdi:element.idRelCfdi, idFormPago:element.idFormaPago, fFiscNotaCredito:element.fFiscFactura, plaNotaCredito:element.plaFactura, antNotaCredito:element.antFactura, tPagoNotaCredito:element.tPagoFactura, mPagoNotaCredito:element.mPagoFactura, refNotaCredito:element.refFactura, idCliente:this.notacredito.idCliente, idUsuario:this.notacredito.idUsuario };
      });
    },error=>{console.log(<any>error);});
  }

  Iva(){
    this.http.Ivas().subscribe(result=>{
      this.Ivas = result;
    },error=>{console.log(<any>error);});
  }

  Moneda(){
    this.http.Monedas().subscribe(result=>{
      this.Monedas = result;
    },error=>{console.log(<any>error);});
  }
    
  CargarDatosArt(row){
    this.validadorHtml = 1;
    this.notacreditocart = {idUsuario:this.notacredito.idUsuario,idFactura:row.idFactura,versFactura:row.versFactura,idCotizacion:row.idCotizacion,versCotizacion:row.versCotizacion,idPartida:row.idPartida,idAreaCotizacion:row.idAreaCotizacion,areaCotizacion:row.areaCotizacion,idArticulo:row.idArticulo,codSatArticulo:row.codSatArticulo,nomArticulo:row.nomArticulo,idProveedor:row.idProveedor,nomProveedor:row.nomProveedor,cantArticulo:row.cantArticulo,cosArticulo:row.cosArticulo,descuento:row.dctoArticulo,idIva:row.idIva,idMoneda:row.idMoneda};
  }

  CancelarDatos(){//boton cancelar cuando seleccionas un articulo y se cierra el formulario
    this.validadorHtml = 0;
    this.notacreditocart = {idUsuario:this.notacredito.idUsuario,idFactura:0,versFactura:0,idCotizacion:0,versCotizacion:0,idPartida:0,idAreaCotizacion:0,areaCotizacion:"",idArticulo:0,codSatArticulo:0,nomArticulo:"",idProveedor:0,nomProveedor:"",cantArticulo:0,cosArticulo:0,descuento:0,idIva:0,idMoneda:0}
  }

  ActualizarArticulo(){
    this.http.ActualizarArt(this.notacreditocart).subscribe(result=>{
      console.log(result);
      this.RenderTable();
      this.CancelarDatos();
    },error=>{console.log(<any>error);});
  }

  GuardarNotaCredito(){
    this.http.GuardarNotaCred(this.notacredito).subscribe(result=>{
      console.log(result);
      this.util.NavegacionDinamica(result,"notacredito");
    },error=>{console.log(<any>error);});
   }

  EliminarArticulo(row){
    this.notacreditocart = {idUsuario:this.notacredito.idUsuario,idFactura:row.idFactura,versFactura:row.versFactura,idCotizacion:row.idCotizacion,versCotizacion:row.versCotizacion,idPartida:row.idPartida,idAreaCotizacion:row.idAreaCotizacion,areaCotizacion:row.areaCotizacion,idArticulo:row.idArticulo,codSatArticulo:row.codSatArticulo,nomArticulo:row.nomArticulo,idProveedor:row.idProveedor,nomProveedor:row.nomProveedor,cantArticulo:row.cantArticulo,cosArticulo:row.cosArticulo,descuento:row.dctoArticulo,idIva:row.idIva,idMoneda:row.idMoneda};
    this.http.DeleteArticulo(this.notacreditocart).subscribe(result=>{
      this.RenderTable();
    },error=>{console.log(<any>error);});
  }
     
  Regresar(){
    this.Reset();
    this.util.navegacion("/notacredito");
  }

  Reset(){  
    this.ValPlazo = 0;
    this.LimpiadoCliente();
  }
    
}

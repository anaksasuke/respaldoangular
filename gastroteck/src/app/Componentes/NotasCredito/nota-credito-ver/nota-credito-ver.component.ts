import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import{ MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UtilService } from '../../../util.service';
import { NotaCreditoService } from '../../../Servicios/notacredito.service';
import { NotaCreditoEliminar } from '../../../Modelos/NotaCreditoEliminar';
import { CambiarEstatusNotaCredito } from '../../../Modelos/CambiarEstatusNotaCredito';
@Component({
  selector: 'app-nota-credito-ver',
  templateUrl: './nota-credito-ver.component.html',
  styleUrls: ['./nota-credito-ver.component.scss']
})
export class NotaCreditoVerComponent implements OnInit {
  Columns=["nomNotaCredito", "facNotaCredito", "cotNotaCredito", "razSocCliente","fFiscNotaCredito", "estOperacion", "fModNotaCredito", "Acciones"];
  dataSource:any;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static:true}) sort:MatSort;
  constructor(public util:UtilService,public http:NotaCreditoService,public dialog:MatDialog) { }
  NCBorrar = new NotaCreditoEliminar(0,0,0);
  cambiarestatus = new CambiarEstatusNotaCredito(0,0,0,0,0)
   
  ngOnInit() {
    this.RenderTable();
    this.cambiarestatus.idUsuario = localStorage.Usuario;
    this.NCBorrar.idUsuario = localStorage.Usuario;
  }

  Filtro(filtroStr){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  RenderTable(){
    this.http.NotaCredito().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },error=>{console.log(<any>error);});
  }

  EditarVersion(row){
    this.util.getDatos(row);
    this.util.navegacion("notacredito/nota-credito-editar");
  }

  VerVersion(row){
    this.util.getDatos(row);
    this.util.navegacion("notacredito/ver-version"); 
  }

  NuevaVersion(row){
    this.util.getDatos(row);
    this.util.navegacion("notacredito/nueva-version"); 
  }

  EliminarNotaCredito(row){
    this.NCBorrar = {idUsuario:this.NCBorrar.idUsuario,idNotaCredito:row.idNotaCredito, versNotaCredito:row.versNotaCredito};
    this.http.NotaCreditoBorrar(this.NCBorrar).subscribe(result=>{
      this.RenderTable();
      console.log(result);
    },error=>{console.log(<any>error);});
  }

  Descartar(row){
      this.NCBorrar = {idUsuario:this.NCBorrar.idUsuario,idNotaCredito:row.idNotaCredito, versNotaCredito:row.versNotaCredito};
      this.http.NotaCreditoDescartar(this.NCBorrar).subscribe(result=>{
      console.log(result);
      this.RenderTable();
    },error=>{console.log(<any>error);});
  }

  OpenModal():void{
    const dialogRef = this.dialog.open(EstatusNC,{
    width:"400",
    height:"400",
    data:this.cambiarestatus
      });
      dialogRef.afterClosed().subscribe(result=>{
        this.RenderTable();
      });
  }

  CambiarEstatus(row){
    this.cambiarestatus = {idUsuario:this.cambiarestatus.idUsuario,idNotaCredito:row.idNotaCredito, versNotaCredito:row.versNotaCredito,idTCfdiRel:row.idRelCfdi, idEstOperacion:row.idEstOperacion};
    this.OpenModal();
  }

  openDialog(row):void{
    const dialogRef = this.dialog.open(ModalVersiones, {
    width:'900px',
    height: '410px',
    data:  this.cambiarestatus
    });
    dialogRef.afterClosed().subscribe(result=>{
    this.RenderTable();  
    });
  }

}

@Component({
  selector:"EstatusNC",
  templateUrl:"EstatusNC.html"
})

export class EstatusNC implements OnInit{
  estatus;
  constructor(public modal:MatDialogRef<EstatusNC>,public http:NotaCreditoService,@Inject(MAT_DIALOG_DATA) public EstatusNC:CambiarEstatusNotaCredito){}

  ngOnInit(){
    this.Estatus();
  }

  Estatus(){
    this.http.Estatus().subscribe(result=>{
      this.estatus = result;
    },error=>{console.log(<any>error);});
  }

  onNoClick():void{
    this.modal.close();
  }

  CambiarEstatus(){
    this.http.CambiarEstatus(this.EstatusNC).subscribe(result=>{
      this.modal.close();
      console.log(result);
    },error=>{console.log(<any>error);});
  }
}

@Component({
  selector:'NotaCreditoVervers',
  templateUrl:'NotaCreditoververs.html',
  styleUrls: ['NotaCreditoververs.component.scss']
})

export class ModalVersiones implements OnInit{
  constructor(public Modal:MatDialogRef<ModalVersiones>, public util:UtilService, public http:NotaCreditoService, @Inject(MAT_DIALOG_DATA) public NCBorrar:CambiarEstatusNotaCredito){}
  dataSource: any;
  usuario;
  @ViewChild(MatPaginator, {static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  Columns=["nomNotaCredito", "facNotaCredito", "cotNotaCredito", "razSocCliente", "estOperacion", "fModNotaCredito", "Acciones"];
    
  ngOnInit(){
    this.RenderTable();
  }

  Filtro(filtroStr){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }
  
  RenderTable(){
    this.http.Verversion(this.NCBorrar.idNotaCredito).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },error=>{console.log(<any>error);});
  }

  Traspasar2(row,param){
    this.util.getDatos(row);
    switch(param){
      case 1:
        this.util.navegacion("notacredito/ver-version");
        this.Modal.close();
        break;
      case 2:
        this.util.navegacion("notacredito/nueva-version");
        this.Modal.close();
      break;
    }
  }
}




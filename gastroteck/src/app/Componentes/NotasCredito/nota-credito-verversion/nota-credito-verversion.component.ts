import { Component, OnInit, ViewChild } from '@angular/core';
import { NotaCreditoService } from '../../../Servicios/notacredito.service';
import { FormControl} from '@angular/forms';
import { Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ClienteFacturaInter } from 'src/app/Interfaces/ClienteFactura';
import { FacturaClienteInter } from 'src/app/Interfaces/FacturaCliente';
import { NotaCredito } from 'src/app/Modelos/NotaCredito';
import { UtilService } from 'src/app/util.service';
import { MatTableDataSource } from '@angular/material';
import { NotaCreditoArticulos } from 'src/app/Modelos/NotaCreditoArticulos';
import { NotaCreditoEliminar } from '../../../Modelos/NotaCreditoEliminar';

@Component({
  selector: 'app-nota-credito-verversion',
  templateUrl: './nota-credito-verversion.component.html',
  styleUrls: ['./nota-credito-verversion.component.scss']
})
export class NotaCreditoVerversionComponent implements OnInit {
  //arreglos para los dropdown list
  TipoDePago = [{id:"1",vista:"Contado"}, {id:"2",vista:"Credito"}];
  MetodoDePago = [{id:"1",vista:"PUE - pago con solo una existencia"}, {id:"2",vista:"PPD - pago parcial o diferido"}];
  FormaDePago; CFDI; CFDIRel;
  Ivas; Monedas; validadorHtml = 0;

  StateCliente = new FormControl();
  FilterCliente:Observable<ClienteFacturaInter[]>;
  Cliente:ClienteFacturaInter[]=[];
    
  facturacliente = new FormControl();
  FilteredFacturacliente:Observable<FacturaClienteInter[]>;
  Facturacliente:FacturaClienteInter[]=[];
    
  Columns=['nomCotizacion','areaCotizacion','codSatArticulo','nomArticulo','nomProveedor','cantArticulo','subArticulo','impArticulo','iva','cveMoneda'];
  dataSource : any; 
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static:true}) sort:MatSort;
  constructor(public http:NotaCreditoService,public util:UtilService) { }
  notacredito = new NotaCredito(0,0,0,0,0,0,0,"",0,0,0,0,'');
  notacreditocart = new NotaCreditoArticulos(0,0,0,0,0,0,0,'',0,0,'',0,'',0,0,0,0,0);
  cargarArticulos = new NotaCreditoEliminar(0,0,0);

  ngOnInit() {
    this.notacredito.idUsuario = localStorage.Usuario;
    this.CargarInformacion();
    this.CargarForPago();
    this.CargarCFDI();
    this.CargarCFDIRel();
  }

  CargarForPago(){
    this.http.FormaPago().subscribe(result=>{
      this.FormaDePago = result;
    },error=>{console.log(<any>error);});
  }
    
  CargarCFDI(){
    this.http.CFDI().subscribe(result=>{
      this.CFDI = result;
    },error=>{console.log(<any>error);});
  }

  CargarCFDIRel(){
    this.http.CFDIRelacional().subscribe(result=>{
      this.CFDIRel = result;
    },error=>{console.log(<any>error);});
  }

  CargarInformacion(){
    var datos = this.util.showDatos();
    this.notacredito = {idUsuario:this.notacredito.idUsuario,idCliente:datos.idCliente,idNotaCredito:datos.idNotaCredito,versNotaCredito:datos.versNotaCredito,idFormPago:datos.idFormaPago,idTipoCfdi:datos.idTipoCfdi,idRelCfdi:datos.idRelCfdi,fFiscNotaCredito:datos.fFiscNotaCredito,plaNotaCredito:datos.plaNotaCredito,tPagoNotaCredito:datos.tPagoNotaCredito,mPagoNotaCredito:datos.mPagoNotaCredito,refNotaCredito:datos.refNotaCredito,antNotaCredito:datos.antNotaCredito};
    this.cargarArticulos = {idNotaCredito:this.notacredito.idNotaCredito,versNotaCredito:this.notacredito.versNotaCredito,idUsuario:this.notacredito.idUsuario};
    this.StateCliente.setValue(datos.razSocCliente);
    this.StateCliente.disable();
    this.facturacliente.setValue(datos.facNotaCredito);
    this.facturacliente.disable();
    this.CargarArticulo();
  }

  CargarArticulo(){
    this.http.CargarArticulos(this.cargarArticulos).subscribe(result=>{
      console.log(result);
      this.RenderTable();
    },error=>{console.log(<any>error);});
  }
 
  RenderTable(){
    this.http.Tabla(this.notacredito.idUsuario).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
        this.dataSource.data = result;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    },error=>{console.log(<any>error);});
  }

  CargarDatosArt(row){
    this.validadorHtml = 1;
    this.notacreditocart = {idUsuario:this.notacredito.idUsuario,idFactura:row.idFactura,versFactura:row.versFactura,idCotizacion:row.idCotizacion,versCotizacion:row.versCotizacion,idPartida:row.idPartida,idAreaCotizacion:row.idAreaCotizacion,areaCotizacion:row.areaCotizacion,idArticulo:row.idArticulo,codSatArticulo:row.codSatArticulo,nomArticulo:row.nomArticulo,idProveedor:row.idProveedor,nomProveedor:row.nomProveedor,cantArticulo:row.cantArticulo,cosArticulo:row.cosArticulo,descuento:row.dctoArticulo,idIva:row.idIva,idMoneda:row.idMoneda};
    this.Iva();
    this.Moneda();
  }

  Iva(){
    this.http.Ivas().subscribe(result=>{
      this.Ivas = result;
    },error=>{console.log(<any>error);});
  }

  Moneda(){
    this.http.Monedas().subscribe(result=>{
      this.Monedas = result;
    },error=>{console.log(<any>error);});
  }

  CancelarDatos(){//boton cancelar cuando seleccionas un articulo y se cierra el formulario
    this.validadorHtml = 0;
    this.notacreditocart = {idUsuario:this.notacredito.idUsuario,idFactura:0,versFactura:0,idCotizacion:0,versCotizacion:0,idPartida:0,idAreaCotizacion:0,areaCotizacion:"",idArticulo:0,codSatArticulo:0,nomArticulo:"",idProveedor:0,nomProveedor:"",cantArticulo:0,cosArticulo:0,descuento:0,idIva:0,idMoneda:0}
  }

  ActualizarArticulo(){
    this.http.ActualizarArt(this.notacreditocart).subscribe(result=>{
      console.log(result);
      this.RenderTable();
      this.CancelarDatos();
    },error=>{console.log(<any>error);});
  }

  EliminarArticulo(row){
    this.notacreditocart = {idUsuario:this.notacredito.idUsuario,idFactura:row.idFactura,versFactura:row.versFactura,idCotizacion:row.idCotizacion,versCotizacion:row.versCotizacion,idPartida:row.idPartida,idAreaCotizacion:row.idAreaCotizacion,areaCotizacion:row.areaCotizacion,idArticulo:row.idArticulo,codSatArticulo:row.codSatArticulo,nomArticulo:row.nomArticulo,idProveedor:row.idProveedor,nomProveedor:row.nomProveedor,cantArticulo:row.cantArticulo,cosArticulo:row.cosArticulo,descuento:row.dctoArticulo,idIva:row.idIva,idMoneda:row.idMoneda};
    this.http.DeleteArticulo(this.notacreditocart).subscribe(result=>{
      this.RenderTable();
    },error=>{console.log(<any>error);});
  }

  Actualizar(){
    this.http.ActualizarNotaCredito(this.notacredito).subscribe(result=>{
      console.log(result);
      this.util.NavegacionDinamica(result,"notacredito");
    },error=>{console.log(<any>error);});
  }

  Regresar(){
    
  }
  
}

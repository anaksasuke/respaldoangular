import { Component, OnInit,ViewChild } from '@angular/core';
import { OrdenesService } from '../../../Servicios/ordenes.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map,startWith } from 'rxjs/operators';
import { CotizacionInter } from '../../../Interfaces/Cotizacion';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { OrdEliminar } from '../../../Modelos/OrdEliminar';
import { OrdenArticulo } from '../../../Modelos/OrdArticulo';
import { Orden } from '../../../Modelos/Ordenes'
import { UtilService } from '../../../util.service';
import { ProveedorInter } from '../../../Interfaces/Proveedor';
import { ArticuloInter } from '../../../Interfaces/Articulo';
@Component({
  selector: 'app-order-crear',
  templateUrl: './order-crear.component.html',
  styleUrls: ['./order-crear.component.scss']
})
export class OrderCrearComponent implements OnInit {

  stateCtrl = new FormControl();
  FilteredCot:Observable<CotizacionInter[]>;
  Cotizacion:CotizacionInter[]=[];

  stateProveedor = new FormControl();
  FiltereProveedor:Observable<ProveedorInter[]>;
  Proveedor:ProveedorInter[]=[];

  stateCtrlArt = new FormControl();
  FilteredArt:Observable<ArticuloInter[]>;
  Articulo:ArticuloInter[]=[];

  dataSource:any;
  ArticuloFaltante = 0; Ordtipo = 0; NombreArt=""; disabled=0;
  tipoOrd; areas;
  moneda; iva;
  constructor(public http:OrdenesService,public util:UtilService) { }
  Columns=["idPartida","nomCotizacion","areaCotizacion","modArticulo","nomArticulo","marcArticulo","cantArticulo","cosArticulo","dctoArticulo","subArticulo","iva","impArticulo","cveMoneda","Acciones"];
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  ordenEliminar = new OrdEliminar(0,0,0,0,0);
  ordenArticulo = new OrdenArticulo(0,0,0,0,0,0,0,"",'',0,0,0,0,0);
  orden = new Orden(0,0,0,0,0,'','','','');

  ngOnInit(){
    this.orden.idUsuario = localStorage.Usuario;
    this.ordenArticulo.idUsuario = this.orden.idUsuario;
    this.TipoOrden();
    this.Proveedores();
    this.Iva();
    this.Monedas();
    this.Areas();
  }

  Iva(){
    this.http.IVA().subscribe(result=>{
      this.iva = result;
    },error=>{console.log(<any>error);});
  }

  Monedas(){
    this.http.Monedas().subscribe(result=>{
      this.moneda = result;
    },error=>{console.log(<any>error);});
  }

  Areas(){
    this.http.Areas().subscribe(result=>{
      this.areas = result;
    },error=>{console.log(<any>error);});
  }

  TipoOrden(){
    this.http.TipoOrden().subscribe(result=>{
      this.tipoOrd = result;
    },error=>{console.log(<any>error);});
  }

  ObtTipo(param){
    if(this.Ordtipo != 0 && this.Ordtipo != param){
      this.Ordtipo = param;
      this.CambioProveedor();
      this.TablaArticulos();
    }else {
      this.Ordtipo = param;
    }
  }

  Proveedores(){
    this.http.Proveedor().subscribe(result=>{
      this.Proveedor = Object.values(result);
      this.FiltereProveedor = this.stateProveedor.valueChanges.pipe(startWith(''),map(pro => pro ? this.FilterPro(pro): this.Proveedor.slice()));
    },error=>{console.log(<any>error);});
  }

  FilterPro(value:string):ProveedorInter[]{
    const filterValue = value.toLowerCase();
    return this.Proveedor.filter(pro => pro.nomProveedor.toLowerCase().indexOf(filterValue) === 0);
  }

  ObtProveedor(param){
    if(this.orden.idProveedor != 0 && this.orden.idProveedor != param){
      this.CambioProveedor();
      this.TablaArticulos();
      this.orden.idProveedor = param;
      this.stateCtrl.setValue("");
      this.ValidarTipo(param);
    }else {
      this.TablaArticulos();
      this.orden.idProveedor = param;
      this.ValidarTipo(param);
    }
  }

  ValidarTipo(param){
    switch(parseInt(this.orden.idTipoOrden.toString())){
      case 1:
        this.Cotizaciones(param)
        this.Articulos();
      break;
      case 3:
        this.Articulos();
      break;
    }
  }

  CambioProveedor(){
    this.http.CambioProveedor(this.orden.idUsuario).subscribe(result=>{
      console.log(result);
    },error=>{console.log(<any>error);});
  }

  Articulos(){
    this.http.Articulo(this.orden.idProveedor).subscribe(result=>{
      this.Articulo = Object.values(result);
      this.FilteredArt = this.stateCtrlArt.valueChanges.pipe(startWith(''),map(articulo => articulo ? this.filterArt(articulo):this.Articulo.slice()));
    },error=>{console.log(<any>error);});
  }
  
  filterArt(value:string):ArticuloInter[]{
      const filterValue = value.toLowerCase();
      return this.Articulo.filter(articulos => articulos.modArticulo.toLowerCase().indexOf(filterValue)===0);
  }

  ObtArticulo(Articulo){
    this.NombreArt = Articulo.nomArticulo;
    this.ordenArticulo.idArticulo = Articulo.idArticulo;
    if(this.orden.idTipoOrden == 3){
      this.ArticuloProveedorCot();
    }
  }

  //En caso de que el tipo de Orden se por Cotizacion
  Cotizaciones(id){
    this.http.AutoCot(id).subscribe(result=>{
      this.Cotizacion = Object.values(result);
      this.FilteredCot = this.stateCtrl.valueChanges.pipe(startWith(''),map(cot => cot ? this.FilterCot(cot): this.Cotizacion.slice()));
    },error=>{console.log(<any>error);});
  }

  FilterCot(value:string):CotizacionInter[]{
    const filterValue = value.toLowerCase();
    return this.Cotizacion.filter(cot => cot.nomCotizacion.toLowerCase().indexOf(filterValue) === 0);
  }

  ArticulosCot(cotizacion){
    this.http.CargarArticulos(this.orden.idUsuario,this.orden.idProveedor,cotizacion.idCotizacion,cotizacion.versCotizacion).subscribe(result=>{
       console.log(result);
       this.TablaArticulos();
    },error=>{console.log(<any>error);});
  }

  TablaArticulos(){
    this.http.CargarTabla(this.orden.idUsuario).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
    },error=>{console.log(<any>error);});
  }

  CargarArticulo(row){
    this.ArticuloFaltante = row.resArticulo;
    this.NombreArt = row.nomArticulo;
    this.stateCtrlArt.setValue(row.modArticulo);
    this.stateCtrlArt.disable();
    this.ordenArticulo = {idUsuario:this.orden.idUsuario,idCotizacion:row.idCotizacion,idArticulo:row.idArticulo,version:row.versCotizacion,cantida:row.cantArticulo,modelo:row.modArticulo,nombre:row.nomArticulo,precio:row.cosArticulo,descuento:row.dctoArticulo,idMon:row.idMoneda,idIva:row.idIva,idPartida:row.idPartida,idArea:row.idAreaCotizacion,ubicador:1}
    this.ArticuloProveedorCot();
  }

  ArticuloProveedorCot(){
    this.http.DatosArticulo(this.orden.idProveedor,this.ordenArticulo.idArticulo).subscribe(result=>{
      Object.values(result).forEach(element =>{
        this.ordenArticulo.precio = element.cRepArticulo;
        this.ordenArticulo.idMon = element.idMoneda;
      });
    },error=>{console.log(<any>error);});
  }

  ValidarButton(){
    if(this.orden.idTipoOrden == 1 && (parseFloat(this.ordenArticulo.cantida.toString())  > parseFloat(this.ArticuloFaltante.toString())  || this.ordenArticulo.cantida == 0 || (this.ordenArticulo.cantida.toString()) =="")){
      this.disabled = 1;
    }else {
      this.disabled = 0;
    }
  }

  Actualizar(){
    this.http.EditarArt(this.ordenArticulo).subscribe(result=>{
      console.log(result);
      this.TablaArticulos();
      this.Cancelar();
    },error=>{console.log(<any>error);});
  }

  Cancelar(){
    this.ordenArticulo = {idUsuario:this.orden.idUsuario,idCotizacion:0,idArticulo:0,version:0,cantida:0,modelo:"",nombre:"",precio:0,descuento:0,idMon:0,idIva:0,idPartida:0,idArea:0,ubicador:0}
    this.ArticuloFaltante = 0;
    this.NombreArt = "";
    this.stateCtrlArt.setValue("");
    this.stateCtrlArt.enable();
  }

  Guardar(){
    this.http.GuardarArt(this.ordenArticulo).subscribe(result=>{
      console.log(result);
      this.TablaArticulos();
      this.Cancelar();
    },error=>{console.log(<any>error);});
  }

  GuardarOrden(){
    this.http.GuardarOrden(this.orden).subscribe(result=>{
      console.log(result);
      this.util.NavegacionDinamica(result,'orden');
    },error=>{console.log(<any>error);});
  }

  Borrar(row){
    this.http.EliminarArt(row).subscribe(result=>{
      console.log(result);
      this.TablaArticulos();
    },error=>{console.log(<any>error);});
  }
}
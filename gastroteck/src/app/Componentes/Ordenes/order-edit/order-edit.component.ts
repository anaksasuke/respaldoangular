import { Component, OnInit,ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map,startWith } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { UtilService } from '../../../util.service';
import { Orden } from '../../../Modelos/Ordenes';
import { OrdenesService } from '../../../Servicios/ordenes.service';
import { OrdenArticulo } from '../../../Modelos/OrdArticulo';
import { OrdEliminar } from '../../../Modelos/OrdEliminar';
import { OrdMonedaCom } from '../../../Modelos/OrdMonedaCom';
import { ConfiguracionesService } from '../../../Servicios/configuraciones.service';
import { CotizacionInter } from '../../../Interfaces/Cotizacion';
import { ProveedorInter } from '../../../Interfaces/Proveedor';
import { ArticuloInter } from '../../../Interfaces/Articulo';

@Component({
  selector: 'app-order-edit',
  templateUrl: './order-edit.component.html',
  styleUrls: ['./order-edit.component.scss']
})
export class OrderEditComponent implements OnInit {
  stateCtrl = new FormControl();
  FilteredCot:Observable<CotizacionInter[]>;
  Cotizacion:CotizacionInter[]=[];

  stateProveedor = new FormControl();
  FiltereProveedor:Observable<ProveedorInter[]>;
  Proveedor:ProveedorInter[]=[];

  stateCtrlArt = new FormControl();
  FilteredArt:Observable<ArticuloInter[]>;
  Articulo:ArticuloInter[]=[];
  orden = new Orden(0,0,0,0,0,'','','','');  

  //Variables para selects 
  proveedor; iva; moneda; monedaDrop; monedaDrop2; tipoOrd; disabled=0; ArticuloFaltante=0; NombreArt=""; areas;
  dataSource:any;
  constructor(public util:UtilService,public http:OrdenesService) { }
  Columns=["nomCotizacion","modArticulo","marcArticulo","cantArticulo","cosArticulo","subArticulo","iva","impArticulo","cveMoneda","Acciones"];
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  ordenArticulo = new OrdenArticulo(0,0,0,0,0,0,0,'','',0,0,0,0,0);
  ordenEliminar = new OrdEliminar(0,0,0,0,0);
  ordMoneda = new OrdMonedaCom(0,0,0,0);
  ngOnInit() {
    this.orden.idUsuario = localStorage.Usuario;
    this.CargarOrden();
    this.TipoOrden();
    this.Iva();
    this.Monedas();
    this.Areas();
  }

  CargarOrden(){
    var datos = this.util.showDatos();
    this.orden = {idProveedor:datos.idProveedor,idTipoOrden:datos.idTipOperacion,idUsuario:this.orden.idUsuario,ord:datos.idOrden,version:datos.versOrden,ref:datos.refOrden,encabezado:datos.encOrden,pie:datos.pieOrden,observaciones:datos.obsOrden};
    this.stateProveedor.setValue(datos.nomProveedor);
    this.stateProveedor.disable();
    this.stateCtrl.setValue(datos.ordCotizacion);
    this.stateCtrl.disable();
    this.CargarArticulosOrden();
  }

  Iva(){
    this.http.IVA().subscribe(result=>{
      this.iva = result;
    },error=>{console.log(<any>error);});
  }

  Monedas(){
    this.http.Monedas().subscribe(result=>{
      this.moneda = result;
    },error=>{console.log(<any>error);});
    }

  Areas(){
    this.http.Areas().subscribe(result=>{
      this.areas = result;
    },error=>{console.log(<any>error);});
  }

  CargarArticulosOrden(){
    this.http.CargarArticulosOrden(this.orden).subscribe(resul=>{
      console.log(resul);
      this.TablaArticulos();
    },error=>{console.log(<any>error);});
  }

  TipoOrden(){
    this.http.TipoOrden().subscribe(result=>{
      this.tipoOrd = result;
    },error=>{console.log(<any>error);});
  }

  CargarArticulo(row){
    this.ArticuloFaltante = row.resArticulo;
    this.NombreArt = row.nomArticulo;
    this.stateCtrlArt.setValue(row.modArticulo);
    this.stateCtrlArt.disable();
    this.ordenArticulo = {idUsuario:this.orden.idUsuario,idCotizacion:row.idCotizacion,idArticulo:row.idArticulo,version:row.versCotizacion,cantida:row.cantArticulo,modelo:row.modArticulo,nombre:row.nomArticulo,precio:row.cosArticulo,descuento:row.dctoArticulo,idMon:row.idMoneda,idIva:row.idIva,idPartida:row.idPartida,idArea:row.idAreaCotizacion,ubicador:1}
    this.ArticuloProveedorCot();
  }

  ArticuloProveedorCot(){
    this.http.DatosArticulo(this.orden.idProveedor,this.ordenArticulo.idArticulo).subscribe(result=>{
      Object.values(result).forEach(element =>{
        this.ordenArticulo.precio = element.cRepArticulo;
        this.ordenArticulo.idMon = element.idMoneda;
      });
    },error=>{console.log(<any>error);});
  }

  ValidarButton(){
    if(parseFloat(this.ordenArticulo.cantida.toString())  > parseFloat(this.ArticuloFaltante.toString())  || this.ordenArticulo.cantida == 0 || (this.ordenArticulo.cantida.toString()) ==""){
      this.disabled = 1;
    }else {
      this.disabled = 0;
    }
  }

  Actualizar(){
    this.http.EditarArt(this.ordenArticulo).subscribe(result=>{
      console.log(result);
      this.TablaArticulos();
      this.Cancelar();
    },error=>{console.log(<any>error);});
  }

  Cancelar(){
    this.ordenArticulo = {idUsuario:this.orden.idUsuario,idCotizacion:0,idArticulo:0,version:0,cantida:0,modelo:"",nombre:"",precio:0,descuento:0,idMon:0,idIva:0,idPartida:0,idArea:0,ubicador:0}
    this.ArticuloFaltante = 0;
    this.NombreArt = "";
    this.stateCtrlArt.setValue("");
    this.stateCtrlArt.enable();
  }

  TablaArticulos(){
    this.http.CargarTabla(this.orden.idUsuario).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
    },error=>{console.log(<any>error);});
  }

  Editar(){
    this.http.ActualizarOrden(this.orden).subscribe(result=>{
      this.util.NavegacionDinamica(result,"orden");
      console.log(result);
    },error=>{console.log(<any>error);});
  }

}

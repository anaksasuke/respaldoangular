import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { OrdenesService } from '../../../Servicios/ordenes.service';
import { OrdenEstatus } from '../../../Modelos/OrdenEstatus';
import { UtilService } from '../../../util.service';
import { Orden } from '../../../Modelos/Ordenes';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-order-show',
  templateUrl: './order-show.component.html',
  styleUrls: ['./order-show.component.scss']
})
export class OrderShowComponent implements OnInit {
  Columns=["acciones","nomOrden","ordCotizacion","estOperacion","nomProveedor","totOrden","tCamOrden","refOrden","fModOrden","usuario"];
  dataSource:any;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  ordenEstatu = new OrdenEstatus(0,0,0,0,0);
  orden = new Orden(0,0,0,0,0,'','','','');
  constructor(public http:OrdenesService,public util:UtilService, public dialog:MatDialog) { }

  ngOnInit() {
    this.RenderTabla();
    this.orden.idUsuario= localStorage.Usuario;
    this.ordenEstatu.idUsuario = localStorage.Usuario;
  }

  RenderTabla(){
    this.http.Ordenes().subscribe(result => {
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },error=>{
      console.log(<any>error);
    });
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  Estatus(row){
    this.ordenEstatu = {idUsuario:this.ordenEstatu.idUsuario,idtipo:row.idTipOperacion,version:row.versOrden,idOrden:row.idOrden,idEstatus:row.idEstOperacion};
    const dialogRef = this.dialog.open(ordenPop,{
      width:'auto',
      height:"auto",
      data:this.ordenEstatu
    });

    dialogRef.afterClosed().subscribe(result => {
      this.RenderTabla();
    });
  }

  EditarVersion(row){
    this.util.getDatos(row);
    this.util.navegacion("/orden/editar");
  }

  NuevaVersion(row){
    this.util.getDatos(row);
    this.util.navegacion("/orden/editar-version");
  }

  Borrar(row){
    this.orden.ord = row.idOrden;
    this.orden.version = row.versOrden;
    this.http.Borrar(this.orden).subscribe(result => {
      console.log(result);
      this.RenderTabla();
    }, error => { console.log(<any>error); });
  }

  Descarte(row){
    this.orden.ord = row.idOrden;
    this.orden.version = row.versOrden;
    this.http.Descartar(this.orden).subscribe(result => {
      console.log(result);
      this.RenderTabla();
    }, error => { console.log(<any>error); });
  }
  
  openDialog(row):void{
    const dialogRef = this.dialog.open(ModalOrderVersion, {
    width:'900px',
    height: '410px ',
    data: this.ordenEstatu
   });
   dialogRef.afterClosed().subscribe(result => {
    this.RenderTabla();
   });
  }

}

@Component({
  selector: 'app-order-show',
  templateUrl: './orden-ver-version.html',
  styleUrls: ['./order-show.component.scss']
})
export class ModalOrderVersion implements OnInit{
  constructor(public Modal:MatDialogRef<ModalOrderVersion>, public util:UtilService,
    public http:OrdenesService, @Inject(MAT_DIALOG_DATA) public ordenEstatu: OrdenEstatus){}

    dataSource:any;
    usuario;
    @ViewChild(MatPaginator, {static:true}) paginator:MatPaginator;
    @ViewChild(MatSort,{static:true})sort:MatSort;
    Columns=["acciones","nomOrden","ordCotizacion","nomProveedor","totOrden","fModOrden"];
  
    ngOnInit(){
      this.RenderTabla();
    }

    RenderTabla(){
      this.http.OrdVerVersion(this.ordenEstatu.idOrden).subscribe(result => {
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = result;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }, error => { console.log(<any>error); });
    }
  
    Filtro(filtroStr:string){
      filtroStr = filtroStr.trim().toLowerCase();
      this.dataSource.filter = filtroStr;
      if(this.dataSource.paginator)
      {
        this.dataSource.paginator.firstPage();
      }
    }

    Traspasar2(row,param){
      this.util.getDatos(row);
      switch(param){
        case 1:
          this.util.navegacion("orden/ver-version");
           this.Modal.close();
        break;
        case 2:
          this.util.navegacion("/orden/editar-version");
          this.Modal.close();
        break;
       }
    }
  }

  @Component({
    selector:"ordenPop",
    templateUrl:"ordenPop.html",
  })

  export class ordenPop implements OnInit {
    constructor(public Modal:MatDialogRef<ModalOrderVersion>,public http:OrdenesService,public util:UtilService,@Inject(MAT_DIALOG_DATA) public ordenEstatu: OrdenEstatus){}
    estatus;
    ngOnInit(){
      this.Estatus();
    }

    Estatus(){
      this.http.Estatus().subscribe(result => {
        this.estatus = result;
      }, error => { console.log(<any>error); });
    }
  
    CambioEstatus(){
      this.http.GuardarEstatus(this.ordenEstatu).subscribe(result => {
        this.Modal.close();
        console.log(result);
      }, error => { console.log(<any>error); });
    }

    Cancelar(){
      this.Modal.close();
    }

  }
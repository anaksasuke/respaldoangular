import { Component, OnInit, ViewChild } from '@angular/core';
import {UtilService } from '../../../util.service';
import { Orden } from '../../../Modelos/Ordenes';
import { OrdenesService } from '../../../Servicios/ordenes.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { OrdenArticulo } from '../../../Modelos/OrdArticulo';
import { OrdEliminar } from '../../../Modelos/OrdEliminar';
import { OrdMonedaCom } from '../../../Modelos/OrdMonedaCom';
import { ConfiguracionesService } from '../../../Servicios/configuraciones.service';

@Component({
  selector: 'app-over-version',
  templateUrl: './over-version.component.html',
  styleUrls: ['./over-version.component.scss']
})
export class OverVersionComponent implements OnInit {

  orden = new Orden(0,0,0,0,0,'','','','');  
  ordenEditar = new OrdenArticulo(0,0,0,0,0,0,0,'','',0,0,0,0,0);
  ordenEliminar = new OrdEliminar(0,0,0,0,0);
  ordMoneda = new OrdMonedaCom(0,0,0,0);
  //Variables para selects 
  proveedor; iva; moneda; monedaDrop; monedaDrop2;
  //Variable para esconder el formulario 
  validador=0;
  Columns=["nomCotizacion","modArticulo","marcArticulo","cantArticulo","cosArticulo","subArticulo","iva","impArticulo","cveMoneda","Acciones"];
  dataSource:any;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  constructor(public util:UtilService,public http:OrdenesService,public http2:ConfiguracionesService) { }

  ngOnInit() {
    this.CargarOrden();
    this.CargarProveedor();
  }

  CargarOrden(){
    var datos = this.util.showDatos();
   // this.orden = {idProveedor:datos.idProveedor,idUsuario:0,ord:datos.idOrden,version:datos.versOrden,ref:datos.refOrden};
    this.CargarArticulos();
  }

  CargarArticulos(){
    this.http.CargarArticulosOrden(this.orden).subscribe(result=>{
      console.log(result);
      this.RenderTabla();
    },error=>{console.log(<any>error);});
  }

  CargarProveedor(){
    this.http.Proveedor().subscribe(result=>{
      this.proveedor = result;
    },error=>{console.log(<any>error);});
  }

  RenderTabla(){
    this.http.CargarTabla(this.orden.idUsuario).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
    },error=>{console.log(<any>error);});
  }

  Ivas(){
    this.http.IVA().subscribe(result=>{
      this.iva = result;
    },error=>{console.log(<any>error);});
  }

  Monedas(){
    this.http.Monedas().subscribe(result=>{
      this.moneda = result;
    },error=>{console.log(<any>error);});
  }

  Cargar(row){
    this.Monedas();
    this.Ivas();
    //this.ordenEditar = {idArticulo:row.idArticulo,idCotizacion:row.idCotizacion,idIva:row.idIva,idMon:row.idMoneda,idUsuario:this.orden.idUsuario,version:row.versCotizacion,cantida:row.cantArticulo,modelo:row.modArticulo,nombre:row.nomArticulo,precio:row.cosArticulo}
    this.validador = 1;
  }

  Cancelar(){
   // this.ordenEditar = {idArticulo:0,idCotizacion:0,idIva:0,idMon:0,idUsuario:this.orden.idUsuario,version:0,cantida:0,modelo:"",nombre:"",precio:0}
    this.ordMoneda = {idUsuario:this.ordMoneda.idUsuario,idMonedaOri:0,idMonedaDes:0,ValorMon:0};
    this.validador = 0;
  }

  Editar(){
    this.http.EditarArt(this.ordenEditar).subscribe(result=>{
      console.log(result);
      this.RenderTabla();
      this.Cancelar();
    },error=>{console.log(<any>error);});
  }

  Regresar(){
    this.util.navegacion("/orden");
  }

  Actualizar(){
    this.http.ActualizarOrden(this.orden).subscribe(result=>{
      this.util.navegacion("/orden")
      console.log(result);
    },error=>{console.log(<any>error);});
  }



}

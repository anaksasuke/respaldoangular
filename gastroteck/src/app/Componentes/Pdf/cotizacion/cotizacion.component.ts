import { Component, OnInit } from '@angular/core';
import { CotizacionesService } from '../../../Servicios/cotizaciones.service';

@Component({
  selector: 'app-cotizacion',
  templateUrl: './cotizacion.component.html',
  styleUrls: ['./cotizacion.component.css']
})
export class CotizacionComponent implements OnInit {

  constructor(public http:CotizacionesService) { }
  datoCliente; total; marca;

  ngOnInit() {
    document.getElementById("sidenav").style.display="none";
    this.visualizar();
    this.articulos();
    this.grantotal();
    this.marcas();
    /*this.datoCliente = true;
    this.total = true;
    this.marca = true;*/
  }

  visualizar(){
    this.http.PdfVista(5,2).subscribe(result=>{
      Object.values(result).forEach(element=>{
        console.log(element);
        this.datoCliente = element;
      });
    },error=>{console.log(<any>error);});
  }

  articulos(){
    this.http.TablaArtActulizado(5,2,3).subscribe(result=>{
    },error=>{console.log(<any>error);});
  }
  
  grantotal(){
    this.http.PdfTotal(5,2).subscribe(result=>{
      Object.values(result).forEach(element=>{
        console.log(element);
        this.total = element;
      });
    },error=>{console.log(<any>error);});
  }

  marcas(){
    this.http.PdfMarcas(3).subscribe(result=>{
      Object.values(result).forEach(element=>{
        console.log(element);
        this.marca = element;
      });
    },error=>{console.log(<any>error);});
  }
    




 
  public downloadPDF() {
    return xepOnline.Formatter.Format('content', {render:'download'});
  }
}

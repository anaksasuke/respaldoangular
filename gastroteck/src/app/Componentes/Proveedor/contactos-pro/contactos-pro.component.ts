import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ContactosProveedor } from '../../../Modelos/ContactosProveedor';
import { UtilService } from '../../../util.service';
import { ProveedorService } from '../../../Servicios/proveedor.service';


@Component({
  selector: 'app-contactos-pro',
  templateUrl: './contactos-pro.component.html',
  styleUrls: ['./contactos-pro.component.scss']
})

export class ContactosProComponent implements OnInit {
  
  constructor(public http:ProveedorService,public util:UtilService,public dialog:MatDialog) { }
  contactosP = new ContactosProveedor(0,0,0,'','','','',0,'',0,0);
  Columns =['acciones','tipoContacto', 'preContacto','nomComContacto','cargoContacto','telCelContacto','emailContacto', 'fAltaContacto','fModContacto'];
  dataSource:any;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    var datos = this.util.showDatos();
    console.log(datos);
    this.contactosP.idPro = datos;
    this.contactosP.idUser = localStorage.Usuario;
    this.RenderTable();
  }

  RenderTable(){
    this.http.ProveedorContactos(this.contactosP.idPro).subscribe(result =>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },error=>{console.log(<any>error);});
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  OpendDialog(){
    const dialogRef = this.dialog.open(contactoPop,{
      height:"auto",
      width:"auto",
      data:this.contactosP
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    });
  }

  ObtDatos(row){
    this.http.obtContProveedor(row.idProveedor,row.idContacto).subscribe(result=>{
      console.log(result);
      this.Cargar(result);
    },error=>{console.log(<any>error);});
  }  

  Cargar(datos){
    datos.forEach(element => {
      this.contactosP = {idCon:element.idContacto,idPro:element.idProveedor,nom:element.nomContacto,pat:element.patContacto,mat:element.matContacto,cel:element.telCelContacto,car:element.cargoContacto,email:element.emailContacto,idT:element.idTipoContacto,def:element.defContacto,idUser:this.contactosP.idUser}
      this.OpendDialog();
    });
  }
  
  Eliminar(row){
    this.contactosP.idPro = row.idProveedor;
    this.contactosP.idCon = row.idContacto;
    this.contactosP.idUser = this.contactosP.idUser;
    this.http.EliminarContProveedor(this.contactosP).subscribe(result => {
      this.RenderTable();
      console.log(result);
    },error=>{console.log(<any>error);});
  }

  Limpiar(){
    this.contactosP = {idCon:0,idPro:this.contactosP.idPro,nom:"",pat:"",mat:"",cel:0,car:"",email:"",idT:0,def:0,idUser:this.contactosP.idUser}
  }

  Regresar(){
    this.util.navegacion("/proveedores");
  }
}

@Component({
  selector:"contactoPop",
  templateUrl:"contactoPop.html",
  styleUrls: ['./contactos-pro.component.scss']
})

export class contactoPop implements OnInit {
  constructor(public modal:MatDialogRef<contactoPop>,public http:ProveedorService,@Inject(MAT_DIALOG_DATA) public contacto:ContactosProveedor){}
  TipoCon; title;
  Default=[{valor:"1",vista:"Si"},{valor:"0",vista:"No"}];

  ngOnInit(){
    this.CargarTipoCont();
    if(this.contacto.idCon == 0)
      this.title = "Nuevo Contacto";
    else
      this.title = "Editar Contacto";
  }

  CargarTipoCont(){
    this.http.TipoContactosPro().subscribe(result=>{
      this.TipoCon = result;
      console.log(this.TipoCon);
    },error=>{console.log(<any>error);});
  }

  Guardar(){
    this.http.AgregarContProveedor(this.contacto).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },error=>{console.log(<any>error);});
  }

  Editar(){
    this.http.EditarContProveedor(this.contacto).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },error=>{console.log(<any>error);});
  }

  Cancelar(){
    this.modal.close();
  }

}
import { Component, OnInit } from '@angular/core';
import { Proveedor } from '../../../Modelos/Proveedor';
import { UtilService } from '../../../util.service';
import { ProveedorService } from '../../../Servicios/proveedor.service';
@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html',
  styleUrls: ['./proveedor.component.scss']
})

export class ProveedorComponent implements OnInit {

  proveedor = new Proveedor (0,0,0,0,0,0,0,"","","","",0,"",0); //intProveedor:FormGroup;  
  title = "Nuevo Proveedor";
  pais; estado; ciudad; paist; tipo;
  
  constructor(private http:ProveedorService, public util:UtilService) { }

  ngOnInit() {
    this.proveedor.idUsu = localStorage.Usuario;
    if(this.util.showDatos() != null){      
      this.getProveedor(this.util.showDatos());
    }    
    this.Pais();
    this.TipoP();
  }

  Nuevo(){
    this.http.ProveedorCrear(this.proveedor).subscribe(result=>{      
      this.Reset();
      this.util.NavegacionDinamica(result,'proveedores');
    },
    error=>{console.log(<any>error);});
  }

  getProveedor(param){
    param.forEach(prov =>{
      this.proveedor = {
        idProv:prov.idProveedor,
        idNprov:prov.idProveedor,
        idUsu:this.proveedor.idUsu,
        pais:prov.idPais,
        estado:prov.idEstado,
        ciudad:prov.idCiudad,
        tipo:prov.idTipoPersona,
        rfc:prov.rfcProveedor,
        nombre:prov.nomProveedor,
        razon:prov.razSocProveedor,
        calle:prov.calleProveedor,
        numero:prov.numProveedor,
        colonia:prov.colProveedor,
        cp:prov.cpProveedor
      }
    });
    this.Estado(this.proveedor.pais);
    this.Ciudad(this.proveedor.estado);
    this.title = "Editar Proveedor";
  }

  Editar(){
    this.proveedor.idUsu = localStorage.Usuario;
    this.http.ProveedorEditar(this.proveedor).subscribe(result=>{
    this.Reset();
    this.util.NavegacionDinamica(result,'proveedores')     
    },
    error=>{console.log(<any>error);});
  }

  Reset(){
    this.proveedor = { idProv:0,idNprov:0,idUsu:this.proveedor.idUsu,pais:0,estado:0,ciudad:0, tipo:0, rfc:"",nombre:"",razon:"",calle:"",numero:0,colonia:"",cp:0 };
    this.title = "Nuevo Proveedor";
  }

  Pais(){
    this.http.Paises().subscribe(result=>{
      this.pais = result;
    },
    error=>{console.log(<any>error);});
  }

  TipoP(){
    this.http.TipoPersonas().subscribe(result=>{
      this.tipo = result;
    }, 
    error =>{console.log(<any>error);
    });
  }

  Estado(idPais){
    this.paist = idPais;
    this.http.EstadoFiltro(idPais).subscribe(result=>{
      this.estado = result;
    },
    error=>{console.log(<any>error);});
  }

  Ciudad(idEstado){
    this.http.CiudadFiltro(idEstado, this.paist).subscribe(result=>{
      this.ciudad = result;
    },
    error=>{console.log(<any>error);});
  }
}
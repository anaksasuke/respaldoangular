import { Component, OnInit, ViewChild,Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Proveedora } from '../../../Modelos/ProveedorA';
import { UtilService } from '../../../util.service';
import { ProveedorService } from '../../../Servicios/proveedor.service';
@Component({
  selector: 'app-proveedora',
  templateUrl: './proveedora.component.html',
  styleUrls: ['./proveedora.component.scss']
})
export class ProveedoraComponent implements OnInit {

  constructor(public http:ProveedorService,public util:UtilService,public dialog:MatDialog) { }
  proveedora = new Proveedora(0,0,0,0,0);
  Columns = ['acciones','nomBanco','rfcBanco','ctaCheques','clabe','fModCuenta'];
  dataSource:any;
  title = "Nueva cuenta";
  selectBanco;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  ngOnInit() {
    var prueba  = this.util.showDatos();
    this.proveedora.idProveedor= parseInt(prueba);
    this.proveedora.idUser = localStorage.Usuario;
    this.RenderTable();
  }
  
  RenderTable(){
    this.http.Proveedora(this.proveedora.idProveedor).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{ console.log(<any>error); });
  }
  
  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator){
      this.dataSource.paginator.firstPage();
    }
  }

  Cargar(row){
    if(row.clabe == ""){
      this.proveedora ={idProveedor:row.idProveedor,idBanco:row.idBanco,idUser:this.proveedora.idUser,Cta:row.ctaCheques,ctaResp:row.ctaCheques}
    } else {
      this.proveedora ={idProveedor:row.idProveedor,idBanco:row.idBanco,idUser:this.proveedora.idUser,Cta:row.clabe,ctaResp:row.clabe}
    }
    this.OpenDialog();
  }

  OpenDialog(){
    const dialogRef = this.dialog.open(proveedoraPop,{
      height:"auto",
      width:"auto",
      data:this.proveedora
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    });
  }

  Reset(){
    this.proveedora ={idProveedor:0,idBanco:0,idUser:this.proveedora.idUser,Cta:0,ctaResp:0}
  }

  Eliminar(row){
    this.Cargar(row);
    this.http.ProveedoraEliminar(this.proveedora).subscribe(result=>{
      this.RenderTable();
    },
    error=>{ console.log(<any>error); });
  }
  
  Regresar(){
    this.util.navegacion("/proveedores");
  }
  
}

@Component({
  selector:"proveedoraPop",
  templateUrl:"proveedora-pop.html",
  styleUrls: ['./proveedora.component.scss']
})
export class proveedoraPop implements OnInit{

  title; validar; selectBanco;

  constructor(public modal:MatDialogRef<proveedoraPop>,public http:ProveedorService,@Inject(MAT_DIALOG_DATA) public proveedora:Proveedora){}

  ngOnInit(){
    this.Title();    
    this.SelectBanco();
  }

  SelectBanco(){
    this.http.Banco().subscribe(result=>{
      this.selectBanco = result;
    },
    error=>{ console.log(<any>error); })
  }
  
  Title(){
    if(this.proveedora.idBanco == 0 && this.proveedora.Cta == 0){
      this.title = "Nueva cuenta bancaria";
      this.validar = 0;
    } else{
      this.title = "Editar cuenta bancaria";
      this.validar = 1;
    }
  }  

  Validar(param){
    var longitud = this.proveedora.Cta.toString().length;
    if((longitud >=9 && longitud <=11)||(longitud >=16 && longitud < 19)){
      if(param == 1){
        this.Nuevo();
      }
      else {
        this.Editar();
      }
    }
    else{
      console.group("error en la clave");
    }
  }

  Nuevo(){
    this.http.ProveedoraCrear(this.proveedora).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },
    error=>{ console.log(<any>error); });
  }

  Editar(){
    this.http.ProveedoraEditar(this.proveedora).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },
    error=>{ console.log(<any>error); });
  }

  Cancelar(){
    this.modal.close();
  }

}
import { Component, OnInit, ViewChild } from '@angular/core';
import { UtilService } from '../../../util.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Proveedor } from '../../../Modelos/Proveedor';
import { ProveedorService } from '../../../Servicios/proveedor.service';
@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.component.html',
  styleUrls: ['./proveedores.component.scss']
})

export class ProveedoresComponent implements OnInit {
  proveedor = new Proveedor (0,0,0,0,0,0,0,"","","","",0,"",0);
  Columns = ['acciones', 'nomProveedor', 'rfcProveedor', 'razSocProveedor', 'dirProveedor', 'cpProveedor', 'ubicacion', 'fAltaProveedor', 'fModProveedor', 'usuario'];
  dataSource: any;
  @ViewChild(MatPaginator, {static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public http:ProveedorService, public util:UtilService) { }

  ngOnInit() {
    this.proveedor.idUsu = localStorage.Usuario;
    this.renderTable();
  }

  renderTable(){
    this.http.Proveedores().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },error=>{console.log(<any>error);});
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator){
      this.dataSource.paginator.firstPage();
    }
  }

  obtenerProv(row){
    this.http.ProveedorDatos(row.idProveedor).subscribe(result=>{
      this.util.getDatos(result);
      this.util.navegacion('proveedores/proveedor');
    },error=>{console.error(<any>error);});
  }

  eliminarProv(row){
    this.proveedor.idProv = row.idProveedor;
    this.http.ProveedorEliminar(this.proveedor).subscribe(result=>{
      this.ngOnInit();  
      this.renderTable();
    },error=>{console.error(<any>error);});
  }

  Contactos(row){
    this.util.getDatos(row.idProveedor);
    this.util.navegacion("/proveedores/contactos");
  }

  CuentaBancaria(row){
    this.util.getDatos(row.idProveedor);
    this.util.navegacion("/proveedores/proveedora");
  }
  
  New(){
    this.util.getDatos(null);    
  } 

  Ubicacion(row){
    return row.ciudad + ', ' + row.estado + ', ' + row.pais;
  }
}
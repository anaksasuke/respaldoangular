import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ProyectosService } from '../../../Servicios/proyectos.service';
import { ClienteProyecto } from '../../../Modelos/ClienteProyecto';
import { UtilService } from '../../../util.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ProyectoInter } from 'src/app/Interfaces/Proyecto';
import { ClienteInter } from 'src/app/Interfaces/Cliente';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {
  public clienteProyectos = new ClienteProyecto(0,0,0,0,0,0,'','');
  Columns = ['acciones', 'nomProyecto', 'compCliente', 'estProyecto', 'progProyecto', 'fModProyecto', 'usuario'];
  dataSource: any;
  @ViewChild(MatPaginator, {static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static:true}) sort:MatSort;
  
  constructor(public http:ProyectosService, public util:UtilService, public dialog:MatDialog) { }

  ngOnInit() {
    this.RenderTable();
    this.clienteProyectos.idUsuario = localStorage.Usuario;
  }

  RenderTable(){
    this.http.ClienteProyectos().subscribe(result=>{
      this.dataSource = new MatTableDataSource;
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{ console.log(<any>error); });
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
      this.dataSource.paginator.firstPage();
  }

  Cargar(row){
    this.clienteProyectos.idCliente = row.idCliente;
    this.clienteProyectos.idProyecto = row.idProyecto;
    this.clienteProyectos.idPrioProyecto = 1;
    this.clienteProyectos.idEstProyecto = row.idEstProyecto;
    this.clienteProyectos.idProgProyecto = row.idProgProyecto;
    this.clienteProyectos.compCliente = row.compCliente;
    this.clienteProyectos.nomProyecto = row.nomProyecto;
    this.openDialog();
  }

  openDialog():void{
    const dialogRef = this.dialog.open(ModalClientes,{
      width:'750px',
      height:'350px',
      data:this.clienteProyectos
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    });
  }

  Reset(){
    this.clienteProyectos = { idCliente:0, idProyecto:0, idUsuario:this.clienteProyectos.idUsuario, idPrioProyecto:0, idEstProyecto:0, idProgProyecto:0, compCliente:'', nomProyecto:''};
    
  }

  Eliminar(row){
    this.clienteProyectos.idCliente = row.idCliente;
    this.clienteProyectos.idProyecto = row.idProyecto;
    this.clienteProyectos.idPrioProyecto = 0;
    this.clienteProyectos.idEstProyecto = row.idEstProyecto;
    this.clienteProyectos.idProgProyecto = row.idProgProyecto;
    this.http.ClienteProyectoEliminar(this.clienteProyectos).subscribe(result=>{
      this.ngOnInit();
    },
    error=>{ console.log(<any>error); });
  }

}

@Component({
  selector:'clientesModelo',
  templateUrl:'./clientesModelo.html',
})

export class ModalClientes implements OnInit{
  prioridad; estatus; progreso; cliente; proyecto; title; usuario;
  stateCtrlCli = new FormControl();
  FilteredCli: Observable<ClienteInter[]>;
  ClienteI: ClienteInter[]=[];
  stateCtrlPro = new FormControl();
  FilteredPro: Observable<ProyectoInter[]>;
  ProyectoI: ProyectoInter[]=[];

  constructor(public Modal:MatDialogRef<ModalClientes>,public http:ProyectosService,
  @Inject(MAT_DIALOG_DATA) public clienteProyectos:ClienteProyecto){}

  ngOnInit(){    
    this.Estatus();
    this.Progreso();
    this.ValidadorEvento();   
  }

  filterCli(value:string):ClienteInter[]{
    const filterValue = value.toLowerCase();
    return this.ClienteI.filter(clientes => clientes.compCliente.toLowerCase().indexOf(filterValue)===0);
  }

  filterPro(value:string):ProyectoInter[]{
    const filterValue = value.toLowerCase();
    return this.ProyectoI.filter(proyectos => proyectos.nomProyecto.toLowerCase().indexOf(filterValue)===0);
  }

  ValidadorEvento(){
    if(this.clienteProyectos.idPrioProyecto == 0){
      this.Cliente();
      this.Proyecto();      
      this.title = 'Asignar Proyecto a Cliente';      
      this.stateCtrlCli.enable();      
      this.stateCtrlPro.enable();
    } else {
      this.title = 'Editar Asignacion';
      this.setCliente(this.clienteProyectos.idCliente);
      this.stateCtrlCli.setValue(this.clienteProyectos.compCliente);
      this.stateCtrlCli.disable();
      this.setProyecto(this.clienteProyectos.idProyecto);
      this.stateCtrlPro.setValue(this.clienteProyectos.nomProyecto);
      this.stateCtrlPro.disable();
    }
  }

  Estatus(){
    this.http.ProyectoEstatus().subscribe(result=>{
      this.estatus = result;
    },
    error=>{
      console.log(<any>error);
    });
  }

  Progreso(){
    this.http.ProyectoProgreso().subscribe(result=>{
      this.progreso = result;
    },
    error=>{
      console.log(<any>error);
    });
  }

  Nuevo(){
    this.http.ClienteProyectoCrear(this.clienteProyectos).subscribe(result=>{
      this.Modal.close();
    },
    error=>{ console.log(<any>error); });
  }

  Editar(){
    this.http.ClienteProyectoEditar(this.clienteProyectos).subscribe(result=>{
      this.Modal.close();
    },
    error=>{ console.log(<any>error); });
  }

  onNoClick():void{
    this.Modal.close();
  }

  Cliente(){
    this.http.Cliente().subscribe(result=>{
      this.ClienteI = Object.values(result);
      this.FilteredCli = this.stateCtrlCli.valueChanges.pipe(startWith(''), map(clientes => clientes ? this.filterCli(clientes):this.ClienteI.slice()));
    },
    error=>{ console.log(<any>error); });
  }

  Proyecto(){
    this.http.Proyectos().subscribe(result=>{
      this.ProyectoI = Object.values(result);
      this.FilteredPro = this.stateCtrlPro.valueChanges.pipe(startWith(''), map(proyectos => proyectos ? this.filterPro(proyectos):this.ProyectoI.slice()));
    },
    error=>{ console.log(<any>error); });
  }

  setCliente(idCli){
    this.clienteProyectos.idCliente = idCli;
  }

  setProyecto(idPro){
    this.clienteProyectos.idProyecto = idPro;
  }
  
}
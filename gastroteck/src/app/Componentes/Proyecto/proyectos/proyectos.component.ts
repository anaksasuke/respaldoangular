import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ProyectosService } from '../../../Servicios/proyectos.service';
import { Proyecto } from '../../../Modelos/Proyecto';

@Component({
  selector: 'app-proyectos',
  templateUrl: './proyectos.component.html',
  styleUrls: ['./proyectos.component.scss']
})
export class ProyectosComponent implements OnInit {
  public proyectos = new Proyecto(0, 0, 0, 0, '', '');
  Columns = ['acciones', 'nomProyecto', 'descProyecto', 'catProyecto', 'espProyecto', 'fAltaProyecto', 'usuario'];
  title = 'Nuevo Proyecto';
  dataSource: any;
  @ViewChild(MatPaginator, {static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static:true}) sort:MatSort;

  constructor(public http:ProyectosService, public dialog:MatDialog) { }

  ngOnInit() {
    this.RenderTable();
    this.proyectos.idUsuario = localStorage.Usuario;
  }

  RenderTable(){
    this.http.Proyectos().subscribe(result=>{
      this.dataSource = new MatTableDataSource;
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, error=>{ console.log(<any>error); });
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
      this.dataSource.paginator.firstPage();
  }

  Cargar(row){
    this.proyectos.idProyecto = row.idProyecto;
    this.proyectos.nomProyecto = row.nomProyecto;
    this.proyectos.descProyecto = row.descProyecto;
    this.proyectos.idCatProyecto = row.idCatProyecto;
    this.proyectos.idEspProyecto = row.idEspProyecto;
    this.openDialog();
  }

  openDialog():void{
    const dialogRef = this.dialog.open(ModalProyectos,{
      width:'750px',
      height:'350px',
      data:this.proyectos
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.RenderTable();
    });
  }

  Reset(){
    this.proyectos = { idUsuario:this.proyectos.idUsuario, idProyecto:0, idCatProyecto:0, idEspProyecto:0, nomProyecto:'', descProyecto:'' };
    this.title = 'Nuevo Proyecto';
  }

  Eliminar(row){
    this.proyectos.idProyecto = row.idProyecto;
    this.proyectos.idCatProyecto = row.idCatProyecto;
    this.proyectos.idEspProyecto = row.idEspProyecto;
    this.proyectos.nomProyecto = row.nomProyecto;
    this.proyectos.descProyecto = row.descProyecto;
    this.http.ProyectoEliminar(this.proyectos).subscribe(result=>{
      this.ngOnInit();
    }, error=>{ console.log(<any>error); });
  }
  
}

@Component({
  selector:'proyectosModelo',
  templateUrl:'./proyectosModelo.html',
})
export class ModalProyectos implements OnInit{
  categoria; especialidad; title;

  constructor(public Modal:MatDialogRef<ModalProyectos>, public http:ProyectosService, @Inject(MAT_DIALOG_DATA) public proyectos:Proyecto){}

  ngOnInit(){
    this.Categoria();
    this.Especialidad();
    this.validador();
  }

  validador(){
    if(this.proyectos.idProyecto == 0)
      this.title = "Nuevo Proyecto";
    else 
      this.title = "Editar Proyecto";    
  }

  Categoria(){
    this.http.ProyectoCategorias().subscribe(result=>{
      this.categoria = result;
    }, error=>{ console.log(<any>error); });
  }

  Especialidad(){
    this.http.ProyectoEspecialidades().subscribe(result=>{
      this.especialidad = result;
    }, error=>{ console.log(<any>error); });
  }

  Nuevo(){
    this.http.ProyectoCrear(this.proyectos).subscribe(result=>{
      this.Modal.close();
    }, error=>{ console.log(<any>error); });
    }
  
  Editar(){
    this.http.ProyectoEditar(this.proyectos).subscribe(result=>{
      this.Modal.close();
    }, error=>{ console.log(<any>error); });
    }

    onNoClick():void{
      this.Modal.close();
    }

}
import { Component, OnInit } from '@angular/core';
import { Usuarios } from '../../../Modelos/Usuarios';
import { UtilService } from '../../../util.service';
import { UsuarioService } from '../../../Servicios/usuarios.service';

@Component({
  selector: 'app-alta-u',
  templateUrl: './alta-u.component.html',
  styleUrls: ['./alta-u.component.scss']
})
export class AltaUComponent implements OnInit {
  usuario = new Usuarios(0,'','','','','','','',0,0,0);
  selcRol; rol;
  selcEstatus;
  constructor(public util:UtilService,public http:UsuarioService) { }

  ngOnInit() {
    this.usuario.idUser = localStorage.Usuario;
    this.Estatus();
    this.Rol();
    if(this.rol = localStorage.Rol != 1){
      this.util.navegacion("");
    }
  }

  Crear(){
    this.http.UsuariosCrear(this.usuario).subscribe(result => {
      this.util.NavegacionDinamica(result,'usuario');
      console.log(result);
    },
    error => { console.log(<any>error); });
  }

  Regresar(){    
    this.util.navegacion('/usuario');
  }

  Rol(){
    this.http.Rol().subscribe(result => {
      this.selcRol = result;
    },
    error=>{ console.log(<any>error); });
  }

  Estatus(){
    this.http.Estatus().subscribe(result => {
      this.selcEstatus = result;
    },
    error => { console.log(<any>error); });
  }

}
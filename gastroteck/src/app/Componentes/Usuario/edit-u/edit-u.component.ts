import { Component, OnInit } from '@angular/core';
import { Usuarios } from '../../../Modelos/Usuarios';
import { UtilService } from '../../../util.service';
import { UsuarioService } from '../../../Servicios/usuarios.service';

@Component({
  selector: 'app-edit-u',
  templateUrl: './edit-u.component.html',
  styleUrls: ['./edit-u.component.scss']
})
export class EditUComponent implements OnInit {
  usuario = new Usuarios(0,'','','','','','','',0,0,0)
  constructor(public http:UsuarioService, public util:UtilService) { }
  Prueba;
  selcRol;
  selcEstatus;
  ngOnInit() {
    this.Prueba = this.http.showDatos();
    this.Estatus();
    this.Rol();
    this.CargaDatos();
    this.usuario.idUserAlt = localStorage.Usuario;
  }

  CargaDatos(){
    this.usuario = {
      idUser:this.Prueba.id_usuario,
      usuario:this.Prueba.usuario,
      contrasena:this.Prueba.password,
      curp:this.Prueba.curp.toUpperCase(),
      nombre:this.Prueba.nombre,
      APaterno:this.Prueba.paterno,
      AMaterno:this.Prueba.materno,
      correo:this.Prueba.email,
      idRol:this.Prueba.rol.toString(),
      idStatus:this.Prueba.estatus.toString(),
      idUserAlt:this.usuario.idUserAlt
    }
  }

  Editar(){
    this.http.UsuariosEditar(this.usuario).subscribe(result => {
      this.util.NavegacionDinamica(result,'usuario');
    },
    error => { console.log(<any>error); });
  }

  Rol(){
    this.http.Rol().subscribe(result => {
      this.selcRol = result;
    },
    error => { console.log(<any>error); });
  }

  Estatus(){
    this.http.Estatus().subscribe(result => {
      this.selcEstatus = result;
    },
    error => { console.log(<any>error); });
  }

  Regresar(){    
    this.util.navegacion('/usuario');
  }

}
import { Component, OnInit,ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UtilService } from '../../../util.service';
import { UsuarioService } from '../../../Servicios/usuarios.service';

@Component({
  selector: 'app-show-u',
  templateUrl: './show-u.component.html',
  styleUrls: ['./show-u.component.scss']
})
export class ShowUComponent implements OnInit {
  Columns = ['usuario','emailUsuario','nomComUsuario','rolUsuario','Acciones'];
  dataSource:any;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  constructor(public http:UsuarioService,public util:UtilService) { }
  idUsuarioAL; Rol;
  ngOnInit() {
    this.RenderTable();
    this.idUsuarioAL = localStorage.Usuario;
    this.Rol = localStorage.Rol;
    if(this.Rol != 1){
      this.util.navegacion("");
    }
  }

  RenderTable(){
    this.http.Usuarios().subscribe(result => {
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort  =this.sort;
    },
    error=>{ console.log(<any>error); });
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter  = filtroStr;
    if(this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  Nuevo(){
    this.util.navegacion('usuario/crear');
  }

  Editar(row){
    this.http.UsuariosSub(row.idUsuario).subscribe(result => {
      this.http.getDatos(result);
      this.util.navegacion('usuario/editar');
    },
    error => { console.log(<any>error); });
  }

  Delet(row){
    var array = {idUsuario:row.idUsuario,idUsuarioAL:this.idUsuarioAL}
    this.http.UsuariosDelet(array).subscribe(result => {
      this.ngOnInit();
    },
    error => { console.log(<any>error); });
  }

}
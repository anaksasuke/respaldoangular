import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import * as moment from 'moment';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { PagosService } from '../../Servicios/pagos.service';
import { UtilService } from '../../util.service';
import { PagosFacturas } from '../../Modelos/PagosFacturas';
import { ClienteFacturaInter } from '../../Interfaces/ClienteFactura';
import { PagoEstatus } from '../../Modelos/PagoEstatus';
import { PagoEntradas } from '../../Modelos/PagoEntradas';
import { ProveedorInter } from '../../Interfaces/Proveedor';
import { PagoFolio } from '../../Modelos/PagoFolio';
@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.scss']
})
export class PagosComponent implements OnInit {

  constructor(public http:PagosService,public util:UtilService,public dialog:MatDialog) { }
  pago = new PagosFacturas(0,0,'',0,0,0,0,0,0,0,0,0,0,0,'',0);
  pagoEntrada = new PagoEntradas(0,0,0,0,0,0,"",0,0,0,0,0,0,'','',0);
  estatus = new PagoEstatus(0,0,0,0,0);
  folio = new PagoFolio(0,0,0,0,'',0);
  Columns = ['numeroPago','nomEntrada','nomProveedor','estOperacion','impPago','resEntrada','Acciones'];
  dataSource:any;
  dataSourceFactura:any;
  ColumnsFactura = ['numeroPago','nomFactura','razSocCliente','estOperacion','impPago','resFactura','Acciones'];
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  ngOnInit() {
    this.pago.idUsuario = localStorage.Usuario;
    this.pagoEntrada.idUsuario = localStorage.Usuario;
    this.estatus.idUsuario = localStorage.Usuario;
    this.TablaEntradas();
    this.TablaFactura();
  }

  TablaEntradas(){
    this.http.PagosEntrada().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },error=>{console.log(<any>error);});
  }

  TablaFactura(){
    this.http.PagosFactura().subscribe(result=>{
      this.dataSourceFactura = new MatTableDataSource();
      this.dataSourceFactura.data = result;
      this.dataSourceFactura.paginator = this.paginator;
      this.dataSourceFactura.sort = this.sort;
    },error=>{console.log(<any>error);});
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter  = filtroStr;
    if(this.dataSource.paginator)
    {this.dataSource.paginator.firstPage();}
  }

  openDialog(){
    const dialogRef = this.dialog.open(pagosPop,{
      width:'700px',
      height:'565px',
      data:this.pago
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.TablaFactura();
    });
  }

  CargarDatos(row){
    this.pago = {idPago:row.idPago,idFactura:row.idFactura,versionFacutra:row.versFactura,idCliente:row.idCliente,nombreCliente:row.razSocCliente,idBancoEmisor:row.bancoCliente,cuentaEmisora:row.ctaCliente,idBancoReceptor:row.bancoPago,cuentaReceptora:row.ctaPago,idMovimiento:row.idMovPago,cheques:row.chequePago,fichas:row.fichaPago,fecha:row.fAltaPago,importe:row.impPago,restante:row.resFactura,idUsuario:this.pago.idUsuario}
    this.openDialog();
  }

  Limpiar(){
    this.pago = {idPago:0,idFactura:0,versionFacutra:0,idCliente:0,nombreCliente:"",idBancoEmisor:0,cuentaEmisora:0,idBancoReceptor:0,cuentaReceptora:0,idMovimiento:0,cheques:0,fichas:0,fecha:"",importe:0,restante:0,idUsuario:this.pago.idUsuario}
  }

  CambiarEstatus(row){
    this.estatus = {idPago:row.idPago,id:row.idFactura,version:row.versFactura,idEstatus:row.idEstOperacion,idUsuario:this.pago.idUsuario};
    this.http.EstatusPago(this.estatus).subscribe(result=>{
      console.log(result);
      this.TablaFactura();
    },error=>{console.log(<any>error);});
  }

  BorrarPago(row){
    this.estatus = {idPago:row.idPago,id:row.idFactura,version:row.versFactura,idEstatus:row.idEstOperacion,idUsuario:this.pago.idUsuario};
    this.http.BorrarPago(this.estatus).subscribe(result=>{
      console.log(result);
      this.TablaFactura();
    },error=>{console.log(<any>error);});
  }

  LipiarPagoEntrada(){
    this.pagoEntrada = {idPago:0,idEntrada:0,version:0,idBanco:0,cuenta:0,idProveedor:0,nombProveedor:"",bancoProveedor:0,cuentaProveedor:0,idMovimiento:0,cheque:0,importe:0,restante:0,ficha:"",fecha:"",idUsuario:this.pagoEntrada.idUsuario}
  }

  OpenDialogEntrada(){
    const dialogRef = this.dialog.open(pagoEntrada,{
      width:'480px',
      height:'555px',
      data:this.pagoEntrada
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.TablaEntradas();
    });
  }

  CargarDatosEntrada(row){
    this.pagoEntrada = {idPago:row.idPago,idEntrada:row.idEntrada,version:row.versEntrada,idBanco:row.bancoPago,cuenta:row.ctaPago,idProveedor:row.idProveedor,nombProveedor:row.nomProveedor,bancoProveedor:row.bancoProveedor,cuentaProveedor:row.ctaProveedor,idMovimiento:row.idMovPago,cheque:row.chequePago,importe:row.impPago,restante:row.resEntrada,ficha:row.fichaPago,fecha:row.fAltaPago,idUsuario:this.pagoEntrada.idUsuario}
    this.OpenDialogEntrada();
  }

  CambiarEstatusEn(row){
    this.estatus = {idPago:row.idPago,id:row.idEntrada,version:row.versEntrada,idEstatus:row.idEstOperacion,idUsuario:this.estatus.idUsuario};
    this.http.EstatusEntrada(this.estatus).subscribe(result=>{
      console.log(result);
      this.TablaEntradas();
    },error=>{console.log(<any>error);});
  }

  BorrarPagoEnt(row){
    this.estatus = {idPago:row.idPago,id:row.idEntrada,version:row.versEntrada,idEstatus:row.idEstOperacion,idUsuario:this.estatus.idUsuario};
    this.http.BorrarEntradaPago(this.estatus).subscribe(result=>{
      console.log(result);
      this.TablaEntradas();
    },error=>{console.log(<any>error);});
  }

  AgregarFolio(param,row){
    if(parseInt(param) == 1){
      this.folio = {idPago:row.idPago,id:row.idFactura,version:row.versFactura,idUsuario:this.pago.idUsuario,folio:row.fFiscPago,tipo:0};
      this.OpendDialogFolio();
    }else {
      this.folio = {idPago:row.idPago,id:row.idEntrada,version:row.versEntrada,idUsuario:this.pago.idUsuario,folio:row.fFiscPago,tipo:1};
      this.OpendDialogFolio();
    } 
  }

  OpendDialogFolio(){
    const dialogRef = this.dialog.open(pagoFolio,{
      width:'auto',
      height:'auto',
      data:this.folio
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.TablaEntradas();
      this.TablaFactura();
    });
  }

}

@Component({
  selector:"pagosPop",
  templateUrl:"pagosPop.html",
  providers:[{provide:DateAdapter,useClass:MomentDateAdapter,deps:[MAT_DATE_LOCALE]},
  {provide:MAT_DATE_FORMATS,useValue:MAT_MOMENT_DATE_FORMATS}],
})

export class pagosPop implements OnInit {
  constructor(public modal:MatDialogRef<pagosPop>,public http:PagosService,@Inject(MAT_DIALOG_DATA) public pago:PagosFacturas,public adapter:DateAdapter<any>){}

  stateCtrl = new FormControl();
  FilterCliente:Observable<ClienteFacturaInter[]>
  cliente:ClienteFacturaInter[]=[];
  facturas; bancoE; cuentaE; bancoR; cuentaR; check = false; movimiento;
  ngOnInit(){
    this.adapter.setLocale('es');
    this.BancoReceptor();
    this.Movimientos();
    this.Ubicador();
  }

  Ubicador(){
    if(this.pago.idPago != 0){
      this.stateCtrl.setValue(this.pago.nombreCliente);
      this.stateCtrl.disable();
      this.Facturas(this.pago.idCliente);
      this.BancoEmisor(this.pago.idCliente);
      this.CuentaEmisora(this.pago.idBancoEmisor);
      this.CuentaReceptora(this.pago.idBancoReceptor);
      if(this.pago.cheques != 0){this.check = true}
    }else {
      this.Clientes();
    }
  }

  Clientes(){
    this.http.Clientes().subscribe(result=>{
      this.cliente = Object.values(result);
      this.FilterCliente = this.stateCtrl.valueChanges.pipe(startWith(''),map(cli => cli ? this._filterCliente(cli): this.cliente.slice()))
    },error=>{console.log(<any>error);});
  }

  _filterCliente(value:string):ClienteFacturaInter[]{
    const filtrovalue = value.toLowerCase();
    return this.cliente.filter(cli => cli.razSocCliente.toLowerCase().indexOf(filtrovalue) === 0);
  }

  Facturas(idCliente){
    this.pago.idCliente = idCliente;
    this.http.Facturas(idCliente).subscribe(result=>{
      this.facturas = result;
    },error=>{console.log(<any>error);});
  }

  Movimientos(){
    this.http.Movimientos().subscribe(result=>{
      this.movimiento = result;
    },error=>{console.log(<any>error);});
  }

  obtVersion(version,restante){
    this.pago.versionFacutra = version;
    this.pago.restante = restante;
  }

  BancoEmisor(idCliente){
    this.http.BancoEmisor(idCliente).subscribe(result=>{
      this.bancoE = result;
    },error=>{console.log(<any>error);});
  }

  CuentaEmisora(idBanco){
    this.http.CuentaEmisora(this.pago.idCliente,idBanco).subscribe(result=>{
      this.cuentaE = result;
    },error=>{console.log(<any>error);});
  }

  BancoReceptor(){
    this.http.BancoReceptor().subscribe(result=>{
      this.bancoR = result;
    },error=>{console.log(<any>error);});
  }

  CuentaReceptora(idBanco){
    this.http.CuentaReceptora(idBanco).subscribe(result=>{
      this.cuentaR = result;
    },error=>{console.log(<any>error);});
  }

  prueba(){
    if(this.check == true){
      this.pago.cheques = 0;
    }
  }

  Guardar(){
    this.pago.fecha = moment(this.pago.fecha).format("YYYY-MM-DD");
    this.http.GuardarPagoFactura(this.pago).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },error=>{console.log(<any>error);});
  }

  Actualizar(){
    this.pago.fecha = moment(this.pago.fecha).format("YYYY-MM-DD");
    this.http.ActualizarPago(this.pago).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },error=>{console.log(<any>error);});
  }

  Cerrar(){
    this.modal.close();
  }

}


@Component({
  selector:"pagoEntrada",
  templateUrl:"pagoEntrada.html"
})

export class pagoEntrada implements OnInit {
  constructor(public modal:MatDialogRef<pagoEntrada>,public http:PagosService,@Inject(MAT_DIALOG_DATA) public pago:PagoEntradas,public adapter:DateAdapter<any>){}
  stateCtrl = new FormControl();
  FilterProveedor:Observable<ProveedorInter[]>
  Proveedor:ProveedorInter[]=[];
  entradas; bancoPro; cuentaPro; bancoEmp; cuentaEmp; movimiento; check = false;
  ngOnInit(){
    this.Movimientos();
    this.BancoReceptor();
    this.Ubicador();
  }

  Ubicador(){
    if(this.pago.idPago != 0){
      this.stateCtrl.setValue(this.pago.nombProveedor);
      this.stateCtrl.disable();
      this.ObtenerProveedor(this.pago.idProveedor);
      this.BancoProveedor();
      this.CuentaProveedor(this.pago.bancoProveedor);
      this.CuentaReceptora(this.pago.idBanco);
    }else{
      this.Proveedores();
    }
  }

  Proveedores(){
    this.http.Proveedores().subscribe(result=>{
      this.Proveedor = Object.values(result);
      this.FilterProveedor = this.stateCtrl.statusChanges.pipe(startWith(''),map(pro => pro ? this.filterPro(pro):this.Proveedor.slice()));
    },error=>{console.log(<any>error);});
  }

  filterPro(value:string):ProveedorInter[]{
    const filterValue = value.toLowerCase();
    return this.Proveedor.filter(pro => pro.nomProveedor.toLowerCase().indexOf(filterValue) === 0);
  }

  ObtenerProveedor(idProveedor){
    this.pago.idProveedor = idProveedor;
    this.http.Entradas(idProveedor).subscribe(result=>{
      this.entradas = result;
    },error=>{console.log(<any>error);});
  }

  ObtVersion(version,restante){
    this.pago.version = version;
    this.pago.restante = restante;
  }

  Movimientos(){
    this.http.Movimientos().subscribe(result=>{
      this.movimiento = result;
    },error=>{console.log(<any>error);});
  }

  BancoProveedor(){
    this.http.BancoProveedor(this.pago.idProveedor).subscribe(result=>{
      this.bancoPro = result;
    },error=>{console.log(<any>error);});
  }

  CuentaProveedor(idcuenta){
    this.http.CuentaProveedor(idcuenta,this.pago.idProveedor).subscribe(result=>{
      this.cuentaPro = result;
    },error=>{console.log(<any>error);});
  }

  BancoReceptor(){
    this.http.BancoReceptor().subscribe(result=>{
      this.bancoEmp = result;
    },error=>{console.log(<any>error);});
  }

  CuentaReceptora(idBanco){
    this.http.CuentaReceptora(idBanco).subscribe(result=>{
      this.cuentaEmp = result;
    },error=>{console.log(<any>error);});
  }

  Check(){
    if(this.check == true){
      this.pago.cheque = 0;
    }
  }

  Guardar(){
    this.pago.fecha = moment(this.pago.fecha).format("YYYY-MM-DD");
    this.http.GuardarPagoEnt(this.pago).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },error=>{console.log(<any>error);});
  }

  Actualizar(){
    this.pago.fecha = moment(this.pago.fecha).format("YYYY-MM-DD");
    this.http.ActualizarPagoEnt(this.pago).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },error=>{console.log(<any>error);});
  }

  Cerrar(){
    this.modal.close();
  }
}

@Component({
  selector:"pagoFolio",
  templateUrl:"pagoFolio.html"
})

export class pagoFolio implements OnInit{
  constructor(public modal:MatDialogRef<pagoEntrada>,public http:PagosService,@Inject(MAT_DIALOG_DATA) public pago:PagoFolio){}

  ngOnInit(){
    console.log(this.pago);
  }

  GuardarEntrada(){
    this.http.AgregarFolioEnt(this.pago).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },error=>{console.log(<any>error);});
  }

  GuardarFactura(){
    this.http.AgregarFolioFac(this.pago).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },error=>{console.log(<any>error);});
  }

  Cancelar(){
    this.modal.close();
  }

}
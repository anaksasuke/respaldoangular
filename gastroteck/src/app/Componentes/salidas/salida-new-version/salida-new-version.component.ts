import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FormControl } from '@angular/forms';
import * as moment from 'moment';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Entradas } from 'src/app/Interfaces/Entradas';
import { ClienteInter } from 'src/app/Interfaces/Cliente';
import { SalidaService } from '../../../Servicios/Salida.service';
import { UtilService } from 'src/app/util.service';
import { Salidas } from '../../../Modelos/Salida';
import { Salidart } from 'src/app/Modelos/Salidart';
import { SalidaCargarArt } from '../../../Modelos/SalidaCargarArt';

@Component({
  selector: 'app-salida-new-version',
  templateUrl: './salida-new-version.component.html',
  styleUrls: ['./salida-new-version.component.scss'],
  providers:[{provide:DateAdapter,useClass:MomentDateAdapter,deps:[MAT_DATE_LOCALE]},
  {provide:MAT_DATE_FORMATS,useValue:MAT_MOMENT_DATE_FORMATS}],
})
export class SalidaNewVersionComponent implements OnInit {
  salida = new Salidas(0,0,0,'','','','','',0,0,'','','');
  salidaArticulo = new Salidart(0,0,0,"",0,0,"",0,0,"",0,"",0,0,0,0,"",0,"","","","");
  CargarArticulos = new SalidaCargarArt(0,0,0);
  Columns = ['nomEntrada', 'nomOrden', 'nomCotizacion', 'modArticulo', 'nomArticulo', 'nomProveedor', 'cantArticulo','Acciones'];
  dataSource: any;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;

  CtrlCliente = new FormControl();
  FilteredCliente:Observable<ClienteInter[]>;
  Cliente:ClienteInter[]=[];

  CtrlEntrada = new FormControl();
  FilteredEntradas:Observable<Entradas[]>;
  Entra:Entradas[]=[];

  constructor(public http:SalidaService,public util:UtilService) { }
  tSalida; validador=0; existencia=0;

  ngOnInit() {
    this.salida.idUsuario = localStorage.Usuario;
    this.CargarDatos();
    this.TipoSalida();
  }

  TipoSalida(){
    this.http.TipoSalida().subscribe(result=>{
      this.tSalida = result;
    },error=>{console.log(<any>error);});
  }

  CargarDatos(){
    var datos = this.util.showDatos();
    this.salida = {idUsuario:this.salida.idUsuario,idCliente:datos.idCliente,idSalida:datos.idSalida,version:datos.verSalida,tipoSalida:datos.idTipOperacion,destino:datos.destSalida,direccion:datos.dirSalida,referencia:datos.refSalida,fechaEntregado:datos.fRecSalida,fechaSalida:datos.fSalida,comentario:datos.comSalida,observaciones:datos.obSalida,pie:datos.pieSalida};
    this.CargarArtSalida();
    this.ValidarTipo(this.salida.tipoSalida);
    this.CtrlCliente.setValue(datos.compCliente);
    this.CtrlCliente.disable();
    this.CtrlEntrada.setValue(datos.entSalida);
  }

  CargarArtSalida(){
    this.http.CargarSalida(this.salida.idUsuario,this.salida.idSalida,this.salida.version).subscribe(result=>{
      console.log(result);
      this.TablaArticulos();
    },error=>{console.log(<any>error);});
  } 

  TablaArticulos(){
    this.http.TablaArticulo(this.salida.idUsuario).subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },error=>{console.log(<any>error);});
  }

  ValidarTipo(tipo){
    if(tipo == 2){
      this.Entradas(this.salida.idCliente);
    }else {
      this.Entradas(0);
    }
  }

  Entradas(id){
    this.http.Entrada(id).subscribe(result=>{
      this.Entra = Object.values(result);
      this.FilteredEntradas = this.CtrlEntrada.valueChanges.pipe(startWith(''),map(ent => ent ? this.filterEnt(ent):this.Entra.slice()));
    },error=>{console.log(<any>error);});
  }

  filterEnt(value:string):Entradas[]{
    const filterValue = value.toLowerCase();
    return this.Entra.filter(ent => ent.nomEntrada.toLowerCase().indexOf(filterValue) === 0);
  }

  ObtEntrada(param){
    this.CargarArticulos = {idUsuario:this.salida.idUsuario,idEntrada:param.idEntrada,version:param.versEntrada};
    this.http.RellenarTabla(this.CargarArticulos).subscribe(result=>{
      console.log(result);
      this.TablaArticulos();
    },error=>{console.log(<any>error);});
  }

  CargarArticulo(row){
    this.validador = 1;
    this.existencia = row.cantArticulo;
    this.salidaArticulo = {idUsuario:this.salida.idUsuario,idEntrada:row.idEntrada,versEntrada:row.versEntrada,nomEntrada:row.nomEntrada,idOrden:row.idOrden,versOrden:row.versOrden,nomOrden:row.nomOrden,idCotizacion:row.idCotizacion,versCotizacion:row.versCotizacion,nomCotizacion:row.nomCotizacion,idArea:row.idAreaCotizacion,nombreArea:row.areaCotizacion,idPartida:row.idPartida,cantArticulo:row.cantArticulo,costo:row.cosArticulo,idIva:row.idIva,iva:row.iva,idMoneda:row.idMoneda,moneda:row.cveMoneda,modArticulo:row.modArticulo,nomArticulo:row.nomArticulo,nomProveedor:row.nomProveedor}
  }

  Actualizar(){
    this.http.EditarArticulo(this.salidaArticulo).subscribe(result=>{
      console.log(result);
      this.TablaArticulos();
      this.Cancelar();
    },error=>{console.log(<any>error);});
  }

  Eliminar(row){
    this.salidaArticulo = {idUsuario:this.salida.idUsuario,idEntrada:row.idEntrada,versEntrada:row.versEntrada,nomEntrada:row.nomEntrada,idOrden:row.idOrden,versOrden:row.versOrden,nomOrden:row.nomOrden,idCotizacion:row.idCotizacion,versCotizacion:row.versCotizacion,nomCotizacion:row.nomCotizacion,idArea:row.idAreaCotizacion,nombreArea:row.areaCotizacion,idPartida:row.idPartida,cantArticulo:row.cantArticulo,costo:row.cosArticulo,idIva:row.idIva,iva:row.iva,idMoneda:row.idMoneda,moneda:row.cveMoneda,modArticulo:row.modArticulo,nomArticulo:row.nomArticulo,nomProveedor:row.nomProveedor}
    this.http.EliminarArticulo(this.salidaArticulo).subscribe(result=>{
      console.log(result);
      this.TablaArticulos();
      this.Cancelar();
    },error=>{console.log(<any>error);});
  }

  Cancelar(){
    this.salidaArticulo = {idUsuario:this.salida.idUsuario,idEntrada:0,versEntrada:0,nomEntrada:"",idOrden:0,versOrden:0,nomOrden:"",idCotizacion:0,versCotizacion:0,nomCotizacion:"",idArea:0,nombreArea:"",idPartida:0,cantArticulo:0,costo:0,idIva:0,iva:"",idMoneda:0,moneda:"",modArticulo:"",nomArticulo:"",nomProveedor:""};
    this.validador = 0;
  }

  GuardarSalida(){
    this.salida.fechaEntregado = moment(this.salida.fechaEntregado).format("YYYY-MM-DD");
    this.salida.fechaSalida = moment(this.salida.fechaSalida).format("YYYY-MM-DD");
    this.http.GuardarSalida(this.salida).subscribe(result=>{
      console.log(result);
      this.util.NavegacionDinamica(result,"salida");
    },error=>{console.log(<any>error);});
  }

}

import { Component, OnInit,ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SalidaService } from '../../../Servicios/Salida.service';
import { UtilService } from '../../../util.service';
import { Estatus } from '../../../Modelos/SalidaEstatus';
@Component({
  selector: 'app-salida-show',
  templateUrl: './salida-show.component.html',
  styleUrls: ['./salida-show.component.scss']
})
export class SalidaShowComponent implements OnInit {
  Columns=["nomSalida","entSalida","ordSalida","cotSalida","estOperacion","compCliente","totSalida","fModSalida","Acciones"];
  dataSource:any;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  estatus = new Estatus(0,0,0,0);
  constructor(public http:SalidaService,public util:UtilService,public dialog:MatDialog) { }

  ngOnInit() {
    this.TablaRender();
    this.estatus.idUsuario = localStorage.Usuario;
  }

  TablaRender(){
    this.http.Salidas().subscribe(result=>{
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = result;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },error=>{console.log(<any>error);});
  }

  Filtro(filtroStr:string){
    filtroStr = filtroStr.trim().toLowerCase();
    this.dataSource.filter = filtroStr;
    if(this.dataSource.paginator)
    {
      this.dataSource.paginator.firstPage();
    }
  }

  OpendEstatus(row){
    this.estatus = {idUsuario:this.estatus.idUsuario,idSalida:row.idSalida,verSalida:row.verSalida,idEstatus:row.idEstOperacion}
    const dialogRef = this.dialog.open(salidaEstatus,{
      width:'auto',
      height:'auto',
      data:this.estatus
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.TablaRender();
    });
  }


  Editar(row){
    this.util.getDatos(row);
    this.util.navegacion("salida/editar");
  }

  NuevaVersion(row){
    this.util.getDatos(row);
    this.util.navegacion("salida/nueva-version");
  }

  Eliminar(row){
    this.estatus = {idUsuario:this.estatus.idUsuario,idSalida:row.idSalida,verSalida:row.verSalida,idEstatus:row.idEstOperacion}
    this.http.EliminarSalida(this.estatus).subscribe(result =>{
      this.TablaRender();
      console.log(result);
    },error=>{console.log(<any>error);});
  }

}

@Component({
  selector:"salidaEstatus",
  templateUrl:"salidaEstatus.html"
})

export class salidaEstatus implements OnInit {
  constructor(public modal:MatDialogRef<salidaEstatus>,public http:SalidaService,@Inject(MAT_DIALOG_DATA) public estatus:Estatus){}
  status;

  ngOnInit(){
    this.Status();
  }

  Status(){
    this.http.Estatus().subscribe(result =>{
      this.status = result;
    },error=>{console.log(<any>error);});
  }

  CambiarEstatus(){
    this.http.CambiaEstatus(this.estatus).subscribe(result=>{
      console.log(result);
      this.modal.close();
    },error=>{console.log(<any>error);});
  }

  Cancelar(){
    this.modal.close();
  }

}
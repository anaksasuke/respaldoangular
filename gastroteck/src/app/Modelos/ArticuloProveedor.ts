export class ArticuloProveedor {
    constructor(
        public idArticulo: number,
        public idProveedor: number,
        public idMoneda: number,
        public precArticulo: number,
        public dctoArticulo: number,
        public gtosArticulo: number,
        public ginsArticulo: number,
        public tEntArticulo: string,
        public idUsuario: number,
        public title:string,
        public nomArticulo: string,   
        public nomProveedor: string
    ){}
}
export class Banco {
    constructor(
        public idBanco:number,
        public idTipo:number,
        public rfc:string,
        public nomBanco:string,
        public razonBanco:string, 
        public idUser:number
    ){}
}
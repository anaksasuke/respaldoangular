export class CambiarEstatusFactura {
    constructor(
        public idUsuario:number, 
        public idFactura:number,
        public version:number,
        public idEstatus:number,
        public idTipo:number
    ){}
}
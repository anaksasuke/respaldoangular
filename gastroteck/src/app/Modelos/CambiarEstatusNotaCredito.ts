export class CambiarEstatusNotaCredito {
    constructor(
        public idUsuario:number, 
        public idNotaCredito: number,
        public versNotaCredito: number,
        public idTCfdiRel:number,
        public idEstOperacion: number,
    ){}
}
export class CambiarEstatusSalida {
    constructor(
        public idUsuario:number,
        public idSalida:number,
        public verSalida:number,
        public idEstOperacion:number
    ){}
}
export class Catalago {
    constructor(
        public idCatalago:number,
        public idClas:number,
        public Model:string,
        public SAT:number,
        public Nom:string,
        public Des:string,
        public ficha:string,
        public img:string,
        public Frente:number,
        public Profundidad:number,
        public Altura:number,
        public idPlomeria:number,
        public idPlomeriaC:number, 
        public idPlomeriaF:number,
        public idPlomeriaD:number,
        public idElectrico:number,
        public idGas:number,
        public idMarca:number,
        public idDep:number,
        public idGru:number,
        public idLine:number,
        public idUMed:number,
        public idTipo:number,
        public idUser:number
    ){}
}
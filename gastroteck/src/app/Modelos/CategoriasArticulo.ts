export class CategoriaArticulo {
    constructor(
        public categoria:string,
        public idCategoria:number,
        public idUser:number
    ){}
}
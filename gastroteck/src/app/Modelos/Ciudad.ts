export class Ciudad {
    constructor(
        public idPais:number,
        public idEstado:number,
        public idCiudad:number,
        public idUser:number,
        public nomCiudad:string
    ){}
}
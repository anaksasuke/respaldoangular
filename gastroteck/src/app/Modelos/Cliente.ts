export class Cliente {
    constructor(
        public idCliente:number,
        public NomComer:string,
        public tel:number,
        public idTipo:number,
        public idUser:number
    ){}
}
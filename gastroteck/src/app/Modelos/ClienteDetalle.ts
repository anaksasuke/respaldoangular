export class ClienteDetalle {
    constructor(
        public idCliente:number,
        public idNuvCli:number,
        public compCliente:string,
        public telFijo:number,
        public idTipoCli:number,
        public idEspCli:number,
        public idGiroCli:number,
        public idPais:number,
        public idEstado:number,
        public idCiudad:number,
        public idTipoPersona:number,
        public rfcCli:string,
        public razSocCli:string,
        public calleCli:string,
        public numCli:number,
        public colCli:string,
        public cpCli:number,
        public pWebCli:string,
        public comCli:string,
        public refCli:string,
        public idUser:number
    ){}
}
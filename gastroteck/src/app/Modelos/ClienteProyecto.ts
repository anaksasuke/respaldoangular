export class ClienteProyecto {
    constructor(
        public idCliente: number,
        public idProyecto: number,
        public idUsuario: number,
        public idPrioProyecto: number,
        public idEstProyecto: number,
        public idProgProyecto: number,
        public compCliente: string,
        public nomProyecto: string,
    ){}
}
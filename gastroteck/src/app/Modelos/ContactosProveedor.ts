export class ContactosProveedor {
    constructor(
        public idCon:number,
        public idPro:number,
        public idT:number,
        public nom:string,
        public pat:string,
        public mat:string,
        public car:string,
        public cel:number,
        public email:string,
        public def:number,
        public idUser:number
    ){}
}
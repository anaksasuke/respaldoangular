export class Convertidor {
    constructor(
        public idUsuario:number,
        public oriMoneda:number,
        public desMoneda:number,
        public convMoneda:number,
        public convertidor:number
    ){}
}
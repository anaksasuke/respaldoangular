export class CotArticulo {
    constructor(
        public idAreaCot:number,
        public idPartida:number,
        public idArticulo:number,
        public idProveedor:number,
        public idMoneda:number,
        public cantidad:number,
        public precio:number,
        public idIva:number,
        public marge:number,
        public descuento:number,
        public precioRep:number,
        public idUser:number, 
        public ubicador:number,
    ){}
}

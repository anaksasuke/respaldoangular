export class CotDatosGen {
    constructor(
        public idCliente:number,
        public idProyect:number,
        public referencia:string,
        public division:string,
        public idCot:number,
        public idUsuario:number,
        public idAgente:number,
        public version:number
    ){}
}
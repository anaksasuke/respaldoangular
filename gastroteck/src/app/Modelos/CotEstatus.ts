export class CotEstatus {
    constructor(
        public idCot:number,
        public version:number,
        public idEstatus:number,
        public idUsuario:number
    ){}
}
export class CotExcepcion {
    constructor(
        public idAreaCot:number,
        public idExcepcion:number,
        public NumExcep:number,
        public precio:number,
        public cantidad:number,
        public Descripcion:string,
        public idIva:number,
        public margen:number,
        public descuento:number,
        public idMoneda:number,
        public idUser:number
    ){}
}
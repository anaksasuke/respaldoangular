export class CotPolitica  {
    constructor(
        public idUsuario:number,
        public idPolitica:number,
        public valPolitica:string,
        public ubicador:number
    ){}
}
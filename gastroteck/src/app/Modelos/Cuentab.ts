export class Cuentab{
    constructor(
        public idCliente:number,
        public ctaResp:number,
        public Cta:number,
        public idBanco:number,
        public moduloC:number,
        public idUser:number,
        public condicion:number
    ){}
}
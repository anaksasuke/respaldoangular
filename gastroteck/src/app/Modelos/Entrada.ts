export class Entrada {
    constructor(
        public idUsuario:number,
        public idProveedor:number,
        public Facturas:string,
        public observaciones:string,
        public Referencia:string,
        public fecha:string,
        public gastos:number,
        public empaque:number,
        public instalacion:number,
        public idEntrada:number,
        public versionEntrada:number
    ){}
}
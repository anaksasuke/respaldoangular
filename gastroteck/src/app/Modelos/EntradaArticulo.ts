export class EntradaArticulo {
    constructor(
        public nomOrd:string,
        public nomCot:string ,
        public modeloArt:string,
        public nomArt:string,
        public idOrd:number,
        public ordVer:number,
        public idCot:number,
        public cotVer:number,
        public idArt:number,
        public idPartida:number,
        public idArea:number,
        public cantidad:number,
        public precio:number,
        public idIva:number,
        public iva:string,
        public idMoneda:number,
        public moneda:string,
       // public gastos:number,
        public idUsuario:number
    ){}
}
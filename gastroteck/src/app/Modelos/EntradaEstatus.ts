export class EntradaEstatus {
    constructor(
        public idUsuario:number,
        public idEntrada:number,
        public verEntrada:number,
        public idEstatus:number
    ){}
}
export class Estado {
    constructor(
        public idUsuario:number,
        public idPais:number,
        public PaisNom:string,
        public idEstado:number,
        public EstadoNom:string
    ){}
}
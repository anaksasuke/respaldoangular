export class Excepcion{
    constructor(
        public idExcepcion:number,
        public excepcion:string,
        public nomExcepcion:string,
        public codSatExcepcion: number,
        public margExcepcion: number,
        public idUsuario:number
    ){}
}
export class FactArticulos {
    constructor(
        public idAreaCot:number,
        public idArticulo:number,
        public idCot:number,
        public versionCot:number,
        public idProveedor:number,
        public idPartida:number,
        public idMoneda:number,
        public cantidad:number,
        public precio:number,
        public idIva:number,
        public marge:number,
        public descuento:number,
        public idUser:number,
        public CodigoSAT:number,
    ){}
}
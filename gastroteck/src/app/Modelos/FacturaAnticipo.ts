export class FacturaAnticipo {
    constructor(
        public idUsuario:number,
        public idCotizacion:number,
        public cntidad:number,
        public nombre:string,
        public codSat:number,
        public idArticulo:number
    ){}
}
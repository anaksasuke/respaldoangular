export class FacturaFolio {
    constructor(
        public idUsuario:number, 
        public idFactura:number,
        public version:number,
        public folioFis:string
    ){}
}
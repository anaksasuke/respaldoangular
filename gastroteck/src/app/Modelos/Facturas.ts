export class Facturas {
    constructor(
        public idFactura:number,
        public versionFactura:number,
        public idCliente:number,
        public tPago:number,
        public mPago:number,
        public fPago:number,
        public plazo:number,
        public folioFis:string,
        public CFDI:number,
        public CFDIRel:number,
        public anticipo: number,
        public referencia:string,
        public tFactura: number,
        public idUsuario:number,
    ){}
}
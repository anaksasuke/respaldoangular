export class FacturaPop {
    constructor(
        public idMoneda:number,
        public idMonedaDestino:number,
        public valorCambio:number,
        public idCotizacion:number,
        public version:number,
        public idUsuario:number,
        public ubicador:number,
    ){}
}
export class Grupos {
    constructor(
        public idGroup:number,
        public idDep:number,
        public nombreGroup:string,
        public idUser:number,
        public grupos:number
    ){}
}
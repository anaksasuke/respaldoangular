export class Linea {
    constructor(
        public idDepar:number,
        public idGrupo:number,
        public idLin:number,
        public nombLin:string,
        public idUser:number,
        public linea:number
    ){}
}
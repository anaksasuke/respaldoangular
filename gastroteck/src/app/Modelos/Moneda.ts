export class Moneda {
    constructor(
        public idUsuario:number,
        public idMoneda:number,
        public moneda:string,
        public cveMoneda:string
    ){}
}
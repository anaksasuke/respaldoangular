export class NotaCredito{
    constructor(
        public idCliente:number,
        public idUsuario:number,
        public idNotaCredito: number,
        public versNotaCredito: number,
        public idTipoCfdi:number,
        public idRelCfdi:number,
        public idFormPago:number,
        public fFiscNotaCredito:string,
        public plaNotaCredito:number,
        public antNotaCredito:number,
        public tPagoNotaCredito:number,
        public mPagoNotaCredito:number,
        public refNotaCredito:string,
    ){}
}
export class NotaCreditoArticulos{
    constructor(
        public idUsuario: number,
        public idFactura: number, 
        public versFactura: number, 
        public idCotizacion: number, 
        public versCotizacion: number,
        public idPartida:number, 
        public idAreaCotizacion: number, 
        public areaCotizacion: string,  
        public idArticulo: number, 
        public codSatArticulo: number,
        public nomArticulo: string, 
        public idProveedor: number, 
        public nomProveedor: string, 
        public cantArticulo: number,
        public cosArticulo: number, 
        public descuento:number,
        public idIva: number, 
        public idMoneda: number,  
    ){}
}
export class OrdenArticulo {
    constructor(
        public idUsuario:number,
        public idCotizacion:number,
        public idPartida:number,
        public idArea:number,
        public version:number,
        public idArticulo:number,
        public cantida:number,
        public modelo:string,
        public nombre:string,
        public precio:number,
        public descuento:number,
        public idMon:number,
        public idIva:number,
        public ubicador:number
    ){}
}

//idU(listo),idC(listo),version(listo) idP(no esta),can(listo),idI(listo)
export class OrdMonedaCom {
    constructor(
        public idUsuario:number,
        public idMonedaOri:number,
        public idMonedaDes:number,
        public ValorMon:number
    ){}
}
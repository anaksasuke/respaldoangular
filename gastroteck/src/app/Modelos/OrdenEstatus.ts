export class OrdenEstatus {
    constructor(
        public idOrden:number,
        public idtipo:number,
        public version:number,
        public idUsuario:number,
        public idEstatus:number
    ){}
}
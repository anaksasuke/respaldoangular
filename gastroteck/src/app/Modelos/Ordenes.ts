export class Orden {
    constructor(
        public idTipoOrden:number,
        public idProveedor:number,
        public idUsuario:number,
        public ord:number,
        public version:number,
        public ref:string,
        public pie:string,
        public encabezado:string,
        public observaciones:string,
    ){}
}
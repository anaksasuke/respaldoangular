export class PagoEntradas {
    constructor(
        public idPago:number,
        public idEntrada:number,
        public version:number,
        public idBanco:number,
        public cuenta:number,
        public idProveedor:number,
        public nombProveedor:string,
        public bancoProveedor:number,
        public cuentaProveedor:number,
        public idMovimiento:number,
        public cheque:number,
        public importe:number,
        public restante:number,
        public ficha:string,
        public fecha:string,
        public idUsuario:number,
    ){}
}
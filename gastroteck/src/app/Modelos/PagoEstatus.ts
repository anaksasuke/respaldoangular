export class PagoEstatus {
    constructor(
        public idPago:number,
        public id:number,
        public version:number,
        public idEstatus:number,
        public idUsuario:number
    ){}
}
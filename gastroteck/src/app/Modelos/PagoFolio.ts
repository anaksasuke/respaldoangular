export class PagoFolio {
    constructor(
        public idPago:number,
        public id:number,
        public version:number,
        public idUsuario:number,
        public folio:string,
        public tipo:number
    ){}
}
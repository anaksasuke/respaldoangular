export class PagosFacturas {
    constructor(
        public idPago:number,
        public idCliente:number,
        public nombreCliente:string,
        public idFactura:number,
        public idMovimiento:number,
        public versionFacutra:number,
        public idBancoEmisor:number,
        public cuentaEmisora:number,
        public idBancoReceptor:number,
        public cuentaReceptora:number,
        public cheques:number,
        public fichas:number,
        public importe:number,
        public restante:number,
        public fecha:string,
        public idUsuario:number
    ){}
}
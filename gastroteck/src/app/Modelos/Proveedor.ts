export class Proveedor {
    constructor(
        public idProv:number,
        public idNprov:number,
        public idUsu:number,
        public pais:number,
        public estado:number,
        public ciudad:number,
        public tipo:number,
        public rfc:string,
        public nombre:string,
        public razon:string,
        public calle:string,
        public numero:number,
        public colonia:string,
        public cp:number        
    ){}
}
export class Proyecto {
    constructor(
        public idUsuario: number,
        public idProyecto: number,
        public idCatProyecto: number,
        public idEspProyecto: number,
        public nomProyecto: string,
        public descProyecto: string
    ){}
}
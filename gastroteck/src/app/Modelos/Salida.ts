export class Salidas {
    constructor(
        public idSalida:number,
        public version:number,
        public idCliente:number,
        public destino:string,
        public direccion:string,
        public referencia:string,
        public fechaSalida:string,
        public fechaEntregado:string,
        public tipoSalida:number,
        public idUsuario:number,
        public comentario:string,
        public observaciones:string,
        public pie:string
    ){}
}
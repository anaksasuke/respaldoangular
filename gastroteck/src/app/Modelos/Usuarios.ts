export class Usuarios {
    constructor(
        public idUser:number,
        public usuario:string,
        public contrasena:string,
        public curp:string,
        public nombre:string,
        public APaterno:string,
        public AMaterno:string,
        public correo:string,
        public idRol:number,
        public idStatus:number,
        public idUserAlt:number
    ){}
}
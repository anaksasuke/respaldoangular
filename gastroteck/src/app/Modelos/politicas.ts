export class Politicas {
    constructor(
        public idPolitica:number,
        public idCatPolitica:number,
        public politica:string,
        public descPolitica:string,
        public prioPolitica:string,
        public defPolitica:string,
        public idUsuario:number
    ){}
}
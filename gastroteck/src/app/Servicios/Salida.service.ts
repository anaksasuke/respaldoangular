import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { dominio } from './../util.service';

@Injectable({
    providedIn: 'root'
})

export class SalidaService {
    url = `${dominio}index.php/Salidas/`;

    constructor(private http:HttpClient) {}
    Clientes(){
      return this.http.get(`${this.url}Cliente`);
    }

    TipoSalida(){
      return this.http.get(`${this.url}TipoSalida`);
    }

    Entrada(idCliente){
      return this.http.get(`${this.url}Entrada/${idCliente}`);
    }

    RellenarTabla(entrada){
      return this.http.post(`${this.url}CargarEntrada`,JSON.stringify(entrada));
    }

    TablaArticulo(idUsuario){
      return this.http.get(`${this.url}TablaArticulo/${idUsuario}`);
    }

    EditarArticulo(articulo){
      return this.http.post(`${this.url}EditarArticulo`,JSON.stringify(articulo));
    }

    EliminarArticulo(articulo){
      return this.http.post(`${this.url}EliminarArticulo`,JSON.stringify(articulo));
    }

    GuardarSalida(salida){
      return this.http.post(`${this.url}GuardarSalida`,JSON.stringify(salida));
    }

    Salidas(){
      return this.http.get(`${this.url}Salida`);
    }

    CargarSalida(idUsuario,idSalida,version){
      return this.http.get(`${this.url}CargarSalida/${idUsuario}/${idSalida}/${version}`);
    }

    ActualizarSalida(salida){
      return this.http.post(`${this.url}ActualizarSalida`,JSON.stringify(salida));
    }

    Estatus(){
      return this.http.get(`${this.url}EstatusVista`);
    }

    CambiaEstatus(estatus){
      return this.http.post(`${this.url}Estatus`,JSON.stringify(estatus));
    }

    EliminarSalida(salida){
      return this.http.post(`${this.url}SalidaEliminar`,JSON.stringify(salida));
    }

  }

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { dominio } from './../util.service';

@Injectable({
  providedIn: 'root'
})
export class CatalagoService {
  public url = `${dominio}Catalago/`;
  public proveedor = `${dominio}Proveedor/`;
  public config = `${dominio}index.php/Configuraciones/`;

  constructor(public http:HttpClient) { }

  Clasificaciones(){
    return this.http.get(`${this.url}Clasificaciones`);
  }  

  Categoria(idClasificacion){
    return this.http.get(`${this.url}Categorias/${idClasificacion}`);
  }

  Marcas(){
    return this.http.get(`${this.url}Marcas`);
  }

  Unidades(){
    return this.http.get(`${this.url}Unidades`);
  }

  Departamentos(){
    return this.http.get(`${this.url}Departamentos`);
  }

  Grupos(idDepartamento){
    return this.http.get(`${this.url}Grupos/${idDepartamento}`);
  }

  Lineas(idDepartamento,idGrupo){
    return this.http.get(`${this.url}Lineas/${idDepartamento}/${idGrupo}`);
  }

  Plomerias(){
    return this.http.get(`${this.url}Plomerias`);
  }

  EnergiaCalor(){
    return this.http.get(`${this.url}EnergiasCalor`);
  }

  EnergiaElectrica(){
    return this.http.get(`${this.url}EnergiasElectricas`);
  }

  Guardar(catalago){
    return this.http.post(`${this.url}CatalagoCrear`,JSON.stringify(catalago));
  }

  Editar(catalago){
    return this.http.post(`${this.url}CatalagoEditar`,JSON.stringify(catalago));
  }

  Catalagos(){
    return this.http.get(`${this.url}Catalagos`);
  }

  ArticulosDesctivados(){
    return this.http.get(`${this.url}CatalagoDesactivado`);
  }

  ObtenerCatalago(idArticulo){
    return this.http.get(`${this.url}ObtenerDatos/${idArticulo}`);
  }

  CatalogoEliminar(catalago){
    return this.http.post(`${this.url}CatalagoEliminar`,JSON.stringify(catalago));
  }

  Desactivar(estatus){
    return this.http.post(`${this.url}DesactivarArticulo`,JSON.stringify(estatus));
  }

  Activar(estatus){
    return this.http.post(`${this.url}ActivarArticulos`,JSON.stringify(estatus));
  }

  ArticuloProveedores(){
    return this.http.get(`${this.url}ArticuloProveedores`);
  }

  ArticuloProveedorEliminar(articuloproveedor){
    return this.http.post(`${this.url}ArticuloProveedorEliminar`, JSON.stringify(articuloproveedor));
  }

  Proveedores(){
    return this.http.get(`${this.proveedor}Proveedores`);
  }

  Monedas(){
    return this.http.get(`${this.config}Monedas`);
  }

  ArticuloProveedorEditar(articuloproveedor){
    return this.http.post(`${this.url}ArticuloProveedorEditar`, JSON.stringify(articuloproveedor));
  }

  ArticuloProveedorCrear(articuloproveedor){
    return this.http.post(`${this.url}ArticuloProveedorCrear`, JSON.stringify(articuloproveedor));
  }

}

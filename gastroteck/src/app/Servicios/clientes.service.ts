import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { dominio } from './../util.service';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {
  public url = `${dominio}index.php/Cliente/`;
  public urlConfig = `${dominio}index.php/Configuraciones/`;
  constructor(private http:HttpClient) { }

  Cliente(){
    return this.http.get(`${this.url}Clientes`);
  }

  tipoCliente(){
    return this.http.get(`${this.url}TiposClientes`);
  }

  DatosCliente(idCliente){
    return this.http.post(`${this.url}ClientesDatos/${idCliente}`,JSON.stringify(idCliente));
  }

  ClienteCrear(cliente){
    return this.http.post(`${this.url}ClienteCrear`,JSON.stringify(cliente));
  }

  ClienteEditar(cliente){
    return this.http.post(`${this.url}ClienteEditar`,JSON.stringify(cliente));
  }

  RazonDescarte(){
    return this.http.get(`${this.url}RDescarte`);
  }

  Descarte(descarte){
    return this.http.post(`${this.url}Descartar`,JSON.stringify(descarte));
  }

  ClientesDescartados(){
    return this.http.get(`${this.url}ClientesDescartados`);
  }

  Reactivar(cliente){
    return this.http.post(`${this.url}ReactivarCli`,JSON.stringify(cliente));
  }

  Contactos(idCliente){
    return this.http.get(`${this.url}ContactosCliente/${idCliente}`);
  }

  ContactosCrear(contactos){
    return this.http.post(`${this.url}ContactosCrear`,JSON.stringify(contactos));
  }

  ContactoEditar(contactos){
    return this.http.post(`${this.url}ContactosEditar`,JSON.stringify(contactos));
  }

  ContactoEliminar(contactos){
    return this.http.post(`${this.url}ContactosEliminar`,JSON.stringify(contactos));
  }

  TipoContactos(){
    return this.http.get(`${this.url}ContactosTipo`);
  }

  obtConAct(idCli,idCon){
    return this.http.get(`${this.url}ObtenerDatos/${idCli}/${idCon}`);
  }

  Giro(){
    return this.http.get(`${this.urlConfig}Giro`);
  }

  Especialidad(){
    return this.http.get(`${this.urlConfig}Especialidad`);
  }

  TipoPersonas(){
    return this.http.get(`${this.urlConfig}PersonaTipos`);
  }

  Paises(){
    return this.http.get(`${this.urlConfig}Pais`);
  }

  EstadoFiltro(idPais){
    return this.http.get(`${this.urlConfig}FiltroEstado/${idPais}`);
  }

  CiudadFiltro(idEstado,idPais){
    return this.http.get(`${this.urlConfig}FiltroCiudad/${idEstado}/${idPais}`);
  }

}

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { dominio } from './../util.service';

@Injectable({
  providedIn: 'root'
})
export class ConfiguracionesService {
  datos;
  public url = `${dominio}index.php/Configuraciones/`;

  constructor(private http:HttpClient) { }

  Login(username,password){
    return this.http.get(`${this.url}Login/${username}/${password}`);
  }

  Paises(){
    return this.http.get(`${this.url}Pais`);
  }
  PaisCrearPrueba(pais){
    return this.http.post(`${this.url}PaisCrear`,JSON.stringify(pais));
  }

  PaisEditPrueba(pais){
    return this.http.post(`${this.url}PaisEditar`,JSON.stringify(pais));
  }

  PaisCrear(Pais){
    return this.http.post(`${this.url}PaisCrear`,JSON.stringify(Pais));
  }

  PaisEdit(Pais){
    return this.http.post(`${this.url}PaisEditar`,JSON.stringify(Pais));
  }

  PaisDelet(Pais){
    return this.http.post(`${this.url}PaisDelete`,JSON.stringify(Pais));
  }

  Estados(){
    return this.http.get(`${this.url}Estado`);
  }

  EstadosCrear(Estado){
    return this.http.post(`${this.url}EstadoCrear`,JSON.stringify(Estado));
  }

  EstadosEdit(Estado){
    return this.http.post(`${this.url}EstadoEditar`,JSON.stringify(Estado));
  }

  EstadosDelet(Estado){
    return this.http.post(`${this.url}EstadoDelete`,JSON.stringify(Estado));
  }

  EstadoFiltro(idPais){
    return this.http.get(`${this.url}FiltroEstado/${idPais}`);
  }

  CiudadFiltro(idEstado,idPais){
    return this.http.get(`${this.url}FiltroCiudad/${idEstado}/${idPais}`);
  }

  Ciudad(){
    return this.http.get(`${this.url}Ciudad`);
  }

  CiudadCrear(ciudad){
    return this.http.post(`${this.url}CiudadCrear`,JSON.stringify(ciudad));
  }

  CiudadEditar(ciudad){
    return this.http.post(`${this.url}CiudadEditar`,JSON.stringify(ciudad));
  }

  CiudadDelet(ciudad){
    return this.http.post(`${this.url}CuidadDelet`,JSON.stringify(ciudad));
  }
  Tuberias(){
    return this.http.get(`${this.url}Tuberia`);
  }

  TuberiaCrear(Tuberia){
    return this.http.post(`${this.url}TuberiaCrear`,JSON.stringify(Tuberia));
  }
  
  TuberiaEditar(Tuberia){
    return this.http.post(`${this.url}TuberiaEditar`,JSON.stringify(Tuberia));
  }

  TuberiaDelet(Tuberia){
    return this.http.post(`${this.url}TuberiaDelet`,JSON.stringify(Tuberia));
  }

  Electricidad(){
    return this.http.get(`${this.url}Electricidad`);
  }

  ElectricidadCrear(Electricidad){
    return this.http.post(`${this.url}ElectricidadCrear`,JSON.stringify(Electricidad));
  }

  ElectricidadEdit(Electricidad){
    return this.http.post(`${this.url}ElectricidadEdit`,JSON.stringify(Electricidad));
  }

  ElectricidadDelet(Electricidad){
    return this.http.post(`${this.url}ElectricidadDelet`,JSON.stringify(Electricidad));
  }

  Calor(){
    return this.http.get(`${this.url}Calor`);
  }

  CalorCrear(Gases){
    return this.http.post(`${this.url}CalorCrear`,JSON.stringify(Gases));
  }

  CalorEditar(Gases){
    return this.http.post(`${this.url}CalorEditar`,JSON.stringify(Gases));
  }

  CalorDelet(Gases){
    return this.http.post(`${this.url}CalorDelet`,JSON.stringify(Gases));
  }
  
  Banco(){
    return this.http.get(`${this.url}Banco`);
  }

  BancoTipo(){
    return this.http.get(`${this.url}BancoTipo`);
  }
    
  BancoCrear(Banco){
    return this.http.post(`${this.url}BancoCrear`,JSON.stringify(Banco));
  }
  
  BancoEditar(Banco){
    return this.http.post(`${this.url}BancoEditar`,JSON.stringify(Banco));
  }
    
  BancoDelet(Banco){
    return this.http.post(`${this.url}BancoDelet`,JSON.stringify(Banco));
  }

  Cuentab(idCliente){
    return this.http.get(`${this.url}CuentaBancaria/${idCliente}`);
  }

  CuentabCrear(Cuentab){
    return this.http.post(`${this.url}CuentaBancariaCrear`,JSON.stringify(Cuentab));
  }

  CuentabEditar(Cuentab){
    return this.http.post(`${this.url}CuentaBancariaEditar`,JSON.stringify(Cuentab));
  }

  CuentabDelet(Cuentab){
    return this.http.post(`${this.url}CuentaBancariaDelet`,JSON.stringify(Cuentab));
  }


  Marcas(){
    return this.http.get(`${this.url}Marcas`);
  }
  
  MarcasCrear(Marcas){
    return this.http.post(`${this.url}MarcasCrear`,JSON.stringify(Marcas));
  }
  
  MarcasEditar(Marcas){
    return this.http.post(`${this.url}MarcasEditar`,JSON.stringify(Marcas));
  }
  
  MarcasDelet(Marcas){
    return this.http.post(`${this.url}MarcasDelet`,JSON.stringify(Marcas));
  }

  Medida(){
      return this.http.get(`${this.url}Medida`);
  }
    
  MedidaCrear(Medida){
    return this.http.post(`${this.url}MedidaCrear`,JSON.stringify(Medida));
  }

  MedidaEditar(Medida){
    return this.http.post(`${this.url}MedidaEditar`,JSON.stringify(Medida));
  }
  
    MedidaDelet(Medida){
      return this.http.post(`${this.url}MedidaDelet`,JSON.stringify(Medida));
    }
    Areas(){
      return this.http.get(`${this.url}Areas`);
    }
  
    AreasCrear(area){
      return this.http.post(`${this.url}AreasCrear`,JSON.stringify(area));
    }
  
    AreasEditar(area){
      return this.http.post(`${this.url}AreasEditar`,JSON.stringify(area));
    }
  
    AreasDelet(area){
      return this.http.post(`${this.url}AreaDelet`,JSON.stringify(area));
    }
    Categorias(){
      return this.http.get(`${this.url}Categorias`);
    }
  
    CategoriasCrear(categorias){
      return this.http.post(`${this.url}CategoriasCrear`,JSON.stringify(categorias));
    }
    
    CategoriasEditar(categorias){
      return this.http.post(`${this.url}CategoriasEditar`,JSON.stringify(categorias))
    }
  
    CategoriasDelet(categorias){
      return this.http.post(`${this.url}CategoriaDelet`,JSON.stringify(categorias));
    }    
    Departamento(){
      return this.http.get(`${this.url}Departamento`);
    }
    
    DepartamentoCrear(departamento){
      return this.http.post(`${this.url}DepartamentoCrear`,JSON.stringify(departamento));
    }
  
    DepartamentoEditar(departamento){
      return this.http.post(`${this.url}DepartamentoEditar`,JSON.stringify(departamento));
    }
  
    DepartamentoDelet(departamento){
      return this.http.post(`${this.url}DepartamentoDelet`,JSON.stringify(departamento));
    }
    Grupos(){
      return this.http.get(`${this.url}Grupos`);
    }
  
    GruposCrear(grupo){
      return this.http.post(`${this.url}GruposCrear`,JSON.stringify(grupo))
    }
  
    GruposEditar(grupo){
      return this.http.post(`${this.url}GruposEditar`,JSON.stringify(grupo));
    }

    GruposDel(grupo){
      return this.http.post(`${this.url}GruposEliminar`,JSON.stringify(grupo));
    }

    Lineas(){
      return this.http.get(`${this.url}Lineas`);
    }
  
    LineasCrear(linea){
      return this.http.post(`${this.url}LineasCrear`,JSON.stringify(linea));
    }
  
    LineasEditar(linea){
      return this.http.post(`${this.url}LineasEditar`,JSON.stringify(linea));
    }
  
    LineasDelete(linea){
      return this.http.post(`${this.url}LineaDelet`,JSON.stringify(linea));
    }
    ObtenerGrupo(idDepartamento){
      return this.http.post(`${this.url}SubLinea/${idDepartamento}`,JSON.stringify(idDepartamento));
    }
    
    Monedas(){
      return this.http.get(`${this.url}Monedas`);
    }
  
    MonedasCrear(monedas){
      return this.http.post(`${this.url}MonedaCrear`, JSON.stringify(monedas));
    }
  
    MonedasEditar(monedas){
      return this.http.post(`${this.url}MonedaEditar`, JSON.stringify(monedas));
    }
  
    MonedasEliminar(monedas){
      return this.http.post(`${this.url}MonedaEliminar`, JSON.stringify(monedas));
    }
    Conversiones(){
      return this.http.get(`${this.url}Conversiones`); //para llenar de una vista  
    }
  
    ConversionesMoneda(conversiones){ //nombre del servicio
      return this.http.post(`${this.url}ConversionMoneda/${conversiones}`, JSON.stringify(conversiones)); //para llenar un dropdownlist 
    }
  
    ConversionesCrear(conversiones){
      return this.http.post(`${this.url}ConversionCrear`, JSON.stringify(conversiones));
    
    }
  
    ConversionesEditar(conversiones){
      return this.http.post(`${this.url}ConversionEditar`, JSON.stringify(conversiones));
    }
  
    ConversionesEliminar(conversiones){
      return this.http.post(`${this.url}ConversionEliminar`, JSON.stringify(conversiones));
    }
    Giro(){
      return this.http.get(`${this.url}Giro`);
    }
  
    Especialidad(){
      return this.http.get(`${this.url}Especialidad`);
    }

    Ivas(){
      return this.http.get(`${this.url}Ivas`);
    }
  
    IvaCrear(iva){
      return this.http.post(`${this.url}IvaCrear`, JSON.stringify(iva));
    }
  
    IvasEditar(iva){
      return this.http.post(`${this.url}IvaEditar`, JSON.stringify(iva));
    }
  
    IvasEliminar(iva){
      return this.http.post(`${this.url}IvaEliminar`, JSON.stringify(iva));
    }

    Excepciones(){
      return this.http.get(`${this.url}Excepciones`);
    }

    ExcepcionCrear(excepcion){
      return this.http.post(`${this.url}ExcepcionCrear`, JSON.stringify(excepcion));
    }

    ExcepcionEditar(excepcion){
      return this.http.post(`${this.url}ExcepcionEditar`, JSON.stringify(excepcion));
    }

    ExcepcionEliminar(excepcion){
      return this.http.post(`${this.url}ExcepcionEliminar`, JSON.stringify(excepcion));
    }

    categoriasArticulo(){
      return this.http.get(`${this.url}TipoCategoria`);
    }
  
    categoriasArticuloCrear(categoria){
      return this.http.post(`${this.url}TipoCategoriaCrear`,JSON.stringify(categoria))
    }
  
    categoriasArticuloEditar(categoria){
      return this.http.post(`${this.url}TipoCategoriaEditar`,JSON.stringify(categoria))
    }

    categoriasArticuloEliminar(categoria){
      return this.http.post(`${this.url}TipoCategoriaEliminar`,JSON.stringify(categoria));
    }

    Politicas(){
      return this.http.get(`${this.url}Politicas`);
    }
    
    PoliticasCrear(politicas){
      return this.http.post(`${this.url}PoliticasCrear`, JSON.stringify(politicas));
    }

    PoliticasEditar(politicas){
      return this.http.post(`${this.url}PoliticasEditar`, JSON.stringify(politicas));
    }

    PoliticasEliminar(politicas){
      return this.http.post(`${this.url}PoliticasEliminar`, JSON.stringify(politicas));
    }

    Aclasificaciones(){
      return this.http.get(`${this.url}Aclasificaciones`);
    }
    
    AclasificacionesC(Aclasif){
      return this.http.post(`${this.url}AclasificacionesC`,JSON.stringify(Aclasif));
    }
  
    AclasificacionesU(Aclasif){
      return this.http.post(`${this.url}AclasificacionesU`,JSON.stringify(Aclasif));
    }
  
    AclasificacionesD(Aclasif){
      return this.http.post(`${this.url}AclasificacionesD`,JSON.stringify(Aclasif));
    }

    Vincular(){
      return this.http.get(`${this.url}Link`);
    }

    VincularCrear(Vincular){
      return this.http.post(`${this.url}CreateLink`, JSON.stringify(Vincular));
    }

    VincularEditar(Vincular){
      return this.http.post(`${this.url}EditLink`, JSON.stringify(Vincular));
    }

    VincularEliminar(Vincular){
      return this.http.post(`${this.url}DeleteLink`, JSON.stringify(Vincular));
    }
    
    showDatos(){
      return this.datos;
    }
  
}

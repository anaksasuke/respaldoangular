import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { dominio } from './../util.service';

@Injectable({
  providedIn: 'root'
})
export class CotizacionesService {
  url=`${dominio}index.php/Cotizacion/`;

  constructor(private http:HttpClient) { }

  AutoCliente(){
    return this.http.get(`${this.url}BusquedaCliente`);
  }

  AutoProyecto(idCliente){
    return this.http.get(`${this.url}BusquedaProyecto/${idCliente}`);
  }

  AutoAgente(){
    return this.http.get(`${this.url}BusquedaAgente`);
  }

  Areas(){
    return this.http.get(`${this.url}AreasCotizacion`);
  }

  Articulos(){
    return this.http.get(`${this.url}BusquedaArticuloMol`);
  }

  Proveedores(idArt){
    return this.http.get(`${this.url}Proveedores/${idArt}`);
  }

  IVA(){
    return this.http.get(`${this.url}Iva`);
  }

  ProveedorArt(idProveedor,idArticulo){
    return this.http.get(`${this.url}ObtenerDatos/${idProveedor}/${idArticulo}`);
  }

  Moneda(){
    return this.http.get(`${this.url}Moneda`);
  }

  AgregarArt(cotArticulo){
    return this.http.post(`${this.url}AgregarArt`,JSON.stringify(cotArticulo));
  }

  ActualizarArt(cotArticulo){
    return this.http.post(`${this.url}EditarArt`,JSON.stringify(cotArticulo));
  }

  EliminarArt(cotArticulo){
    return this.http.post(`${this.url}EliminarArt`,JSON.stringify(cotArticulo));
  }

  TablaArt(idUsuario){
    return this.http.get(`${this.url}CargarTabla/${idUsuario}`);
  }

  CargarExcepciones(){
    return this.http.get(`${this.url}CargarExcepciones`);
  }

  AgregarExcep(cotExcepcion){
    return this.http.post(`${this.url}AgregarExcepcion`,JSON.stringify(cotExcepcion));
  }

  TablaExcep(idUsuario){
    return this.http.get(`${this.url}CargarTablaExcep/${idUsuario}`);
  }

  ActualizarExcep(cotExcepcion){
    return this.http.post(`${this.url}EditarExcep`,JSON.stringify(cotExcepcion));
  }

  EliminarExcep(cotExcepcion){
    return this.http.post(`${this.url}ElimnarExcep`,JSON.stringify(cotExcepcion));
  }

  CargarPloticas(idUser,idCot,Version){
    return this.http.get(`${this.url}PoliticaDef/${idUser}/${idCot}/${Version}`);
  }

  politicas(idusuario){
    return this.http.get(`${this.url}Politicas/${idusuario}`);
  }

  PoliticasActualizar(){
    return this.http.get(`${this.url}PoliticasCompletas`);
  }
  
  TablaPol(idUsuario){
    return this.http.get(`${this.url}CargarTablaPol/${idUsuario}`);
  }

  GuardarPoliticas(politica){
    return this.http.post(`${this.url}AgregarPol`,JSON.stringify(politica));
  }

  ActualizarPoliticas(politica){
    return this.http.post(`${this.url}EditarPol`,JSON.stringify(politica));
  }

  EliminarPolitica(politica){
    return this.http.post(`${this.url}EliminarPol`,JSON.stringify(politica));
  }

  GuardarCotizacion(cotizacion){
    return this.http.post(`${this.url}GuardarCot`,JSON.stringify(cotizacion));
  }

  Cotizaciones(){
    return this.http.get(`${this.url}Cotizaciones`);
  }

  TablaArtActulizado(idCot,Vers,idU){
    return this.http.get(`${this.url}TablaArtEdit/${idCot}/${Vers}/${idU}`);
  }

  TablaExpActualizado(idCot,Vers,idU){
    return this.http.get(`${this.url}TablaExcepEdit/${idCot}/${Vers}/${idU}`);
  }

  CotizacionEditar(cotizacion){
    return this.http.post(`${this.url}ActualizarCot`,JSON.stringify(cotizacion));
  }

  CotizacionMoneda(idUsuario){
    return this.http.get(`${this.url}Currency/${idUsuario}`);
  }

  SpCotMonedas(idUsuario,idMoneda){
    return this.http.get(`${this.url}spMoneda/${idUsuario}/${idMoneda}`);
  }

  ValorMoneda(idOrigen,idDestiono){
    return this.http.get(`${this.url}CMoneda/${idOrigen}/${idDestiono}`);
  }

  Convertidor(cotMonedaCom){
    return this.http.post(`${this.url}CambioMoneda`,JSON.stringify(cotMonedaCom));
  }

  Estatus(){
    return this.http.get(`${this.url}Estatus`);
  }

  CambioEst(estatus){
    return this.http.post(`${this.url}CambioEstatus`,JSON.stringify(estatus));
  }


  Versiones(idCot){
    return this.http.get(`${this.url}ListaVersiones/${idCot}`);
  }

  EliminarCotizacion(cotizacion){
    return this.http.post(`${this.url}EliminarCotizacion`,JSON.stringify(cotizacion));
  }

  PdfVista(idCotizacion, versCotizacion){
    return this.http.get(`${this.url}PdfV/${idCotizacion}/${versCotizacion}`);
  }

  PdfTotal(idCotizacion, versCotizacion){
    return this.http.get(`${this.url}PdfTotales/${idCotizacion}/${versCotizacion}`);
  }

  PdfArticulosT(idUsuario){
    return this.http.get(`${this.url}PdfArticulosTotales/${idUsuario}`);
  }

  PdfAreasT(idUsuario){
    return this.http.get(`${this.url}PdfAreasTotales/${idUsuario}`);
  }

  PdfMarcas(idUsuario){
    return this.http.get(`${this.url}PdfMarcasArticulo/${idUsuario}`);
  }

  PdfArticulos(idUsuario){
    return this.http.get(`${this.url}PdfArticulos/${idUsuario}`);
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { dominio } from './../util.service';

@Injectable({
  providedIn: 'root'
})
export class EntradasService {
  url = `${dominio}index.php/Entradas/`;
  constructor(private http:HttpClient) { }

  Proveedores(){
    return this.http.get(`${this.url}Proveedores`);
  }

  CambioProveedor(idUsuario){
    return this.http.get(`${this.url}CambiarProveedor/${idUsuario}`);
  }

  Ordenes(idProveedor){
    return this.http.get(`${this.url}Ordenes/${idProveedor}`);
  }

  VistaArticulos(idUsuario){
    return this.http.get(`${this.url}VistaArticulos/${idUsuario}`);
  }

  RellenarTabla(entradaCarArt){
    return this.http.post(`${this.url}CargarArtOrd`,JSON.stringify(entradaCarArt));
  }

  ActualizarArt(entradaArt){
    return this.http.post(`${this.url}ActualizarArt`,JSON.stringify(entradaArt));
  }

  BorrarArt(entradaArt){
    return this.http.post(`${this.url}EliminarArt`,JSON.stringify(entradaArt));
  }

  Guardar(entrada){
    return this.http.post(`${this.url}GuardarEntrada`,JSON.stringify(entrada));
  }

  Entradas(){
    return this.http.get(`${this.url}Entrada`);
  }

  CargarEntradaArt(entrada){
    return this.http.post(`${this.url}CarArtEntrada`,JSON.stringify(entrada));
  }

  EditarEntrada(entrada){
    return this.http.post(`${this.url}EditarEntrada`,JSON.stringify(entrada));
  }

  EliminarEntradas(entrada){
    return this.http.post(`${this.url}EliminarEntrada`,JSON.stringify(entrada));
  }

  Estatus(){
    return this.http.get(`${this.url}Estatus`);
  }

  EstatusCambio(estatus){
    return this.http.post(`${this.url}Status`,JSON.stringify(estatus));
  }



}

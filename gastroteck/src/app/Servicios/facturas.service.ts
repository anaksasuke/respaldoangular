import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { dominio } from './../util.service';

@Injectable({
  providedIn: 'root'
})
export class FacturasService {
  url = `${dominio}index.php/Facturas/`;
  constructor(private http:HttpClient) { }

  Clientes(){
    return this.http.get(`${this.url}Clientes`);
  }

  CambioCliente(idUsuario){
    return this.http.post(`${this.url}CambioCliente`,JSON.stringify(idUsuario));
  }

  FormaPago(){
    return this.http.get(`${this.url}FormaPago`);
  }

  CFDI(){
    return this.http.get(`${this.url}CFDI`);
  }

  TipoFactura(){
    return this.http.get(`${this.url}TipoFactura`);
  }

  CFDIRelacional(){
    return this.http.get(`${this.url}CFDIRelaccionado`);
  }

  Areas(){
    return this.http.get(`${this.url}Areas`);
  }

  CodSat(){
    return this.http.get(`${this.url}ArticuloSat`);
  }  

  ArticuloPro(idArticulo){
    return this.http.get(`${this.url}ArticuloPro/${idArticulo}`);
  }

  DatosArticulo($idPro,$idArt){
    return this.http.get(`${this.url}DatosArt/${$idPro}/${$idArt}`);
  }

  Monedas(){
    return this.http.get(`${this.url}Moneda`);
  }

  Ivas(){
    return this.http.get(`${this.url}Iva`);
  }

  CotizacionCli(idCliente){
    return this.http.get(`${this.url}Cotizaciones/${idCliente}`);
  }

  RellenarTable(facturaCarArt){
    return this.http.post(`${this.url}CargarArticulos`,JSON.stringify(facturaCarArt));
  }

  FacturaAnticipo(facturaCarArt){
    return this.http.post(`${this.url}CargarTotales`,JSON.stringify(facturaCarArt));
  }

  ConteoMoneda(idcotizacion,version){
    return this.http.get(`${this.url}ConteoMoneda/${idcotizacion}/${version}`);
  }

  Finiquito(factura){
    return this.http.post(`${this.url}Finiquito`,JSON.stringify(factura));
  }

  MondaFactura(idUsuario){
    return this.http.get(`${this.url}MonedaFactura/${idUsuario}`);
  }

  BorrarMoneda(moneda){
    return this.http.post(`${this.url}BorrarMoneda`,JSON.stringify(moneda));
  }

  OtraMoneda(moneda){
    return this.http.post(`${this.url}OtraMoneda`,JSON.stringify(moneda));
  }

  ValorConversion(idMoneda1,idMoneda2){
    return this.http.get(`${this.url}ValorConversion/${idMoneda1}/${idMoneda2}`);
  }

  Conversion(moneda){
    return this.http.post(`${this.url}ConvertirMoneda`,JSON.stringify(moneda));
  }

  CotizacionFactura(){
    return this.http.get(`${this.url}CotizacionFactura`);
  }

  BorrarCotizacion(cotizacion){
    return this.http.post(`${this.url}BorrarCotizacion`,JSON.stringify(cotizacion));
  }

  TablaArticulos(idUsuario){
    return this.http.get(`${this.url}TablaArticulos/${idUsuario}`);
  }

  GuardarFactura(factura){
    return this.http.post(`${this.url}GuardarFactura`,JSON.stringify(factura));
  }

  Facturas(){
    return this.http.get(`${this.url}Factura`);
  }

  BorrarFacturas(facEliminar){
    return this.http.post(`${this.url}FacturaBorrar`,JSON.stringify(facEliminar));
  }

  AgregarArticulo(Articulo){
    return this.http.post(`${this.url}AgregarArticulo`,JSON.stringify(Articulo));
  }

  EditarArticulo(Articulo){
    return this.http.post(`${this.url}EditarArticulo`,JSON.stringify(Articulo));
  }

  EliminarArticulo(Articulo){
    return this.http.post(`${this.url}EliminarArticulo`,JSON.stringify(Articulo));
  }

  CargarArticulos(FacturaArt){
    return this.http.post(`${this.url}CargarFacturaArt`,JSON.stringify(FacturaArt));
  }

  EditarFactura(factura){
    return this.http.post(`${this.url}EditarFactura`,JSON.stringify(factura));
  }

  Estatus(){
    return this.http.get(`${this.url}Estatus`);
  }

  CambiarEstatus(estatus){
    return this.http.post(`${this.url}CambiarEstatus`,JSON.stringify(estatus));
  }

  AgregarFolio(folio){
    return this.http.post(`${this.url}AgregarFolio`,JSON.stringify(folio));
  }

}

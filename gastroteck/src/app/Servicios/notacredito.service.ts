import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { dominio } from './../util.service';

@Injectable({
    providedIn: 'root'
})

export class NotaCreditoService {
  url = `${dominio}index.php/NotaCredito/`;

  constructor(private http:HttpClient){}

  Clientes(){
    return this.http.get(`${this.url}Clientes`);
  }

  CambioCliente(idUsuario){
    return this.http.post(`${this.url}CambioCliente`,JSON.stringify(idUsuario));
  }

  Facturas(idCliente){
    return this.http.get(`${this.url}Facturas/${idCliente}`);
  }
   
  FormaPago(){
    return this.http.get(`${this.url}FormaPago`);
  }
  
  CFDI(){
    return this.http.get(`${this.url}CFDI`);
  }

  CFDIRelacional(){
    return this.http.get(`${this.url}CFDIRelaccionado`);
  }
  
  RellenarTabla(factura){
    return this.http.post(`${this.url}CargarTabla`,JSON.stringify(factura));
  }

  Tabla(idUsuario){
    return this.http.get(`${this.url}TablaArticulos/${idUsuario}`);
  }

  DatosFactura(idfactura, verfactura){
    return this.http.get(`${this.url}DatosFactura/${idfactura}/${verfactura}`);
  }

  Ivas(){
    return this.http.get(`${this.url}Iva`);
  }

  Monedas(){
    return this.http.get(`${this.url}Moneda`);
  }

  ActualizarArt(articulo){
    return this.http.post(`${this.url}AgregarArticulo`,JSON.stringify(articulo));
  }

  GuardarNotaCred(notacredito){
    return this.http.post(`${this.url}GuardarNotaCredito`,JSON.stringify(notacredito));
  }

  NotaCredito(){
    return this.http.get(`${this.url}NotaCredito`);
  }
   
  ActualizarNotaCredito(actualizar){
    return this.http.post(`${this.url}EditarNotaCredito`,JSON.stringify(actualizar));
  }

  NotaCreditoBorrar(nceliminar){
    return this.http.post(`${this.url}NotaCreditoBorrar`,JSON.stringify(nceliminar));
  }

  DeleteArticulo(deleteart){
    return this.http.post(`${this.url}BorrarArticulo`,JSON.stringify(deleteart));
  }
  
  CargarArticulos(notaCredito){
    return this.http.post(`${this.url}CargarNotaCredito`,JSON.stringify(notaCredito));
  }

  Estatus(){
    return this.http.get(`${this.url}Estatus`);
  }

  CambiarEstatus(estatus){
    return this.http.post(`${this.url}CambiarEstatus`,JSON.stringify(estatus));
  }

  NotaCreditoDescartar(descartar){
    return this.http.post(`${this.url}Descartar`,JSON.stringify(descartar));
  }

  Verversion(idNotaCredito){
    return this.http.get(`${this.url}Verversiones/${idNotaCredito}`);
  }
 
}
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { dominio } from './../util.service';


@Injectable({
  providedIn: 'root'
})
export class OrdenesService {
  url = `${dominio}index.php/Ordenes/`;
  constructor(private http:HttpClient) { }

  TipoOrden(){
    return this.http.get(`${this.url}TipoOrden`);
  }

  Proveedor(){
    return this.http.get(`${this.url}Proveedores`);
  }

  AutoCot(idProveedor){
    return this.http.get(`${this.url}Cotizaciones/${idProveedor}`);
  }

  CargarArticulos(idU,idProv,idCot,version){
    return this.http.get(`${this.url}CargarArticulos/${idU}/${idProv}/${idCot}/${version}`);
  }

  CargarTabla(idUsuario){
    return this.http.get(`${this.url}OrdenArticulos/${idUsuario}`);
  }

  EliminarArt(ArticuloCot){
    return this.http.post(`${this.url}ElimarArtOrden`,JSON.stringify(ArticuloCot));
  }

  EditarArt(ArticuloCot){
    return this.http.post(`${this.url}EditarArtOrden`,JSON.stringify(ArticuloCot));
  }

  GuardarArt(Articulo){
    return this.http.post(`${this.url}GuardarArtOrden`,JSON.stringify(Articulo));
  }

  Monedas(){
    return this.http.get(`${this.url}Monedas`);
  }

  IVA(){
    return this.http.get(`${this.url}Ivas`);
  }

  Areas(){
    return this.http.get(`${this.url}Areas`);
  }

  Ordenes(){
    return this.http.get(`${this.url}Orden`);
  }

  Articulo(idProveedor){
    return this.http.get(`${this.url}Articulos/${idProveedor}`);
  }

  DatosArticulo(idProveedor,idArticulo){
    return this.http.get(`${this.url}/ObtenerDatos/${idProveedor}/${idArticulo}`);
  }

  GuardarOrden(orden){
    return this.http.post(`${this.url}OrdenGuardar`,JSON.stringify(orden));
  }

  Estatus(){
    return this.http.get(`${this.url}Estatus`);
  }

  GuardarEstatus(estatus){
    return this.http.post(`${this.url}CambiarEstatus`,JSON.stringify(estatus));
  }

  CargarArticulosOrden(orden){
    return this.http.post(`${this.url}CargarArticulosOrden`,JSON.stringify(orden));
  }

  ActualizarOrden(orden){
    return this.http.post(`${this.url}OrdenActualizar`,JSON.stringify(orden));
  }

  CambioMoneda(idUsuario){
    return this.http.get(`${this.url}Currency/${idUsuario}`);
  }

  MonedaFaltante(idUsuario,idMoneda){
    return this.http.get(`${this.url}spCambioMoneda/${idUsuario}/${idMoneda}`);
  }

  CambiarMoneda(moneda){
    return this.http.post(`${this.url}CambioMoneda`,JSON.stringify(moneda));
  }

  Borrar(orden){
    return this.http.post(`${this.url}BorrarOrden`,JSON.stringify(orden));
  }

  Descartar(orden){
    return this.http.post(`${this.url}DescartarOrden`,JSON.stringify(orden));
  }

  CambioProveedor(idUsuario){
    return this.http.get(`${this.url}CambioProveedor/${idUsuario}`);
  }

  OrdVerVersion(idorden){
    return this.http.get(`${this.url}OrdenVerVersion/${idorden}`);
  }


}

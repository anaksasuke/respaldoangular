import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { dominio } from './../util.service';

@Injectable({
  providedIn: 'root'
})
export class PagosService {
  url = `${dominio}Pagos/`;
  constructor(private http:HttpClient) { }

  Clientes(){
    return this.http.get(`${this.url}Clientes`);
  }

  Facturas(idCliente){
    return this.http.get(`${this.url}Facturas/${idCliente}`);
  }

  Movimientos(){
    return this.http.get(`${this.url}Movimiento`);
  }

  BancoEmisor(idCliente){
    return this.http.get(`${this.url}BancoEmisor/${idCliente}`);
  }

  CuentaEmisora(idCliente,idBanco){
    return this.http.get(`${this.url}CuentaEmisora/${idCliente}/${idBanco}`);
  }

  BancoReceptor(){
    return this.http.get(`${this.url}BancoReceptor`);
  }

  CuentaReceptora(idBanco){
    return this.http.get(`${this.url}CuentaReceptora/${idBanco}`);
  }

  PagosEntrada(){
    return this.http.get(`${this.url}PagosEntradas`);
  }

  PagosFactura(){
    return this.http.get(`${this.url}PagosFacturas`);
  }

  GuardarPagoFactura(pago){
    return this.http.post(`${this.url}GuardarPagoFactura`,JSON.stringify(pago));
  }

  ActualizarPago(pago){
    return this.http.post(`${this.url}ActualizarPagos`,JSON.stringify(pago));
  }

  EstatusPago(pagoEstatus){
    return this.http.post(`${this.url}AprobarPago`,JSON.stringify(pagoEstatus));
  }

  BorrarPago(pago){
    return this.http.post(`${this.url}BorrarPago`,JSON.stringify(pago));
  }

  Proveedores(){
    return this.http.get(`${this.url}Proveedores`);
  }

  Entradas(idProveedor){
    return this.http.get(`${this.url}EntradaProveedor/${idProveedor}`);
  }

  BancoProveedor(idProveedor){
    return this.http.get(`${this.url}BancoProveedor/${idProveedor}`);
  }

  CuentaProveedor(idBanco,idProveedor){
    return this.http.get(`${this.url}CuentaProveedor/${idBanco}/${idProveedor}`);
  }

  GuardarPagoEnt(pagoEntrada){
    return this.http.post(`${this.url}GuardarPagoEntrada`,JSON.stringify(pagoEntrada));
  }

  ActualizarPagoEnt(pagoEntrada){
    return this.http.post(`${this.url}ActualizarPagoEntrada`,JSON.stringify(pagoEntrada));
  }

  EstatusEntrada(estatus){
    return this.http.post(`${this.url}EstatusEntrada`,JSON.stringify(estatus));
  }

  BorrarEntradaPago(pago){
    return this.http.post(`${this.url}BorrarPagoEntrada`,JSON.stringify(pago));
  }

  AgregarFolioFac(folio){
    return this.http.post(`${this.url}AgregarFolioFactura`,JSON.stringify(folio));
  }

  AgregarFolioEnt(folio){
    return this.http.post(`${this.url}AgregarFolioEntrada`,JSON.stringify(folio));
  }

}

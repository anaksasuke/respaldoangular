import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { dominio } from './../util.service';

@Injectable({
  providedIn: 'root'
})

export class ProveedorService{
  public proveedor = `${dominio}Proveedor/`;
  public config = `${dominio}index.php/Configuraciones/`;  
  constructor(private http:HttpClient){}
  
  Proveedores(){
    return this.http.get(`${this.proveedor}Proveedores`);
  }
    
  ProveedorCrear(proveedor){
    return this.http.post(`${this.proveedor}ProveedorCrear`, JSON.stringify(proveedor));
  }
    
  ProveedorDatos(proveedor){
    return this.http.post(`${this.proveedor}ProveedorDatos/${proveedor}`,JSON.stringify(proveedor));
  }
    
  ProveedorEditar(proveedor){
    return this.http.post(`${this.proveedor}ProveedorEditar`, JSON.stringify(proveedor));
  }
    
  ProveedorEliminar(proveedor){
    return this.http.post(`${this.proveedor}ProveedorEliminar`, JSON.stringify(proveedor));
  }
    
  ProveedorContactos(idProveedor){
    return this.http.get(`${this.proveedor}Contactos/${idProveedor}`);
  }
    
  TipoContactosPro(){
    return this.http.get(`${this.proveedor}TipoContacto`);
  }
    
  AgregarContProveedor(contProveedor){
    return this.http.post(`${this.proveedor}ProveedorCont`,JSON.stringify(contProveedor));
  }
    
  obtContProveedor(idProveedor,idCont){
    return this.http.get(`${this.proveedor}ObtenerDato/${idProveedor}/${idCont}`);
  }
    
  EditarContProveedor(contProveedor){
    return this.http.post(`${this.proveedor}ProveedorContMod`,JSON.stringify(contProveedor));
  }
    
  EliminarContProveedor(contProveedor){
    return this.http.post(`${this.proveedor}ProveedorContDel`,JSON.stringify(contProveedor));
  }

  Paises(){
    return this.http.get(`${this.config}Pais`);
  }

  EstadoFiltro(idPais){
    return this.http.get(`${this.config}FiltroEstado/${idPais}`);
  }
    
  CiudadFiltro(idEstado,idPais){
    return this.http.get(`${this.config}FiltroCiudad/${idEstado}/${idPais}`)
  }

  TipoPersonas(){
    return this.http.get(`${this.config}PersonaTipos`);
  }

  Proveedora(idProveedor){
    return this.http.get(`${this.proveedor}ProveedorAc/${idProveedor}`);
  }

  ProveedoraCrear(Proveedora){
    return this.http.post(`${this.proveedor}ProveedorACrear`,JSON.stringify(Proveedora));
  }

  ProveedoraEditar(Proveedora){
    return this.http.post(`${this.proveedor}ProveedorAEditar`,JSON.stringify(Proveedora));
  }

  ProveedoraEliminar(Proveedora){
    return this.http.post(`${this.proveedor}ProveedorAEliminar`,JSON.stringify(Proveedora));
  }

  Banco(){
    return this.http.get(`${this.config}Banco`);
  }

}
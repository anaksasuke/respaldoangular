import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { dominio } from './../util.service';

@Injectable({
  providedIn: 'root'
})
export class ProyectosService {

    public proyecto = `${dominio}Proyecto/`; 
    public url = `${dominio}index.php/Cliente/`;
     
    constructor(private http:HttpClient) { }

    Cliente(){
        return this.http.get(`${this.url}Clientes`);
    }

    Proyectos(){
        return this.http.get(`${this.proyecto}Proyectos`);
    }

    ProyectoCrear(proyecto){
    return this.http.post(`${this.proyecto}ProyectoCrear`, JSON.stringify(proyecto));
    }

    ProyectoEditar(proyecto){
    return this.http.post(`${this.proyecto}ProyectoEditar`, JSON.stringify(proyecto))
    }
    
    ProyectoEliminar(proyecto){
    return this.http.post(`${this.proyecto}ProyectoEliminar`, JSON.stringify(proyecto))
    }

    ProyectoCategorias(){
    return this.http.get(`${this.proyecto}ProyectoCategorias`);
    }

    ProyectoEspecialidades(){
    return this.http.get(`${this.proyecto}ProyectoEspecialidades`);
    }

    ProyectoPrioriades(){
    return this.http.get(`${this.proyecto}ProyectoPrioridades`)
    }

    ProyectoEstatus(){
    return this.http.get(`${this.proyecto}ProyectoEstatus`);
    }

    ProyectoProgreso(){
    return this.http.get(`${this.proyecto}ProyectoProgreso`);
    }

    ClienteProyectos(){
    return this.http.get(`${this.proyecto}ClienteProyectos`);
    }

    ClienteProyectoCrear(clienteproyecto){
    return this.http.post(`${this.proyecto}ClienteProyectoCrear`, JSON.stringify(clienteproyecto));
    }

    ClienteProyectoEditar(clienteproyecto){
    return this.http.post(`${this.proyecto}ClienteProyectoEditar`, JSON.stringify(clienteproyecto));
    }

    ClienteProyectoEliminar(clienteproyecto){
    return this.http.post(`${this.proyecto}ClienteProyectoEliminar`, JSON.stringify(clienteproyecto))
    }

}
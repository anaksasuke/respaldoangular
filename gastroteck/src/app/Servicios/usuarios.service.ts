import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { dominio } from './../util.service';

@Injectable({
  providedIn: 'root'
})

export class UsuarioService{
    public url = `${dominio}Usuario/`;
    datos;
    constructor(private http:HttpClient){}

    Usuarios(){
        return this.http.get(`${this.url}Usuarios`);
    }

    UsuariosCrear(usuario){
        return this.http.post(`${this.url}UsuariosCrear`,JSON.stringify(usuario));
    }

    UsuariosEditar(usuarios){
        return this.http.post(`${this.url}UsuariosEditar`,JSON.stringify(usuarios));
    }

    UsuariosDelet(usuarios){
        return this.http.post(`${this.url}UsuariosDelet`,JSON.stringify(usuarios));
    }

    UsuariosSub(id){
        return this.http.get(`${this.url}SubUsuarios/${id}`);
    }
      
    Rol(){
        return this.http.get(`${this.url}Rol`);
    }

    Estatus(){
        return this.http.get(`${this.url}Estatus`);
    }

    showDatos(){
        return this.datos;
    }

    //Metodo de actualizacion sin modal 
    getDatos(param){
        this.datos = param;
    }

}
//importacion de componentes para hacer un router
import {ModuleWithProviders} from '@angular/core';
import {Routes,RouterModule} from '@angular/router';

//Componentes 
import { LoginComponent } from './Componentes/Login/login/login.component';
import { HomeComponent } from './Componentes/Home/home/home.component';
import { ShowComponent } from './Componentes/Cliente/show/show.component';
import { CreateComponent } from './Componentes/Cliente/create/create.component';
import { EditComponent } from './Componentes/Cliente/edit/edit.component';
import { PaisComponent } from './Componentes/Configuraciones/pais/pais.component';
import { EstadoComponent } from './Componentes/Configuraciones/estado/estado.component';
import { CuidadComponent } from './Componentes/Configuraciones/cuidad/cuidad.component';
import { IndexComponent } from './Componentes/Configuraciones/index/index.component';
import { PlomeriasComponent } from './Componentes/Configuraciones/plomerias/plomerias.component';
import { ElectricidadComponent } from './Componentes/Configuraciones/electricidad/electricidad.component';
import { GasesComponent } from './Componentes/Configuraciones/gases/gases.component';
import { BancoComponent } from './Componentes/Configuraciones/banco/banco.component';
import { CuentabComponent } from './Componentes/Configuraciones/cuentab/cuentab.component';
import { ShowUComponent } from './Componentes/Usuario/show-u/show-u.component';
import { AltaUComponent } from './Componentes/Usuario/alta-u/alta-u.component';
import { EditUComponent } from './Componentes/Usuario/edit-u/edit-u.component';
import { AreasComponent } from './Componentes/Configuraciones/areas/areas.component';
import { CategoriasComponent } from './Componentes/Configuraciones/categorias/categorias.component';
import { MonedaComponent } from './Componentes/Configuraciones/moneda/moneda.component';
import { ConvertidorComponent } from './Componentes/Configuraciones/convertidor/convertidor.component';
import { DepartamentoComponent } from './Componentes/Configuraciones/departamento/departamento.component';
import { GrupoComponent } from './Componentes/Configuraciones/grupo/grupo.component';
import { LineasComponent } from './Componentes/Configuraciones/lineas/lineas.component';
import { IvaComponent } from './Componentes/Configuraciones/iva/iva.component';
import { MarcasComponent } from './Componentes/Configuraciones/marcas/marcas.component';
import { MedidaComponent } from './Componentes/Configuraciones/medida/medida.component';
import { CategoriasArticuloComponent } from './Componentes/Configuraciones/categorias-articulo/categorias-articulo.component';
import { ProveedoresComponent } from './Componentes/Proveedor/proveedores/proveedores.component';
import { ProveedorComponent } from './Componentes/Proveedor/proveedor/proveedor.component';
import { ShowCatalagoComponent } from './Componentes/Catalago/show-catalago/show-catalago.component';
import { CreateCatalagoComponent } from './Componentes/Catalago/create-catalago/create-catalago.component';
import { UpdateCatalagoComponent } from './Componentes/Catalago/update-catalago/update-catalago.component';
import { ProyectosComponent } from './Componentes/Proyecto/proyectos/proyectos.component';
import { ClientesComponent } from './Componentes/Proyecto/clientes/clientes.component';
import { ContactosComponent } from './Componentes/Cliente/contactos/contactos.component';
import { ArticulosComponent } from './Componentes/Catalago/articulos/articulos.component';
import { DescartadosComponent } from './Componentes/Cliente/descartados/descartados.component';
import { CotizacionCrearComponent } from './Componentes/Cotizaciones/cotizacion-crear/cotizacion-crear.component';
import { ExcepcionComponent } from './Componentes/Configuraciones/excepcion/excepcion.component';
import { PoliticasComponent } from './Componentes/Configuraciones/politicas/politicas.component';
import { CotizacionshowComponent } from './Componentes/Cotizaciones/cotizacionshow/cotizacionshow.component';
import { CotizacionEdVersionComponent } from './Componentes/Cotizaciones/cotizacion-ed-version/cotizacion-ed-version.component';
import { CotizacionVerVersionComponent } from './Componentes/Cotizaciones/cotizacion-ver-version/cotizacion-ver-version.component';
import { ContactosProComponent } from './Componentes/Proveedor/contactos-pro/contactos-pro.component';
import { CotizacionNewVersionComponent } from './Componentes/Cotizaciones/cotizacion-new-version/cotizacion-new-version.component';
import { OrderShowComponent } from './Componentes/Ordenes/order-show/order-show.component';
import { OrderCrearComponent } from './Componentes/Ordenes/order-crear/order-crear.component';
import { ProveedoraComponent } from './Componentes/Proveedor/proveedora/proveedora.component';
import { AclasificacionesComponent } from './Componentes/Configuraciones/aclasificaciones/aclasificaciones.component';
import { VincularComponent } from './Componentes/Configuraciones/vincular/vincular.component';
import { OrderEditComponent } from './Componentes/Ordenes/order-edit/order-edit.component';
import { OrderVersionComponent } from './Componentes/Ordenes/order-version/order-version.component';
import { OverVersionComponent } from './Componentes/Ordenes/over-version/over-version.component';
import { EntradaCrearComponent } from './Componentes/Entradas/entrada-crear/entrada-crear.component';
import { EntradaEditarComponent } from './Componentes/Entradas/entrada-editar/entrada-editar.component';
import { EntradaNewVersionComponent } from './Componentes/Entradas/entrada-new-version/entrada-new-version.component';
import { EntradaShowComponent } from './Componentes/Entradas/entrada-show/entrada-show.component';
import { EntradaVerVersionComponent } from './Componentes/Entradas/entrada-ver-version/entrada-ver-version.component';
import { FacturasCreateComponent } from './Componentes/Facturas/facturas-create/facturas-create.component';
import { FacturasShowComponent } from './Componentes/Facturas/facturas-show/facturas-show.component';
import { FacturasEditarVersionComponent } from './Componentes/Facturas/facturas-editar-version/facturas-editar-version.component';
import { FacturasNuevaVersionComponent } from './Componentes/Facturas/facturas-nueva-version/facturas-nueva-version.component';
import { FacturasVerVersionComponent } from './Componentes/Facturas/facturas-ver-version/facturas-ver-version.component';
import { SalidasComponent } from './Componentes/salidas/salida-crear/salidas.component';
//Array para rutas 

import { NotaCreditoCrearComponent } from './Componentes/NotasCredito/nota-credito-crear/nota-credito-crear.component';
import { NotaCreditoVerComponent } from './Componentes/NotasCredito/nota-credito-ver/nota-credito-ver.component';
import { NotaCreditoEditarComponent } from './Componentes/NotasCredito/nota-credito-editar/nota-credito-editar.component';
import { NotaCreditoVerversionComponent } from './Componentes/NotasCredito/nota-credito-verversion/nota-credito-verversion.component';
import { NotaCreditoNuevaversionComponent } from './Componentes/NotasCredito/nota-credito-nuevaversion/nota-credito-nuevaversion.component';
import { PagosComponent } from './Componentes/pagos/pagos.component';
import { DesactivadosComponent } from './Componentes/Catalago/desactivados/desactivados.component';
import { SalidaShowComponent } from './Componentes/salidas/salida-show/salida-show.component';
import { SalidaEditarComponent } from './Componentes/salidas/salida-editar/salida-editar.component'; 
import { SalidaNewVersionComponent } from './Componentes/salidas/salida-new-version/salida-new-version.component';
import { CotizacionComponent } from './Componentes/Pdf/cotizacion/cotizacion.component';

//Array para rutas  

const appRoutes:Routes=[
    {path:'login',component:LoginComponent},
    {path:'home',component:HomeComponent},
    {path:'usuario',component:ShowUComponent},
    {path:'usuario/crear',component:AltaUComponent},
    {path:'usuario/editar',component:EditUComponent},
    {path:'cliente',component:ShowComponent},
    //{path:'cliente/crear',component:CreateComponent},
    {path:'cliente/editar',component:EditComponent},
    {path:'cliente/contactos',component:ContactosComponent},
    {path:'cliente/descartados',component:DescartadosComponent},
    {path:'proveedores', component:ProveedoresComponent},
    {path:'proveedores/proveedor', component:ProveedorComponent},
    {path:'proveedores/contactos',component:ContactosProComponent},
    {path:'proveedores/proveedora', component:ProveedoraComponent},
    {path:'catalago',component:ShowCatalagoComponent},
    {path:'catalago/crear',component:CreateCatalagoComponent},
    {path:'catalago/editar',component:UpdateCatalagoComponent},
    {path:'catalogo/articulos', component:ArticulosComponent},
    {path:'catalago/desactivados',component:DesactivadosComponent},
    {path:'cotizacion',component:CotizacionshowComponent},
    {path:'cotizacion/crear',component:CotizacionCrearComponent},
    {path:'cotizacion/editar',component:CotizacionEdVersionComponent},
    {path:'cotizacion/ver-version', component:CotizacionVerVersionComponent},
    {path:'cotizacion/nueva-version', component:CotizacionNewVersionComponent},
    {path:'orden',component:OrderShowComponent},
    {path:'orden/crear',component:OrderCrearComponent},
    {path:'orden/editar',component:OrderEditComponent},
    {path:'orden/editar-version',component:OrderVersionComponent},
    {path:'orden/ver-version', component:OverVersionComponent},
    {path:'entradas',component:EntradaShowComponent},
    {path:'entradas/crear',component:EntradaCrearComponent},
    {path:'entradas/editar',component:EntradaEditarComponent},
    {path:'entradas/nueva-version',component:EntradaNewVersionComponent},
    {path:'entrada/ver-version', component:EntradaVerVersionComponent},
    {path:'facturas',component:FacturasShowComponent},
    {path:'facturas/crear',component:FacturasCreateComponent},
    {path:'facturas/editar-version',component:FacturasEditarVersionComponent},
    {path:'facturas/nueva-version',component:FacturasNuevaVersionComponent},
    {path:'facturas/ver-version', component:FacturasVerVersionComponent},
    {path:'pago',component:PagosComponent},
    {path:'configuraciones/pais',component:PaisComponent},
    {path:'configuraciones/estado',component:EstadoComponent},
    {path:'configuraciones/ciudad',component:CuidadComponent},
    {path:'configuraciones/index',component:IndexComponent},
    {path:'configuraciones/plomerias', component:PlomeriasComponent},
    {path:'configuraciones/electricidad', component:ElectricidadComponent},
    {path:'configuraciones/gases', component:GasesComponent},
    {path:'configuraciones/banco', component:BancoComponent},
    {path:'configuraciones/cuentab', component:CuentabComponent},
    {path:'configuraciones/area', component:AreasComponent},
    {path:'configuraciones/categorias', component:CategoriasComponent},
    {path:'configuraciones/moneda', component:MonedaComponent},
    {path:'configuraciones/convertidor', component:ConvertidorComponent},
    {path:'configuraciones/departamentos',component:DepartamentoComponent},
    {path:'configuraciones/grupo',component:GrupoComponent},
    {path:'configuraciones/linea',component:LineasComponent},
    {path:'configuraciones/marcas', component:MarcasComponent},
    {path:'configuraciones/medida', component:MedidaComponent},
    {path:'configuraciones/iva', component:IvaComponent},
    {path:'configuraciones/articuloCategoria',component:CategoriasArticuloComponent},
    {path:'configuraciones/excepciones', component:ExcepcionComponent},
    {path:'configuraciones/politicas', component:PoliticasComponent},
    {path:'proyectos', component:ProyectosComponent},
    {path:'proyectos/clientes', component:ClientesComponent},
    {path:'configuraciones/aclasificaciones', component:AclasificacionesComponent},
    {path:'salida',component:SalidaShowComponent},
    {path:'salida/crear',component:SalidasComponent},
    {path:'salida/editar',component:SalidaEditarComponent},
    {path:'salida/nueva-version',component:SalidaNewVersionComponent},
    {path:'configuraciones/vincular', component:VincularComponent},
    {path:'notacredito/nota-credito-crear', component:NotaCreditoCrearComponent},
    {path:'notacredito', component:NotaCreditoVerComponent},
    {path:'notacredito/nota-credito-editar', component:NotaCreditoEditarComponent},
    {path:'notacredito/ver-version', component:NotaCreditoVerversionComponent},
    {path:'notacredito/nueva-version', component:NotaCreditoNuevaversionComponent},
    {path:'pdf/pdfcotizacion', component:CotizacionComponent}
   
];

//export router hacia app.modules

export const appRoutingProviders:any[]=[];
export const routing:ModuleWithProviders = RouterModule.forRoot(appRoutes);
import { Component, OnInit, OnDestroy, ChangeDetectorRef} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import { UtilService } from './util.service';
import { ServiciosService } from './servicios.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'gastroteck';
  mobileQuery:MediaQueryList;
  rol;
  private _mobileQueryListener: () => void;
  constructor(public util:UtilService,public http:ServiciosService,changeDetectorRef:ChangeDetectorRef,media:MediaMatcher){
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit(){
    if(localStorage.Usuario == undefined || localStorage.Usuario == null){
      this.util.navegacion("/login");
    }
    this.rol = localStorage.Rol ;
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  Router(param){
    this.util.navegacion(param);
  }

  Reset(){
    this.http.getDatos(undefined);
  }

  Salir(){
    localStorage.clear();
    this.util.navegacion("login")
  }

}

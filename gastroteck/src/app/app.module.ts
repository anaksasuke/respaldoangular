
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {routing,appRoutingProviders} from './aap.routing';
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';

//Componentes
import { AppComponent } from './app.component';
import { HomeComponent } from './Componentes/Home/home/home.component';
import { LoginComponent } from './Componentes/Login/login/login.component';
import { ShowComponent,descartePop } from './Componentes/Cliente/show/show.component';
import { CreateComponent } from './Componentes/Cliente/create/create.component';
import { PaisComponent, PaisModal } from './Componentes/Configuraciones/pais/pais.component';
import { IndexComponent } from './Componentes/Configuraciones/index/index.component';
import { EstadoComponent } from './Componentes/Configuraciones/estado/estado.component';
import { PlomeriasComponent, plomeriasModal } from './Componentes/Configuraciones/plomerias/plomerias.component';
import { CuidadComponent, ciudadModal } from './Componentes/Configuraciones/cuidad/cuidad.component';
import { ElectricidadComponent, electricidadModal } from './Componentes/Configuraciones/electricidad/electricidad.component';
import { GasesComponent, gasesModal } from './Componentes/Configuraciones/gases/gases.component';
import { BancoComponent, bancoPop } from './Componentes/Configuraciones/banco/banco.component';
import { CuentabComponent, cuentabModal } from './Componentes/Configuraciones/cuentab/cuentab.component';
import { ShowUComponent } from './Componentes/Usuario/show-u/show-u.component';
import { AltaUComponent } from './Componentes/Usuario/alta-u/alta-u.component';
import { EditUComponent } from './Componentes/Usuario/edit-u/edit-u.component';
import { AreasComponent, areaModal } from './Componentes/Configuraciones/areas/areas.component';
import { CategoriasComponent, categoriaModal } from './Componentes/Configuraciones/categorias/categorias.component';
import { EditComponent } from './Componentes/Cliente/edit/edit.component';
import { DepartamentoComponent, departamentosModal } from './Componentes/Configuraciones/departamento/departamento.component';
import { GrupoComponent, grupoModal } from './Componentes/Configuraciones/grupo/grupo.component';
import { LineasComponent, lineasModal } from './Componentes/Configuraciones/lineas/lineas.component';
import { MonedaComponent, monedaModal } from './Componentes/Configuraciones/moneda/moneda.component';
import { ConvertidorComponent, convertidorModal } from './Componentes/Configuraciones/convertidor/convertidor.component';
import { IvaComponent, ivaModal } from './Componentes/Configuraciones/iva/iva.component';
import { MarcasComponent, marcasModal } from './Componentes/Configuraciones/marcas/marcas.component';
import { MedidaComponent, medidaModal } from './Componentes/Configuraciones/medida/medida.component';
import { CategoriasArticuloComponent, categoriaArticuloModal } from './Componentes/Configuraciones/categorias-articulo/categorias-articulo.component';
import { ProveedoresComponent } from './Componentes/Proveedor/proveedores/proveedores.component';
import { ProveedorComponent } from './Componentes/Proveedor/proveedor/proveedor.component';
import { ProyectosComponent, ModalProyectos } from './Componentes/Proyecto/proyectos/proyectos.component';
import { ClientesComponent, ModalClientes } from './Componentes/Proyecto/clientes/clientes.component';
import { CreateCatalagoComponent } from './Componentes/Catalago/create-catalago/create-catalago.component';
import { ShowCatalagoComponent } from './Componentes/Catalago/show-catalago/show-catalago.component';
import { UpdateCatalagoComponent } from './Componentes/Catalago/update-catalago/update-catalago.component';
import { ArticulosComponent, ModalArticulos } from './Componentes/Catalago/articulos/articulos.component';
import { ContactosComponent, clienteContacto} from './Componentes/Cliente/contactos/contactos.component';
import { DescartadosComponent } from './Componentes/Cliente/descartados/descartados.component';
import { CotizacionCrearComponent } from './Componentes/Cotizaciones/cotizacion-crear/cotizacion-crear.component';
import { ExcepcionComponent } from './Componentes/Configuraciones/excepcion/excepcion.component';
import { PoliticasComponent, politicasModal } from './Componentes/Configuraciones/politicas/politicas.component';
import { CotizacionshowComponent, CotizacionVersiones } from './Componentes/Cotizaciones/cotizacionshow/cotizacionshow.component';
import { CotizacionEdVersionComponent } from './Componentes/Cotizaciones/cotizacion-ed-version/cotizacion-ed-version.component';
import { CotizacionVerVersionComponent } from './Componentes/Cotizaciones/cotizacion-ver-version/cotizacion-ver-version.component';
import { ContactosProComponent, contactoPop } from './Componentes/Proveedor/contactos-pro/contactos-pro.component';
import { ProveedoraComponent, proveedoraPop } from './Componentes/Proveedor/proveedora/proveedora.component';
import { CotizacionNewVersionComponent, cotizacionConvertidor } from './Componentes/Cotizaciones/cotizacion-new-version/cotizacion-new-version.component';
import { AclasificacionesComponent, aclasificacionesModal } from './Componentes/Configuraciones/aclasificaciones/aclasificaciones.component';
import { VincularComponent, vincularModal } from './Componentes/Configuraciones/vincular/vincular.component';
import { OrderCrearComponent } from './Componentes/Ordenes/order-crear/order-crear.component';
import { OrderShowComponent, ModalOrderVersion } from './Componentes/Ordenes/order-show/order-show.component';
import { OrderEditComponent } from './Componentes/Ordenes/order-edit/order-edit.component';
import { OrderVersionComponent } from './Componentes/Ordenes/order-version/order-version.component';
import { OverVersionComponent } from './Componentes/Ordenes/over-version/over-version.component';
import { ModalEstado } from './Componentes/Configuraciones/estado/estado.component';
import { EntradaCrearComponent } from './Componentes/Entradas/entrada-crear/entrada-crear.component';
import { EntradaEditarComponent } from './Componentes/Entradas/entrada-editar/entrada-editar.component';
import { EntradaNewVersionComponent } from './Componentes/Entradas/entrada-new-version/entrada-new-version.component';
import { EntradaShowComponent } from './Componentes/Entradas/entrada-show/entrada-show.component';
import { EntradaVerVersionComponent } from './Componentes/Entradas/entrada-ver-version/entrada-ver-version.component';
import { FacturasShowComponent } from './Componentes/Facturas/facturas-show/facturas-show.component';
import { EstatusFac } from './Componentes/Facturas/facturas-show/facturas-show.component';
import { FolioFiscalPop } from './Componentes/Facturas/facturas-show/facturas-show.component';
import { FacturasCreateComponent, facturaMod } from './Componentes/Facturas/facturas-create/facturas-create.component';
import { FacturasEditarVersionComponent } from './Componentes/Facturas/facturas-editar-version/facturas-editar-version.component';
import { FacturasNuevaVersionComponent } from './Componentes/Facturas/facturas-nueva-version/facturas-nueva-version.component';
import { FacturasVerVersionComponent } from './Componentes/Facturas/facturas-ver-version/facturas-ver-version.component';
import { NotaCreditoCrearComponent } from './Componentes/NotasCredito/nota-credito-crear/nota-credito-crear.component';
import { NotaCreditoVerComponent, EstatusNC, ModalVersiones } from './Componentes/NotasCredito/nota-credito-ver/nota-credito-ver.component';
import { NotaCreditoEditarComponent } from './Componentes/NotasCredito/nota-credito-editar/nota-credito-editar.component';
import { NotaCreditoVerversionComponent } from './Componentes/NotasCredito/nota-credito-verversion/nota-credito-verversion.component';
import { NotaCreditoNuevaversionComponent } from './Componentes/NotasCredito/nota-credito-nuevaversion/nota-credito-nuevaversion.component';
import { PagosComponent, pagoEntrada, pagoFolio} from './Componentes/pagos/pagos.component';
import { pagosPop } from './Componentes/pagos/pagos.component';
//Componentes Material angular 
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatSelectModule } from '@angular/material/select';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatRippleModule } from '@angular/material/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTreeModule } from '@angular/material/tree';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatNativeDateModule } from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSidenavModule} from '@angular/material/sidenav';

//Dependencia para materiales diferentes a material angular 
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ToastrModule } from 'ngx-toastr'; 
//Dependencia para servicos 
import {HttpClientModule} from '@angular/common/http';
import { DesactivadosComponent } from './Componentes/Catalago/desactivados/desactivados.component';
import { CotizacionPop } from './Componentes/Cotizaciones/cotizacionshow/cotizacionshow.component';
import { ordenPop } from './Componentes/Ordenes/order-show/order-show.component';
import { SalidasComponent } from './Componentes/salidas/salida-crear/salidas.component';
import { entradaEstatus } from './Componentes/Entradas/entrada-show/entrada-show.component';
import { SalidaShowComponent } from './Componentes/salidas/salida-show/salida-show.component';
import { SalidaEditarComponent } from './Componentes/salidas/salida-editar/salida-editar.component';
import { SalidaNewVersionComponent } from './Componentes/salidas/salida-new-version/salida-new-version.component';
import { salidaEstatus } from './Componentes/salidas/salida-show/salida-show.component';

import { CotizacionComponent } from './Componentes/Pdf/cotizacion/cotizacion.component';


@NgModule({
  declarations: [
    AppComponent,LoginComponent,HomeComponent,ShowComponent,CreateComponent,PaisComponent,IndexComponent,EstadoComponent,PlomeriasComponent,CuidadComponent,
    ElectricidadComponent,GasesComponent,BancoComponent,CuentabComponent,ShowUComponent,AltaUComponent,EditUComponent,AreasComponent,CategoriasComponent,EditComponent,
    DepartamentoComponent,GrupoComponent,LineasComponent,MonedaComponent,ConvertidorComponent,IvaComponent,MarcasComponent,CategoriasArticuloComponent,ProveedoresComponent,
    ProveedorComponent,MedidaComponent,CreateCatalagoComponent, ShowCatalagoComponent,UpdateCatalagoComponent,ProyectosComponent,ModalProyectos, ClientesComponent, ContactosComponent,ArticulosComponent,
    DescartadosComponent,CotizacionCrearComponent,ExcepcionComponent, PoliticasComponent,CotizacionshowComponent,CotizacionVersiones,CotizacionEdVersionComponent, CotizacionNewVersionComponent, ContactosProComponent, OrderCrearComponent, OrderShowComponent,
    VincularComponent,AclasificacionesComponent,ProveedoraComponent, OrderEditComponent,OrderVersionComponent,ModalEstado,EntradaCrearComponent, EntradaEditarComponent, ModalArticulos, ModalClientes, EntradaNewVersionComponent, EntradaShowComponent,
    FacturasShowComponent,EstatusFac,FacturasCreateComponent, FacturasEditarVersionComponent, FacturasNuevaVersionComponent,OverVersionComponent, EntradaVerVersionComponent, FacturasVerVersionComponent,clienteContacto,
    CotizacionVerVersionComponent, ModalOrderVersion,FolioFiscalPop,bancoPop,CotizacionPop,ordenPop,SalidasComponent,entradaEstatus,salidaEstatus,facturaMod,EstatusNC,ModalVersiones,pagoEntrada,pagoFolio,contactoPop,cotizacionConvertidor,descartePop,
    NotaCreditoCrearComponent, NotaCreditoVerComponent, NotaCreditoEditarComponent,NotaCreditoVerversionComponent, NotaCreditoNuevaversionComponent, PagosComponent,pagosPop, DesactivadosComponent, SalidaShowComponent, SalidaEditarComponent, SalidaNewVersionComponent, PaisModal, plomeriasModal, ciudadModal, electricidadModal,
     gasesModal, cuentabModal, areaModal, categoriaModal,departamentosModal,grupoModal,lineasModal,monedaModal,convertidorModal, ivaModal, marcasModal, medidaModal, categoriaArticuloModal, politicasModal, aclasificacionesModal, vincularModal, proveedoraPop,CotizacionComponent],
  imports: [
    routing,BrowserModule,BrowserAnimationsModule,MatInputModule,MatProgressSpinnerModule,MatTableModule,MatPaginatorModule,MatSortModule,MatSidenavModule,
    MDBBootstrapModule.forRoot(),HttpClientModule,FormsModule,MatSelectModule,MatExpansionModule,MatFormFieldModule,MatMenuModule,MatIconModule,MatButtonModule, MatTabsModule, MatTooltipModule, MatToolbarModule,
    MatRippleModule,ReactiveFormsModule,MatCheckboxModule,MatAutocompleteModule,MatTreeModule,MatCardModule,MatDialogModule,MatNativeDateModule,MatDatepickerModule,
    ToastrModule.forRoot({closeButton:true,timeOut:2000})
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA],
  entryComponents:[EstadoComponent,ModalEstado,ArticulosComponent,entradaEstatus,pagoFolio,ModalArticulos,ClientesComponent,ModalClientes,EstatusFac,salidaEstatus,EstatusNC,ModalVersiones,facturaMod,ModalOrderVersion,pagosPop,contactoPop,pagoEntrada,FolioFiscalPop,bancoPop,CotizacionPop,ordenPop, ModalProyectos, descartePop, CotizacionVersiones,
    PaisModal, plomeriasModal, ciudadModal, electricidadModal, gasesModal,clienteContacto, cuentabModal, areaModal, categoriaModal,departamentosModal, grupoModal, lineasModal, monedaModal, convertidorModal, ivaModal,cotizacionConvertidor, marcasModal, medidaModal, categoriaArticuloModal, politicasModal, aclasificacionesModal, vincularModal, proveedoraPop, CreateComponent
  ], //Aqui debes declarar el nuevo modal que esta declarando y el html 
  providers: [appRoutingProviders,MatNativeDateModule,MatDatepickerModule],
  bootstrap: [AppComponent],
  
})
export class AppModule { }

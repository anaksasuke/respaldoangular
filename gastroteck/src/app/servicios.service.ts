import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { dominio } from './util.service';

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {

  id;
  nombre;
  datos;
  datosProv = null;
  
  public url = `${dominio}Actualizado/`;
  public P = `${dominio}index.php/Configuraciones/`;  
  public proveedor = `${dominio}Proveedor/`;
  public proyecto = `${dominio}Proyecto/`;
  public catalagourl = `${dominio}Catalago/`;
  constructor(private http:HttpClient) { }

  Login(username,password){
    return this.http.get(`${this.url}Login/${username}/${password}`);
  }

  Cliente(){
    return this.http.get(`${this.url}Clientes`);
  }

  ClienteCrear(cliente){
    return this.http.post(`${this.url}ClienteCrear`,JSON.stringify(cliente));
  }

  ClienteEditar(cliente){
    return this.http.post(`${this.url}ClienteEditar`,JSON.stringify(cliente));
  }

  tipoCliente(){
    return this.http.get(`${this.url}TiposClientes`);
  }

  DatosCliente(idCliente){
    return this.http.post(`${this.url}ClientesDatos/${idCliente}`,JSON.stringify(idCliente));
  }

  Descarte(descarte){
    return this.http.post(`${this.url}Descartar`,JSON.stringify(descarte));
  }

  RazonDescarte(){
    return this.http.get(`${this.url}RDescarte`);
  }


  Agentes(){
    return this.http.get(`${this.url}Agentes`);
  }

  TipoPersonas(){
    return this.http.get(`${this.P}PersonaTipos`);
  }

  Paises(){
    return this.http.get(`${this.url}Pais`);
  }
  
  PaisCrearPrueba(pais){
    return this.http.post(`${this.P}PaisCrear`,JSON.stringify(pais));
  }

  PaisEditPrueba(pais){
    return this.http.post(`${this.P}PaisEditar`,JSON.stringify(pais));
  }

  PaisCrear(Pais){
    return this.http.post(`${this.url}PaisCrear`,JSON.stringify(Pais));
  }

  PaisEdit(Pais){
    return this.http.post(`${this.url}PaisEditar`,JSON.stringify(Pais));
  }

  PaisDelet(Pais){
    return this.http.post(`${this.url}PaisDelete`,JSON.stringify(Pais));
  }

  Estados(){
    return this.http.get(`${this.url}Estado`);
  }

  EstadosCrear(Estado){
    return this.http.post(`${this.url}EstadoCrear`,JSON.stringify(Estado));
  }

  EstadosEdit(Estado){
    return this.http.post(`${this.url}EstadoEditar`,JSON.stringify(Estado));
  }

  EstadosDelet(Estado){
    return this.http.post(`${this.url}EstadoDelete`,JSON.stringify(Estado));
  }

  Ciudad(){
    return this.http.get(`${this.url}Ciudad`);
  }

  CiudadCrear(ciudad){
    return this.http.post(`${this.url}CiudadCrear`,JSON.stringify(ciudad));
  }

  CiudadEditar(ciudad){
    return this.http.post(`${this.url}CiudadEditar`,JSON.stringify(ciudad));
  }

  CiudadDelet(ciudad){
    return this.http.post(`${this.url}CuidadDelet`,JSON.stringify(ciudad));
  }

  Tuberias(){
    return this.http.get(`${this.url}Tuberia`);
  }

  TuberiaCrear(Tuberia){
    return this.http.post(`${this.url}TuberiaCrear`,JSON.stringify(Tuberia));
  }
  
  TuberiaEditar(Tuberia){
    return this.http.post(`${this.url}TuberiaEditar`,JSON.stringify(Tuberia));
  }

  TuberiaDelet(Tuberia){
    return this.http.post(`${this.url}TuberiaDelet`,JSON.stringify(Tuberia));
  }

  Electricidad(){
    return this.http.get(`${this.url}Electricidad`);
  }

  ElectricidadCrear(Electricidad){
    return this.http.post(`${this.url}ElectricidadCrear`,JSON.stringify(Electricidad));
  }

  ElectricidadEdit(Electricidad){
    return this.http.post(`${this.url}ElectricidadEdit`,JSON.stringify(Electricidad));
  }

  ElectricidadDelet(Electricidad){
    return this.http.post(`${this.url}ElectricidadDelet`,JSON.stringify(Electricidad));
  }

  Calor(){
    return this.http.get(`${this.url}Calor`);
  }

  CalorCrear(Gases){
    return this.http.post(`${this.url}CalorCrear`,JSON.stringify(Gases));
  }

  CalorEditar(Gases){
    return this.http.post(`${this.url}CalorEditar`,JSON.stringify(Gases));
  }

  CalorDelet(Gases){
    return this.http.post(`${this.url}CalorDelet`,JSON.stringify(Gases));
  }

  Banco(){
    return this.http.get(`${this.url}Banco`);
  }
  
  BancoCrear(Banco){
    return this.http.post(`${this.url}BancoCrear`,JSON.stringify(Banco));
  }

  BancoEditar(Banco){
    return this.http.post(`${this.url}BancoEditar`,JSON.stringify(Banco));
  }
  
  BancoDelet(Banco){
    return this.http.post(`${this.url}BancoDelet`,JSON.stringify(Banco));
  }

  Usuarios(){
    return this.http.get(`${this.url}Usuarios`);
  }

  UsuariosCrear(usuario){
    return this.http.post(`${this.url}UsuariosCrear`,JSON.stringify(usuario));
  }

  UsuariosEditar(usuarios){
    return this.http.post(`${this.url}UsuariosEditar`,JSON.stringify(usuarios));
  }

  UsuariosDelet(usuarios){
    return this.http.post(`${this.url}UsuariosDelet`,JSON.stringify(usuarios));
  }

  UsuariosSub(id){
    return this.http.get(`${this.url}SubUsuarios/${id}`);
  }

  Cuentab(idCliente){
    return this.http.get(`${this.url}CuentaBancaria/${idCliente}`);
  }

  CuentabCrear(Cuentab){
    return this.http.post(`${this.url}CuentaBancariaCrear`,JSON.stringify(Cuentab));
  }

  CuentabEditar(Cuentab){
    return this.http.post(`${this.url}CuentaBancariaEditar`,JSON.stringify(Cuentab));
  }

  CuentabDelet(Cuentab){
    return this.http.post(`${this.url}CuentaBancariaDelet`,JSON.stringify(Cuentab));
  }

  Marcas(){
    return this.http.get(`${this.url}Marcas`);
  }

  MarcasCrear(Marcas){
    return this.http.post(`${this.url}MarcasCrear`,JSON.stringify(Marcas));
  }

  MarcasEditar(Marcas){
    return this.http.post(`${this.url}MarcasEditar`,JSON.stringify(Marcas));
  }

  MarcasDelet(Marcas){
    return this.http.post(`${this.url}MarcasDelet`,JSON.stringify(Marcas));
  }

  Medida(){
    return this.http.get(`${this.url}Medida`);
  }
  
  MedidaCrear(Medida){
    return this.http.post(`${this.url}MedidaCrear`,JSON.stringify(Medida));
  }
  MedidaEditar(Medida){
    return this.http.post(`${this.url}MedidaEditar`,JSON.stringify(Medida));
  }

  MedidaDelet(Medida){
    return this.http.post(`${this.url}MedidaDelet`,JSON.stringify(Medida));
  }
  Areas(){
    return this.http.get(`${this.url}Areas`);
  }

  AreasCrear(area){
    return this.http.post(`${this.url}AreasCrear`,JSON.stringify(area));
  }

  AreasEditar(area){
    return this.http.post(`${this.url}AreasEditar`,JSON.stringify(area));
  }

  AreasDelet(area){
    return this.http.post(`${this.url}AreaDelet`,JSON.stringify(area));
  }

  Categorias(){
    return this.http.get(`${this.url}Categorias`);
  }

  CategoriasCrear(categorias){
    return this.http.post(`${this.url}CategoriasCrear`,JSON.stringify(categorias));
  }
  
  CategoriasEditar(categorias){
    return this.http.post(`${this.url}CategoriasEditar`,JSON.stringify(categorias))
  }

  CategoriasDelet(categorias){
    return this.http.post(`${this.url}CategoriaDelet`,JSON.stringify(categorias));
  }
  
  Departamento(){
    return this.http.get(`${this.url}Departamento`);
  }
  
  DepartamentoCrear(departamento){
    return this.http.post(`${this.url}DepartamentoCrear`,JSON.stringify(departamento));
  }

  DepartamentoEditar(departamento){
    return this.http.post(`${this.url}DepartamentoEditar`,JSON.stringify(departamento));
  }

  DepartamentoDelet(departamento){
    return this.http.post(`${this.url}DepartamentoDelet`,JSON.stringify(departamento));
  }

  Grupos(){
    return this.http.get(`${this.url}Grupos`);
  }

  GruposCrear(grupo){
    return this.http.post(`${this.url}GruposCrear`,JSON.stringify(grupo))
  }

  GruposEditar(grupo){
    return this.http.post(`${this.url}GruposEditar`,JSON.stringify(grupo));
  }

  Lineas(){
    return this.http.get(`${this.url}Lineas`);
  }

  LineasCrear(linea){
    return this.http.post(`${this.url}LineasCrear`,JSON.stringify(linea));
  }

  LineasEditar(linea){
    return this.http.post(`${this.url}LineasEditar`,JSON.stringify(linea));
  }

  LineasDelete(linea){
    return this.http.post(`${this.url}LineaDelet`,JSON.stringify(linea));
  }

  ObtenerGrupo(idDepartamento){
    return this.http.post(`${this.url}SubLinea/${idDepartamento}`,JSON.stringify(idDepartamento));
  }

  ObtenerLine(idDep,idGroup){
    return this.http.post(`${this.url}ObtenerLinea/${idDep}/${idGroup}`,JSON.stringify(idDep,idGroup));
  }

  Monedas(){
    return this.http.get(`${this.url}Monedas`);
  }

  MonedasCrear(monedas){
    return this.http.post(`${this.url}MonedaCrear`, JSON.stringify(monedas));
  }

  MonedasEditar(monedas){
    return this.http.post(`${this.url}MonedaEditar`, JSON.stringify(monedas));
  }

  MonedasEliminar(monedas){
    return this.http.post(`${this.url}MonedaEliminar`, JSON.stringify(monedas));
  }

  Conversiones(){
    return this.http.get(`${this.url}Conversiones`); //para llenar de una vista  
  }

  ConversionesMoneda(conversiones){ //nombre del servicio
    return this.http.post(`${this.url}ConversionMoneda/${conversiones}`, JSON.stringify(conversiones)); //para llenar un dropdownlist 
  }

  ConversionesCrear(conversiones){
    return this.http.post(`${this.url}ConversionCrear`, JSON.stringify(conversiones));
  
  }

  ConversionesEditar(conversiones){
    return this.http.post(`${this.url}ConversionEditar`, JSON.stringify(conversiones));
  }

  ConversionesEliminar(conversiones){
    return this.http.post(`${this.url}ConversionEliminar`, JSON.stringify(conversiones));
  }

  Giro(){
    return this.http.get(`${this.url}Giro`);
  }

  Especialidad(){
    return this.http.get(`${this.url}Especialidad`);
  }

  Ivas(){
    return this.http.get(`${this.url}Ivas`);
  }

  IvaCrear(iva){
    return this.http.post(`${this.url}IvaCrear`, JSON.stringify(iva));
  }

  IvasEditar(iva){
    return this.http.post(`${this.url}IvaEditar`, JSON.stringify(iva));
  }

  IvasEliminar(iva){
    return this.http.post(`${this.url}IvaEliminar`, JSON.stringify(iva));
  }

  Excepciones(){
    return this.http.get(`${this.url}Excepciones`);
  }

  ExcepcionCrear(excepcion){
    return this.http.post(`${this.url}ExcepcionCrear`, JSON.stringify(excepcion));
  }

  ExcepcionEditar(excepcion){
    return this.http.post(`${this.url}ExcepcionEditar`, JSON.stringify(excepcion));
  }

  ExcepcionEliminar(excepcion){
    return this.http.post(`${this.url}ExcepcionEliminar`, JSON.stringify(excepcion));
  }
  
  Proveedores(){
    return this.http.get(`${this.proveedor}Proveedores`);
  }

  ProveedorCrear(proveedor){
    return this.http.post(`${this.proveedor}ProveedorCrear`, JSON.stringify(proveedor));
  }

  ProveedorDatos(proveedor){
    return this.http.post(`${this.proveedor}ProveedorDatos/${proveedor}`,JSON.stringify(proveedor));
  }

  ProveedorEditar(proveedor){
    return this.http.post(`${this.proveedor}ProveedorEditar`, JSON.stringify(proveedor));
  }

  ProveedorEliminar(proveedor){
    return this.http.post(`${this.proveedor}ProveedorEliminar`, JSON.stringify(proveedor));
  }

  ProveedorContactos(idProveedor){
    return this.http.get(`${this.proveedor}Contactos/${idProveedor}`);
  }

  TipoContactosPro(){
    return this.http.get(`${this.proveedor}TipoContacto`);
  }

  AgregarContProveedor(contProveedor){
    return this.http.post(`${this.proveedor}ProveedorCont`,JSON.stringify(contProveedor));
  }

  obtContProveedor(idProveedor,idCont){
    return this.http.get(`${this.proveedor}ObtenerDato/${idProveedor}/${idCont}`);
  }

  EditarContProveedor(contProveedor){
    return this.http.post(`${this.proveedor}ProveedorContMod`,JSON.stringify(contProveedor));
  }

  EliminarContProveedor(contProveedor){
    return this.http.post(`${this.proveedor}ProveedorContDel`,JSON.stringify(contProveedor));
  }

  Proyectos(){
    return this.http.get(`${this.proyecto}Proyectos`);
  }

  ProyectoCrear(proyecto){
    return this.http.post(`${this.proyecto}ProyectoCrear`, JSON.stringify(proyecto));
  }

  ProyectoEditar(proyecto){
    return this.http.post(`${this.proyecto}ProyectoEditar`, JSON.stringify(proyecto))
  }
  
  ProyectoEliminar(proyecto){
    return this.http.post(`${this.proyecto}ProyectoEliminar`, JSON.stringify(proyecto))
  }

  ProyectoCategorias(){
    return this.http.get(`${this.proyecto}ProyectoCategorias`);
  }

  ProyectoEspecialidades(){
    return this.http.get(`${this.proyecto}ProyectoEspecialidades`);
  }

  ProyectoPrioriades(){
    return this.http.get(`${this.proyecto}ProyectoPrioridades`)
  }

  ProyectoEstatus(){
    return this.http.get(`${this.proyecto}ProyectoEstatus`);
  }

  ProyectoProgreso(){
    return this.http.get(`${this.proyecto}ProyectoProgreso`);
  }

  ClienteProyectos(){
    return this.http.get(`${this.proyecto}ClienteProyectos`);
  }

  ClienteProyectoCrear(clienteproyecto){
    return this.http.post(`${this.proyecto}ClienteProyectoCrear`, JSON.stringify(clienteproyecto));
  }

  ClienteProyectoEditar(clienteproyecto){
    return this.http.post(`${this.proyecto}ClienteProyectoEditar`, JSON.stringify(clienteproyecto));
  }

  ClienteProyectoEliminar(clienteproyecto){
    return this.http.post(`${this.proyecto}ClienteProyectoEliminar`, JSON.stringify(clienteproyecto))
  }

  Unidad(){
    return this.http.get(`${this.url}UnidadesMed`);
  }

  ArticuloProveedores(){
    return this.http.get(`${this.catalagourl}ArticuloProveedores`);
  }

  ArticuloProveedorCrear(articuloproveedor){
    return this.http.post(`${this.catalagourl}ArticuloProveedorCrear`, JSON.stringify(articuloproveedor));
  }

  ArticuloProveedorEditar(articuloproveedor){
    return this.http.post(`${this.catalagourl}ArticuloProveedorEditar`, JSON.stringify(articuloproveedor));
  }

  ArticuloProveedorEliminar(articuloproveedor){
    return this.http.post(`${this.catalagourl}ArticuloProveedorEliminar`, JSON.stringify(articuloproveedor));
  }

  //metodo para guardar variables de session
  getSession(id,nombre){
    this.id = id;
    this.nombre = nombre;
  }

  //metodo para mostrar variables de session
  showSession(){
    var Session={
      idUser:this.id,
      username:this.nombre
    }
    return Session;
  }

  //Metodo de actualizacion sin modal 
  getDatos(param){
    this.datos = param;
  }

  showDatos(){
    return this.datos;
  }

  setDatosProv(param){
    this.datosProv = param;
  }

  getDatosProv(){
    return this.datosProv;
  }  

  EstadoFiltro(idPais){
    return this.http.get(`${this.url}FiltroEstado/${idPais}`);
  }

  CiudadFiltro(idEstado,idPais){
    return this.http.get(`${this.url}FiltroCiudad/${idEstado}/${idPais}`)
  }

  Rol(){
    return this.http.get(`${this.url}Rol`);
  }

  Estatus(){
    return this.http.get(`${this.url}Estatus`);
  }

  categoriasArticulo(){
    return this.http.get(`${this.url}TipoCategoria`);
  }

  categoriasArticuloCrear(categoria){
    return this.http.post(`${this.url}TipoCategoriaCrear`,JSON.stringify(categoria));
  }

  categoriasArticuloEditar(categoria){
    return this.http.post(`${this.url}TipoCategoriaEditar`,JSON.stringify(categoria));
  }

  ArticuloClasificaciones(){
    return this.http.get(`${this.url}ArtiClas`);
  }

  CatalagoCrear(catalago){
    return this.http.post(`${this.catalagourl}CatalagoCrear`,JSON.stringify(catalago));
  }

  Catalagos(){
    return this.http.get(`${this.catalagourl}Catalagos`);
  }

  ObtenerCatalago(idart){
    return this.http.get(`${this.catalagourl}ObtenerDatos/${idart}`);
  }

  CatalagoEditar(catalago){
    return this.http.post(`${this.catalagourl}CatalagoEditar`,JSON.stringify(catalago));
  }

  CatalogoEliminar(catalago){
    return this.http.post(`${this.catalagourl}CatalagoEliminar`,JSON.stringify(catalago));
  }

  Proveedora(idProveedor){
    return this.http.get(`${this.proveedor}ProveedorAc/${idProveedor}`);
  }
  
  ProveedoraCrear(Proveedora){
    return this.http.post(`${this.proveedor}ProveedorACrear`,JSON.stringify(Proveedora));
  }
  
  ProveedoraEditar(Proveedora){
    return this.http.post(`${this.proveedor}ProveedorAEditar`,JSON.stringify(Proveedora));
  }
  
  ProveedoraEliminar(Proveedora){
    return this.http.post(`${this.proveedor}ProveedorAEliminar`,JSON.stringify(Proveedora));
  }


}
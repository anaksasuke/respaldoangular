import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';    

//export const dominio = "https://angular.clrswap.com/WebServices/";
export const dominio = "http://gastrotek.clrswap.com/WebServices/";
@Injectable({
  providedIn: 'root'
})
export class UtilService {
  id;
  nombre;
  datos;
  constructor(public router:Router,private notificacion:ToastrService, ) {}
  
  guardar(){
    this.notificacion.success("Se guardo correctamente" ,'Correcto');
  }

  Borrar(){
    this.notificacion.error("Se borro correctamente",'Correcto');
  }

  PoltDefault(politica){
    this.notificacion.error(`${politica} no puede ser borrada. Es una politica por defecto`)
  }

  ErrorLogeo(){
    this.notificacion.error("Resive sus credenciales");
  }

  Error(){
    this.notificacion.error("El número de cuenta es menor de 9 digitos");
  }

  login(param:any){
    this.notificacion.info(`Bienvenido ${param}`,"Gastroteck");
  }

  mensajeEstatus(param:any){
    this.notificacion.success(`No se puede Editar o Crear una nueva version de la ${param} mientras este con estatus aprobada`);
  }

  navegacion(param:string){
    this.router.navigate([`/${param}`]);
  }

  NavegacionDinamica(param,ubicacion){
    switch(param){
      case '?': this.notificacion.warning('Error de datos, Revise que sus datos son los correctos');
      break;
      case 4000: this.notificacion.warning('No puede guardar el registro por que ya existe');
      break;
      case 'X000': this.notificacion.warning('No puede borrar este registro ya que se encuentra en uso');
      break;
      case 0: 
      this.notificacion.success('El registro se a guardado correctamente');
      this.router.navigate([`/${ubicacion}`]);
      break;
    }
  }

  CambioEstatus(param){
    switch(param){
      case '?': this.notificacion.warning('Error inesperado, Vuelva intentar');
      break;
      case 0: 
      this.notificacion.success('Se a cambiado el estatus correctamente');
      break;
    }
  }

  FacturaNavegacion(param,ubicacion){
    switch(param){
      case '?': this.notificacion.warning('Error de datos,Revise que sus datos son los correctos');
      break;
      case 4000: this.notificacion.warning('No puede guardar el registro por que ya existe');
      break;
      case 'X000': this.notificacion.warning('No puede guardar la factura contiene mas de una moneda');
      break;
      case 0: 
      this.notificacion.success('El registro se a guardado correctamente');
      this.router.navigate([`/${ubicacion}`]);
      break;
    }
  }


  //metodo para guardar variables de session
  getSession(id,nombre){
    this.id = id;
    this.nombre = nombre;
  }

  //metodo para mostrar variables de session
  showSession(){
    var Session={
      idUser:this.id,
      username:this.nombre
    }
    return Session;
  }

  getDatos(param){
    this.datos = param;
  }

  showDatos(){
    return this.datos;
  }

  getId(param){
    this.id =  param;
  }

  showId(){
    return this.id;
  }

}
